<?php
// Require CLI and parameters
if( php_sapi_name() != 'cli' ) die('Error: This script can only be run from command line only, exiting...');
if( !isset( $argv ) || empty( $argv[1] ) || empty( $argv[2] ) ) die('Error: Missing configuration parameter, exiting...');

/*
    #argv[*]
        # [1] Configuration to run
        # [2] Domain (test / live)
        # [3] Enable/Disable Slack updates ( set = Disable )
*/

// Set timezone for AWS instances
date_default_timezone_set('Pacific/Auckland');

// Set fs-automation version (shown on script load)
$version = "1.1";

// Load Prerequisites
require __DIR__ . '/aws-sdk/aws-autoloader.php';
require __DIR__ . '/classes/generic.php';
require __DIR__ . '/classes/configuration.php';
require __DIR__ . '/classes/aws.php';

// Load some ASCII art if you want it
if( file_exists( __DIR__ . '/header.txt' ) ) echo "\n" . file_get_contents( __DIR__ . '/header.txt' ). "\n Automation v" . $version . " / PHP v" . phpversion() . "\n"; 

// Make Slack messaging variable more meaningful & error resistant (true/false only)
$argv[3] = !isset( $argv[3] ) ? true : false;

// Load Configuration and apply to AWS instance
$aws = new aws( loadConfiguration( $argv[1], $argv[2], $argv[3] ) );
