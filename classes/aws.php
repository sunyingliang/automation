<?php
require __DIR__ . '/Common/CURL.php';
use FS\Common\CURL;

class aws
{
    public $sdkEC2;
    public $sdkEC2Keypair;
    public $sdkEC2SecurityGroups;
    public $sdkEC2SecurityGroupIds;
    public $sdkS3;
    public $sdkS3Bucket;
    public $timeoutMax = ( 30 * 60 ); // Thirty minute default timeout
    public $newInstanceID;
    public $newInstanceName;
    public $newImageName;
    public $newImageID;
    public $configuration;
    public $securityGroupVpcId;

    function __construct( $configuration )
    {
        // For configuration debug uncommit this, script won't proceed to AWS SDK
        //die(var_dump($configuration));

        // Save configuration against the AWS class
        $this->configuration = $configuration;

        // Script start
        message('Starting: ' . $this->configuration->name . ' ' . $this->configuration->domain);

        // Load EC2 Credentials
        $this->loadEC2();

        // Set timeout if available
        if( isset( $this->configuration->timeout ) && !empty( $this->configuration->timeout ) ) $this->timeoutMax = $this->configuration->timeout;

        // AWS SDK Init
        $this->newInstanceName = 'FS-Automation ' . $this->configuration->name . ' ' . $this->configuration->domain . ' ' . date( 'Y-m-d H:i:s' );
        $this->newImageName = $this->configuration->name . ' ' . date( 'Y-m-d His' ); // Note H:i:s is incompatable with AMI names
        $this->validate();

        // Fetch AMI from Resemblance
        $ami   = '';
        $alias = $this->configuration->environment[$this->configuration->domain]['ami'];

        // Check if 'ami' in configuration is alias, if it is, fetch from resemblance alias API
        if (substr($alias, 0, 1) == '{' && substr($alias, -1) == '}') {
            try {
                $url   = $this->configuration->resemblance[$this->configuration->domain]['alias_url'];
                $token = $this->configuration->resemblance[$this->configuration->domain]['alias_token'];
                $alias = str_replace(['{','}'], '', $alias);

                $options = [
                    CURLOPT_URL            => $url,
                    CURLOPT_POST           => true,
                    CURLOPT_FOLLOWLOCATION => true,
                    CURLOPT_RETURNTRANSFER => true,
                    CURLOPT_POSTFIELDS     => [
                        'alias' => $alias,
                        'token' => $token
                    ]
                ];

                $curl     = CURL::init($options);
                $response = CURL::exec($curl);

                CURL::close($curl);

                if ($response === false) {
                    throw new Exception('Failed to get AMI ' . $alias . ' from Resemblance. CURL Error: "' . curl_error($curl) . '"');
                }

                $responseArr = json_decode($response, true);

                if (json_last_error() !== JSON_ERROR_NONE) {
                    throw new Exception('Could not parse data returned from Infrastructure. Response: "' . $response . '"');
                }

                if ($responseArr['response']['status'] === 'success') {
                    if (is_array($responseArr['response']['data'])) {
                        $ami = $responseArr['response']['data'][0]['id'];
                    } else {
                        throw new Exception($responseArr['response']['data']);
                    }
                } else {
                    throw new Exception('Error response from ' . $curl . '. Response: "' . $responseArr['response']['message']);
                }
            } catch (Exception $ex) {
                handleException($ex->getMessage());
            }
        } else {
            $ami = $alias;
        }

        // Create the instance
        $this->createInstance(
            $ami,
            $this->sdkEC2Keypair,
            $this->configuration->environment[ $this->configuration->domain ][ 'type' ],
            $this->sdkEC2SecurityGroupIds, 
            $this->configuration->script[ 'data' ],
            ( isset( $this->configuration->environment[ $this->configuration->domain ][ 'availabilityZone' ] ) ? $this->configuration->environment[ $this->configuration->domain ][ 'availabilityZone' ] : '' ),
            ( isset( $this->configuration->environment[ $this->configuration->domain ][ 'drives' ] ) ? $this->configuration->environment[ $this->configuration->domain ][ 'drives' ] : '' ),
            ( isset( $this->configuration->environment[ $this->configuration->domain ][ 'subnetId' ] ) ? $this->configuration->environment[ $this->configuration->domain ][ 'subnetId' ] : '' )
        );

        // Add a human readable tag to the instance
        $this->addTag( 'Name', $this->newInstanceName );

        // Wait for the instance to stop before continuing
        $this->waitForStop();

        // Clear instance userdata in all cases after stop as this is a security risk
        $this->updateUserData();

        // Check whether or not we create an AMI before continuing
        if( $this->configuration->createAMI )
        {
            $this->createAMI();
            $this->terminateInstance();
            $this->resemblancePostScript();
            slackUpdate( $this->configuration, 'Deployed ' . $this->configuration->name . ' [' . $this->configuration->domain . '] AMI: ' . $this->newImageID );
        }
        else
        {
            slackUpdate( $this->configuration, 'Deployed ' . $this->configuration->name . ' [' . $this->configuration->domain . '] Instance: ' . $this->newInstanceID );
            $this->resemblancePostScript();
        }

        // Script end
        message( "Nothing left to do, have a fantastic day!", true );
    }

    function loadSDK( $region, $key, $secret )
    {
        message( "Loading AWS SDK" );

        $sdk = new Aws\Sdk( [
            'version'     => 'latest',
            'region'      => $region,
            'credentials' => [
                'key'    => $key,
                'secret' => $secret,
            ],
        ] );

        message( "Loaded AWS SDK" );

        return $sdk;
    }

    function loadEC2()
    {
        message( "Loading EC2 Component" );

        $EC2Instance = $this->loadSDK(
            $this->configuration->aws[ $this->configuration->domain ][ 'region' ],
            $this->configuration->aws[ $this->configuration->domain ][ 'key' ],
            $this->configuration->aws[ $this->configuration->domain ][ 'secret' ]
        )->createEc2();

        if( !$EC2Instance ) message( "AWS connection configuration not available, exiting", true );

        $this->sdkEC2                   = $EC2Instance;
        $this->sdkEC2Keypair            = isset($this->configuration->environment[ $this->configuration->domain ][ 'keyname' ]) ? $this->configuration->environment[ $this->configuration->domain ][ 'keyname' ] : '';
        $this->sdkEC2SecurityGroups     = isset($this->configuration->environment[ $this->configuration->domain ][ 'securitygroups' ][ 'groupname' ]) ? $this->configuration->environment[ $this->configuration->domain ][ 'securitygroups' ][ 'groupname' ] : [];
        $this->sdkEC2SecurityGroupIds   = isset($this->configuration->environment[ $this->configuration->domain ][ 'securitygroups' ][ 'groupId' ]) ? $this->configuration->environment[ $this->configuration->domain ][ 'securitygroups' ][ 'groupId' ] : [];

        message( "Loaded EC2 Component" );
    }

    function validate()
    {
        message( "Validating SDK Settings" );

        // Make GroupIds unique
        $this->sdkEC2SecurityGroupIds = array_unique($this->sdkEC2SecurityGroupIds);

        // Validate keypairs
        $keycount = 1;

        foreach( $this->sdkEC2->describeKeyPairs()[ 'KeyPairs' ] as $awspair )
        {
            if( $this->sdkEC2Keypair == $awspair[ 'KeyName' ] )
            {
                $keycount--; // Found key, reduce $keycount
            }
        }

        if( $keycount != 0 ) message( "Error: KeyPair not found in AWS", true );

        // Validate security GroupIds
        $groupcount = count( $this->sdkEC2SecurityGroupIds );

        foreach( $this->sdkEC2->describeSecurityGroups()[ 'SecurityGroups' ] as $awsgroup )
        {
            foreach( $this->sdkEC2SecurityGroupIds as $localgroupid )
            {
                if( $localgroupid == $awsgroup[ 'GroupId' ] )
                {
                    $groupcount--; // Found group, reduce $keycount
                }
            }
        }

        if( $groupcount != 0 )
        {
            message( "Error: At least one SecurityGroupId not found in AWS", true );
        }

        // Validate security group names (and convert to GroupIds)
        $groupcount = count( $this->sdkEC2SecurityGroups );

        foreach( $this->sdkEC2->describeSecurityGroups()[ 'SecurityGroups' ] as $awsgroup )
        {
            foreach( $this->sdkEC2SecurityGroups as $localgroup )
            {
                if( $localgroup == $awsgroup[ 'GroupName' ] )
                {
                    $groupcount--; // Found group, reduce $keycount

                    // Also add this to the GroupIds as this is used to identify non-unique group names for non-default VPC's
                    if( !in_array( $awsgroup[ 'GroupId' ], $this->sdkEC2SecurityGroupIds, true ) )
                    {
                        array_push( $this->sdkEC2SecurityGroupIds, $awsgroup[ 'GroupId' ]);
                    }

                    if( isset( $awsgroup['VpcId'] ) && !empty($awsgroup['VpcId']))
                    {
                        $this->securityGroupVpcId = $awsgroup['VpcId'];
                    }
                }
            }
        }

        if( $groupcount != 0 )
        {
            message( "Error: At least one SecurityGroup not found in AWS", true );
        }

        if ( isset( $this->configuration->environment[ $this->configuration->domain ][ 'drives' ] ))
        {
            foreach ( $this->configuration->environment[ $this->configuration->domain ][ 'drives' ] as $driveName => $driveValue )
            {
                // Need to check if it already exists in the array
                if ( !isset( $driveName ) && empty( $driveName ) )
                {
                    message( "Error: Invalid Configuration. " . $driveName . " should not be empty" );
                }
            }
        }

        message( "SDK settings validated" );
    }

    function createInstance( $imageid, $keypair, $instancetype, $securitygroupids, $userdata, $availabilityzone, $drives, $subnetid )
    {
        message( "Creating new instance" );
        
        $request = [
                'ImageId'           => $imageid,
                'MaxCount'          => 1,
                'MinCount'          => 1,
                'KeyName'           => $keypair,
                'SecurityGroupIds'  => $securitygroupids,
                'InstanceType'      => $instancetype,
                'UserData'          => base64_encode( $userdata )
        ];

        if ( !empty( $availabilityzone ) )
        {
            $request['Placement'] = [ 'AvailabilityZone' => $availabilityzone ];

            if ( !empty( $this->securityGroupVpcId ) ) {
                $subnet = $this->sdkEC2->describeSubnets([
                    'Filters' => [
                        [
                            'Name'   => 'availabilityZone',
                            'Values' => [$availabilityzone]
                        ],
                        [
                            'Name'   => 'vpc-id',
                            'Values' => [$this->securityGroupVpcId]
                        ]
                    ]
                ] );

                if ( isset( $subnet['Subnets']['SubnetId'] ) ) {
                    $vpcSubnetId = $subnet['Subnets']['SubnetId'];
                } else if ( isset($subnet['Subnets'][0]['SubnetId']) ) {
                    $vpcSubnetId = $subnet['Subnets'][0]['SubnetId'];
                }
            }

            if ( !empty( $vpcSubnetId ) ) {
                $request[ 'SubnetId' ] = $vpcSubnetId;
            }
        }

        if ( empty( $vpcSubnetId ) && !empty( $subnetid ) ) {
            $request[ 'SubnetId' ] = $subnetid;
        }

        if ( !empty( $drives ) ) {

            $instanceStoreCounter           = 0;
            $request['BlockDeviceMappings'] = [];

            foreach ( $drives as $driveName => $driveValues ) {

                if ( isset($driveName) && $driveName == 'root' ) {
                    $driveName = 'sda1';
                }

                if ( !empty($driveName) ) {

                    if ( $driveValues['type'] == 'ebs' ) {
                        switch ( $driveValues['medium'] ) {
                            case 'magnetic':
                                $VolumeType = 'standard';
                                break;
                            case 'ssd':
                                $VolumeType = 'gp2';
                                break;
                            default:
                                $VolumeType = $driveValues['medium'];
                                break;
                        }

                        if ( !isset($driveValues['DeleteOnTermination']) ){
                            $driveValues['DeleteOnTermination'] = true;
                        }

                        array_push($request['BlockDeviceMappings'], [
                            'DeviceName' => "/dev/" . $driveName,
                            'Ebs'        => [
                                'VolumeSize'          => $driveValues['size'],
                                'VolumeType'          => $VolumeType,
                                'DeleteOnTermination' => $driveValues['DeleteOnTermination']
                            ]
                        ]);
                    }

                    if ( $driveValues[ 'type' ] == 'instance-store' ) {

                        $validTypes = [ 'm3', 'g2', 'c3', 'x1', 'r3', 'i2', 'd2' ];

                        if ( in_array(substr($instancetype, 0, 2), $validTypes) ) {
                            if ( $instanceStoreCounter < 23) {

                                array_push($request['BlockDeviceMappings'], [
                                    'DeviceName'  => "/dev/" . $driveName,
                                    'VirtualName' => 'ephemeral' . $instanceStoreCounter
                                ]);

                                $instanceStoreCounter++;
                            } else {
                                handleException( "Max instance-store volumes exceeded" );
                            }
                        } else {
                            handleException( $instancetype . " is EBS only." );
                        }
                    }
                }
            }
        }

        try
        {
            $response = $this->sdkEC2->runInstances( $request );

            $this->newInstanceID = $response[ 'Instances' ][ 0 ][ 'InstanceId' ];
        }
        catch( AwsException $ex )
        {
            handleException( $ex->getMessage() );
        }
        catch( Exception $ex )
        {
            handleException( $ex->getMessage() );
        }

        message( "Created new instance, InstanceID: " . $this->newInstanceID );
    }

    function addTag( $name, $value )
    {
        message( "Tagging instance " . $this->newInstanceID );

        try
        {
            $response = $this->sdkEC2->createTags( [
                'Resources' => [ $this->newInstanceID ],
                'Tags'      => [ [ 'Key' => $name, 'Value' => $value ] ]
            ] );
        }
        catch( AwsException $ex )
        {
            handleException( $ex->getMessage() );
        }
        catch( Exception $ex )
        {
            handleException( $ex->getMessage() );
        }

        message( "Instance tagged as '" . $this->newInstanceName . "'" );
    }

    function startInstance()
    {
        message( "Starting instance " . $this->newInstanceID );

        try
        {
            $response = $this->sdkEC2->startInstances( [ 'InstanceIds' => [ $this->newInstanceID ] ] );
        }
        catch( AwsException $ex )
        {
            handleException( $ex->getMessage() );
        }
        catch( Exception $ex )
        {
            handleException( $ex->getMessage() );
        }
    }

    function stopInstance()
    {
        message( "Stopping instance " . $this->newInstanceID );

        try
        {
            $response = $this->sdkEC2->stopInstances( [ 'InstanceIds' => [ $this->newInstanceID ] ] );
        }
        catch( AwsException $ex )
        {
            handleException( $ex->getMessage() );
        }
        catch( Exception $ex )
        {
            handleException( $ex->getMessage() );
        }
    }

    function waitForStop()
    {
        message( "Waiting for instance stop" );

        while( true )
        {
            try
            {
                $response = $this->sdkEC2->describeInstanceStatus( [ 'InstanceIds' => [ $this->newInstanceID ], 'IncludeAllInstances' => true ] );
                if( $response[ 'InstanceStatuses' ][ 0 ][ 'InstanceState' ][ 'Name' ] == 'stopped' ) break;
                if( runTime() > $this->timeoutMax ) message( "Error: Maximum timeout of '" . round( $this->timeoutMax / 60, 0 ) . "' minutes exceeded when checking instance state, exiting", true );
                sleepTime();
                continue;
            }
            catch( AwsException $ex )
            {
                handleException( $ex->getMessage() );
            }
            catch( Exception $ex )
            {
                handleException( $ex->getMessage() );
            }
        }

        message( "Instance stopped, continuing" );
    }

    function waitForStart()
    {
        message( "Waiting for instance start" );

        while( true )
        {
            try
            {
                $response = $this->sdkEC2->describeInstanceStatus( [ 'InstanceIds' => [ $this->newInstanceID ], 'IncludeAllInstances' => true ] );
                if( $response[ 'InstanceStatuses' ][ 0 ][ 'InstanceState' ][ 'Name' ] == 'running' ) break;
                if( runTime() > $this->timeoutMax ) message( "Error: Maximum timeout of '" . round( $this->timeoutMax / 60, 0 ) . "' minutes exceeded when checking instance state, exiting", true );
                sleepTime();
                continue;
            }
            catch( AwsException $ex )
            {
                handleException( $ex->getMessage() );
            }
            catch( Exception $ex )
            {
                handleException( $ex->getMessage() );
            }
        }

        message( "Instance started, continuing" );
    }

    function updateUserData( $userData = "" )
    {
        message( "Updating UserData" );

        try
        {
            $this->sdkEC2->modifyInstanceAttribute( [
                'InstanceId' => $this->newInstanceID,
                'UserData'   => [
                    'Value' => $userData
                ]
            ] );
        }
        catch( AwsException $ex )
        {
            handleException( $ex->getMessage() );
        }
        catch( Exception $ex )
        {
            handleException( $ex->getMessage() );
        }
    }

    function createAMI()
    {
        message( "Creating AMI" );

        try
        {
            $response = $this->sdkEC2->createImage( [ 'InstanceId' => $this->newInstanceID, 'Name' => $this->newImageName ] );
            $this->newImageID = $response[ 'ImageId' ];
        }
        catch( AwsException $ex )
        {
            handleException( $ex->getMessage() );
        }
        catch( Exception $ex )
        {
            handleException( $ex->getMessage() );
        }

        message( "Created new AMI, ImageID: " . $this->newImageID . " named " . $this->newImageName );
    }

    function terminateInstance()
    {
        try
        {
            $response = $this->sdkEC2->terminateInstances( [ 'InstanceIds' => [ $this->newInstanceID ] ] );
        }
        catch( AwsException $ex )
        {
            handleException( $ex->getMessage() );
        }
        catch( Exception $ex )
        {
            handleException( $ex->getMessage() );
        }

        message( "Instance " . $this->newInstanceID . " terminated" );
    }

    function resemblancePostScript()
    {
        // Check if Resemblance settings are in place before doing anything.
        if(
            isset( $this->configuration->resemblance[ $this->configuration->domain ][ 'apiurl' ] ) &&
            isset( $this->configuration->resemblance[ $this->configuration->domain ][ 'apiscript' ] ) &&
            isset( $this->configuration->resemblance[ $this->configuration->domain ][ 'apikey' ] ) &&
            isset( $this->configuration->resemblance[ $this->configuration->domain ][ 'apiquery' ] )
        )
        {
            $url    = $this->configuration->resemblance[ $this->configuration->domain ][ 'apiurl' ];
            $script = $this->configuration->resemblance[ $this->configuration->domain ][ 'apiscript' ];
            $key    = $this->configuration->resemblance[ $this->configuration->domain ][ 'apikey' ];
            $query  = $this->configuration->resemblance[ $this->configuration->domain ][ 'apiquery' ];

            if( $this->configuration->createAMI )
            {
                message( "Waiting for image to become available" );

                if( empty( $this->newImageID ) ) message( "Image is missing, cannot run FS-Resemblance post script", true );

                // If there is a Resemblance post-script to run, wait for the AMI to be finished then run the script
                while( true )
                {
                    try
                    {
                        $response = $this->sdkEC2->describeImages( [ 'ImageIds' => [ $this->newImageID ] ] );
                        if( $response[ 'Images' ][ 0 ][ 'State' ] == 'available' ) break;
                        if( runTime() > $this->timeoutMax ) message( "Error: Maximum timeout of '" . round( $this->timeoutMax / 60, 0 ) . "' minutes exceeded when checking instance state, exiting", true );
                        sleepTime();
                        continue;
                    }
                    catch( AwsException $ex )
                    {
                        handleException( $ex->getMessage() );
                    }
                    catch( Exception $ex )
                    {
                        handleException( $ex->getMessage() );
                    }
                }

                message( "Image is now available, continuing" );
            }
            else if( empty( $this->newInstanceID ) )
            {
                message( "Instance is missing, cannot run Resemblance post script", true );
            }

            message( "Running Resemblance post script" );
            runCurl( $url . '?script=' . urlencode( $script ) . '&key=' . urlencode( $key ) . '&' . urlencode( $query ) . '=' . urlencode( $this->configuration->createAMI ? $this->newImageID : $this->newInstanceID ), null );
        }
    }
}
