<?php

class configuration
{
    // Variables are only defined for keeping order when debugging object
    public $name;
    public $domain;
    public $domainAllowed;
    public $timeout;
    public $createAMI;
    public $script;
    public $environment;
    public $aws;
    public $git;
    public $s3;
    public $commands;
    public $resemblance;
    public $slack;
    public $enableSlack;
    public $options;

    function __construct($json = false, $domain = false, $enableSlack = true)
    {
        $this->set(json_decode($json, true));
        $this->domain      = $domain;
        $this->enableSlack = $enableSlack;

        // Initialise configuration object
        if ($json && $domain) {
            $this->loadScript();
            $this->loadSnippets();
            $this->loadEnvironment('aws');
            $this->loadEnvironment('git');
            $this->loadEnvironment('s3');
            $this->loadEnvironment('monitoring');
            $this->loadEnvironment('resemblance');
            $this->loadEnvironment('slack');
            $this->loadEnvironment('commands');
            $this->loadDefaults();

            // Final cleanup
            $this->script['data'] = discardBlankBreak($this->script['data']);

            $this->validate();
        }
    }

    function set($data)
    {
        foreach ($data as $key => $value) {
            $this->{$key} = !empty($this->{$key}) ? array_merge_recursive($this->{$key}, $value) : $value;
        }
    }

    function loadScript()
    {
        $scriptFileLocation = __DIR__ . '/../scripts/' . $this->script['file'];

        if (!file_exists($scriptFileLocation)) {
            message("Error: Script file not found, exiting", true);
        }

        $this->script['data'] = file_get_contents($scriptFileLocation);
    }

    function loadSnippets()
    {
        // If script has at least one snippet i.e. {snip_}
        if (strpos($this->script['data'], '{snip_') !== false) {
            // Chech if snippet directory exists
            if (file_exists(__DIR__ . '/../snippets')) {
                // Get snippets
                $files = scandir(__DIR__ . '/../snippets/');

                // Iterate through each snippet template
                foreach ($files as $fileName) {
                    // Get rid of special directories
                    if ($fileName === '.' || $fileName === '..') {
                        continue;
                    }

                    // Only need the name, not the extension (if it has one)
                    $cleanFileName = (strpos($fileName, '.') !== false) ? substr($fileName, 0, strpos($fileName, '.')) : $fileName;

                    // Check if snippet exists in data
                    if (strpos($this->script['data'], '{snip_' . $cleanFileName . '}') !== false) {
                        // Get the snippet content
                        $snippetData = file_get_contents(__DIR__ . '/../snippets/' . $fileName);

                        // Do the token -> snippet replace
                        $this->script['data'] = (string)str_replace('{snip_' . $cleanFileName . '}', $snippetData, $this->script['data']);
                    }
                }
            }
        }
    }

    function loadDefaults()
    {
        $scriptFileData = $this->script['data'];

        // Replace all environment placeholders with configuration domain
        $scriptFileData = (string)str_replace('{environment}', $this->domain, $scriptFileData);

        // Replace all build date placeholders with date time
        $scriptFileData = (string)str_replace('{build}', date('Y-m-d H:i:s'), $scriptFileData);

        // Replace all name placeholders with configuration name
        $scriptFileData = (string)str_replace('{name}', $this->name, $scriptFileData);

        $this->script['data'] = $scriptFileData;
    }

    function loadEnvironment($type)
    {
        // Load environment configurations
        $configurationFileLocation = __DIR__ . '/../environments/' . $type . '.conf';

        if (!file_exists($configurationFileLocation)) {
            message("Error: " . $type . " configuration file not found, exiting", true);
        }

        $configuration       = file_get_contents($configurationFileLocation);
        $configurationObject = json_decode($configuration, true);

        if (json_last_error() != JSON_ERROR_NONE) {
            message("Error: " . $type . " configuration file is not valid JSON, exiting", true);
        }

        // Special case for Resemblance data merge
        if ($type == 'resemblance') {
            if (isset($this->resemblance[$this->domain])) {
                if (isset($configurationObject[$type][$this->domain])) {
                    $this->resemblance[$this->domain] = array_merge($configurationObject[$type][$this->domain], $this->resemblance[$this->domain]);
                } else {
                    $this->resemblance[$this->domain] = null;
                }
            }
        } else {
            $this->set($configurationObject);
        }

        // Special cases for environment variables
        switch ($type) {
            case 'git':
                if (isset($this->git)) {
                    foreach ($this->git as $key => $value) {
                        $this->script['data'] = (string)str_replace('{' . $key . '}', $value, $this->script['data']);
                    }
                }
                break;
            case 's3';
                if (isset($this->s3)) {
                    foreach ($this->s3 as $key => $value) {
                        $this->script['data'] = (string)str_replace('{s3_' . $key . '}', $value, $this->script['data']);
                    }
                }
                break;
            case 'monitoring':
                if (isset($this->monitoring[$this->domain])) {
                    foreach ($this->monitoring[$this->domain] as $key => $value) {
                        $this->script['data'] = (string)str_replace('{monitoring_' . $key . '}', $value, $this->script['data']);
                    }
                }
                break;
            case 'resemblance':
                if (isset($this->resemblance[$this->domain])) {
                    foreach ($this->resemblance[$this->domain] as $key => $value) {
                        $this->script['data'] = (string)str_replace('{resemblance_' . $key . '}', $value, $this->script['data']);
                    }
                }
                break;
            case 'slack':
                if (isset($this->slack)) {
                    foreach ($this->slack as $key => $value) {
                        $this->script['data'] = (string)str_replace('{slack_' . $key . '}', $value, $this->script['data']);
                    }
                }
                break;
            case 'commands':
                if (isset($this->commands[$this->domain])) {
                    foreach ($this->commands[$this->domain] as $key => $value) {
                        $this->script['data'] = (string)str_replace($key, $value, $this->script['data']);
                    }
                }

                if (isset($this->commands['generic'])) {
                    foreach ($this->commands['generic'] as $key => $value) {
                        $this->script['data'] = (string)str_replace($key, $value, $this->script['data']);
                    }
                }
                break;
        }
    }

    function validate()
    {
        // Search template to validate dynamic configuration object structure
        $search = [
            'name',
            'domain',
            'timeout',
            'createAMI',
            'environment' => [
            ],
            'script'      => [
                'file',
                'data'
            ],
            'aws'         => [
            ],
            'git'         => [
                'github'
            ],
            's3'          => [
                'region',
                'key',
                'secret',
                'bucket'
            ],
            'commands'    => [
            ],
            'monitoring'  => [
            ],
            'resemblance' => [
            ],
            'slack'       => [
                'channel',
                'username',
                'url'
            ],
            'enableSlack'
        ];

        // Check if our expected configuration exists
        foreach ($search as $key => $value) {
            if (is_array($value)) {
                if (in_array($key, ['environment', 'commands', 'monitoring', 'resemblance', 'aws'])) {
                    // Check it contains value from domain
                    if (!isset($this->{$key}{$this->domain})) {
                        message("Error: Invalid configuration '" . $key . "' is missing '" . $this->domain . "', exiting", true);
                    }
                } else {
                    foreach ($value as $arrayKey => $arrayValue) {
                        if (!isset($this->{$key}{$arrayValue})) {
                            message("Error: Invalid configuration '" . $key . "' is missing '" . $arrayValue . "', exiting", true);
                        }
                    }
                }
            } elseif (!isset($this->{$value})) {
                message("Error: Invalid configuration '" . $value . "' is missing, exiting", true);
            }
        }

        // Check if the essential parameters exists for db-migration
        if (strpos(strtolower($this->name), 'db-migration-') === 0) {
            $mandatoryParams = ['{migration_target}', '{migration_host}', '{migration_database}', '{migration_username}', '{migration_password}', '{migration_repo}', '{migration_dir_master}'];
            $missedParams    = [];

            foreach ($mandatoryParams as $mandatory) {
                if (!isset($this->commands[$this->domain][$mandatory])) {
                    $missedParams[] = $mandatory;
                }
            }

            if (isset($this->commands[$this->domain]['sql'])) {
                if (!isset($this->commands[$this->domain]['migration-dir-slave'])) {
                    $missedParams[] = 'migration-dir-slave';
                }
            } else {
                if (isset($this->commands[$this->domain]['migration-dir-slave'])) {
                    $missedParams[] = 'sql';
                }
            }

            if (!empty($missedParams)) {
                if (count($missedParams) > 1) {
                    $message = 'Error: Required configurations {' . implode(', ', $missedParams) . '} are missing, exiting';
                } else {
                    $message = 'Error: Required configuration {' . implode(', ', $missedParams) . '} is missing, exiting';
                }

                message($message, true);
            }
        }

        // Check dbMigration flag, prompt user to ensure db-migration has been done before continue
        if (property_exists($this, 'dbMigration')) {
            message('Has the DB-Migration for ' . $this->name . ' [' . $this->domain . '] been processed? [y|n]: ');

            while ($answer = strtolower($this->getUserAnswer())) {
                if (in_array($answer, ['y', 'yes', 'n', 'no'])) {
                    break;
                }
                message('Please input your answer as one of [y|yes|n|no]:');
            }

            if (in_array($answer, ['n', 'no'])) {
                message('Please run /bat/' . $this->dbMigration . ' and wait for it to be completed before continuing, exiting script.', true);
            }
        }
    }

    private function getUserAnswer()
    {
        return trim(fgets(STDIN));
    }
}
