<?php

function loadConfiguration($configurationFile, $domain, $enableSlack)
{
    $configurationFileLocation = __DIR__ . '/../configurations/' . $configurationFile . '.conf';

    if (!file_exists( $configurationFileLocation )) {
        message( "Error: Configuration file not found, exiting", true );
    }

    $configuration = file_get_contents( $configurationFileLocation );

    json_decode( $configuration );

    if (json_last_error() != JSON_ERROR_NONE) {
        message( "Error: Configuration file is not valid JSON, exiting", true );
    }

    if ($domain != 'test' && $domain != 'devops' && $domain != 'internal' && $domain != 'live') {
        message( "Error: Invalid environment", true );
    }

    return new configuration( $configuration, $domain, $enableSlack );
}

function handleException($exception)
{
    message( $exception, true );
}

function runTime()
{
    return round( microtime( true ) - $_SERVER[ 'REQUEST_TIME_FLOAT' ], 0 );
}

function sleepTime()
{
    // Time to take a nap for a bit
    sleep( rand( 25, 35 ) );
}

function discardBlankBreak($content)
{
    $result = '';

    foreach (preg_split( '/\r\n|\n|\r/', $content ) as $line) {
        // Get rid of tabs (just for this loop, we want them in the result)
        $customLine = trim(preg_replace('/\t+/', '', $line));

        // Get rid of spaces (just for this loop, we want them in the result)
        $customLine = str_replace( ' ', '', $customLine);

        // If first character = hash (not hash bang) or blank, discard
        $hasHash        = substr( $customLine, 0, 1 ) == '#';
        $hasHashBang    = substr( $customLine, 0, 2 ) == '#!';
        $hasEmptyLine   = empty( $customLine );

        if (( $hasHash && !$hasHashBang ) || $hasEmptyLine) {
            continue;
        }

        $result .= empty( $result ) ? $line : "\n" . $line;
    }

    return $result;
}

function runCurl($url, $post)
{
    $ch = curl_init( $url );

    if (isset( $post )) {
        curl_setopt( $ch, CURLOPT_POST, 1 );
        curl_setopt( $ch, CURLOPT_POSTFIELDS, $post );
    }

    curl_setopt( $ch, CURLOPT_FOLLOWLOCATION, 1 );
    curl_setopt( $ch, CURLOPT_HEADER, 0 );
    curl_setopt( $ch, CURLOPT_RETURNTRANSFER, 1 );

    $response = curl_exec( $ch );
    curl_close( $ch );

    return $response;
}

function slackUpdate($configuration, $message)
{
    if ($configuration->enableSlack) {
        message( "Posting Slack status update" );
        $slackData = '{"channel": "' . $configuration->slack[ 'channel' ] . '", "username": "' . $configuration->slack[ 'username' ] . '", "text": "' . $message . '"}';
        runCurl( $configuration->slack[ 'url' ], $slackData );
    }
}

function message($message, $die = false)
{
    $message = PHP_EOL . date('H:i:s') . ' -> ' . $message;

    if ($die) {
        die($message . PHP_EOL);
    }

    echo($message);

    sleep(1);
}
