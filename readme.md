# Automation


### GENERAL INFORMATION

####General
    1) FS-Automation parses configuration files and scripts in order to make an AMI which gets passed off to FS-Resemblance
    2) It is designed to run off command line only
    3) There is a default timeout of 30 minutes built in for debugging purposes, you can override this in the configuration
    4) You can watch the progress of instance creation in the AWS console while the application runs
    5) A 'userdata' script is sent to AWS on image creation with a number of commands to setup the server
        1) A typical fs-automation script takes 10-20 minutes to complete
        2) Each image will automatically shut down at the end of the userdata execution
            1) The userdata progress can be tracked in the AWS console by right clicking / Instance Settings / Get System Log
            2) Any script with createAMI=false in the config will not create an AMI (will run post steps if specified i.e. FS-Resemblance)
        3) See the "Dynamic file deployment variables" section below for special userdata cases
    6) If CreateAMI is enabled and the instance has finished building an AMI (Amazon Machine Image) will be created, and the origional image terminated
        1) You can disable this step in configuration and allow Resemblance to run post scripts with the new instance instead
    7) Upon termination, if there are post commands to run the script will wait until the AMI has finished then handover to FS-Resemblance

#####Input
    1) CLI Call
        1) Example call: php automate.php proxy test
    2) Requires matching configuration in /configurations directory
        1) Example /configurations/proxy.conf
    3) Requires matching script in /scripts directory
        1) Example /scripts/proxy.sh

#####Output
    1) Update FirmstepDC / S3 with related deploy files
    1) Create new instance based on your configuration
        1) Runs script on instance
        2) Shuts down instance
    3) If AMI creation is enabled
        1) Copies instance to AMI
        2) Terminates instance
        3) Returns AMI ImageID in CLI & Slack (Auckland Team)
    4) If AMI creation is disabled ( Instance only )
        1) Start instance
    5) Runs post AMI creation scripts (i.e. Resemblance API updates)
    6) Posts notification to slack


#### INSTALLATION

1) Clone FS-Automation project from Git
2) Install prerequisites
    1) PHP > 5.6.* (Ideally > 7.*)
	2) Requires: extension=php_curl.dll
	3) Requires: extension=php_openssl.dll
2) You can create your own run.bat files, a number of examples can be found in the /bat directory
    1) Example call: php ../automate.php proxy test
        1) Ensure your environment variables are setup to make such a call
            1) Refer to https://docs.google.com/document/d/1bwX4jHkVR0G1PN4SBgUWml6HV-FoTm7D0I2csvZJtts/edit
    2) Run the script ensuring you're using the right environment and the configuration has all the credentials needed


#### GENERAL SETUP

##### Special curl certificates for AWS
1) If you have not done so already, curl requires special certificates on windows to connect with AWS
    1) Download: http://curl.haxx.se/ca/cacert.pem
    2) Include in php.ini under curl.cainfo (you may need to uncomment) i.e.
        1) Example: curl.cainfo = "C:\Users\Username\Desktop\Working\SHA\cacert.pem"


##### Environment configuration files
1) aws.conf (use aws_example.conf as reference)
    1) region
        1) Your AWS region
            1) You can find this in the url when logged into the AWS account you're using for deployment
    2) key
        1) Your AWS account key (created when you first made your account)
    3) secret
        1) Your AWS secret (created when you first made your account)
    NOTE: If you cannot find your access keys or don't have one you can create a new access key by heading to 
        AWS Identity & Access management / users / {yourusername} / Security Credentials
2) git.conf (use git_example.conf as reference)
    1) github
        1) Token is a github 'Personal Access Token', found at https://github.com/settings/tokens
            1) When asked for a scope, select all under 'repo'
    2) stash
        1) Token is a base64 encoded string of your username and password in username:password format
            1) PHP Example: base64_encode('username:password');
3) s3.conf (this does not have an example, as of writing the same keys are used for all deployments)
4) resemblance.conf (this does not have an example, as of writing the same urls are used for all deployments)
5) monitoring.conf (this does not have an example, as of writing the same urls are used for all deployments)
6) slack.conf (this does not have an example, as of writing the same keys are used for all deployments)
7) commands.conf (specific set of commands for each environment, parsed into scripts)

##### Making configuration files
1) Feel free to use proxy.conf as a template
    1) Remove any custom commands (Keep the test/live sections though)
    2) Replace any variables with your project needs
        1) "name"           -> Is your project name, eventually this will make it to the AMI name
        2) "timeout"        -> Is the timeout the script will wait while an instance is built
        3) "createAMI"      -> Does your script need an instance or AMI? Set to true if you need an AMI
        4) "script"         -> The AWS UserData script to be interpreted
        5) "Environment"    -> Env specific settings i.e. Test / Devops / Internal / Live
            1) "ami"                -> The AMI to base the machine off (different per environment, PV or HVM)
            2) "type"               -> The instance 'type', e.g. t2.nano (when using 'instance store', the instance type should be 'm3', 'g2', 'c3', 'x1', 'r3', 'i2', 'd2'. Amazon EC2 Instance Types: https://aws.amazon.com/ec2/instance-types/ )
            3) "keyname"            -> The environment keyname to use e.g. Sydney-Test
            4) "availabilityZone"   -> Optional: The availabilityZone to place the instance in, random by default
            5) "securitygroups"     -> The security groups to apply (one method supported currently)
                1) "groupname"          -> Optional value for the security groups to apply by name (Resolved internally to Id)
                2) "groupId"            -> Optional value for the security group id's to apply (Note if using non-default vpc id is required)
            6) "subnetId"             -> Optional value for the subnet Id of the VPC you're joining (Required for non-default VPC's). 
                                         Please note that if a AvailabilityZone and VPC Security Gouup is set this is found automatically and will overwrite any subnetId specified.
            7) "drives"             -> Optional value for the attached drives.
                1)"deviceName"           -> The name of devices. e.g. root (device name should be unique. For Linux device name should be sdf through sdp, and for Windows should be xvdf through xvdp. Linux may rename your devices from sdf through sdp to xvdf through xvdp)
                    1)"type"                  ->device type, (ebs or instance-store) Note: If type is 'instance-store', only this parameter is required
                    2)"size"                  ->volume size for ebs devices in gigbytes, cannot be smaller than the default volume size of a instance type
                    3)"medium"                ->volume type for ebs devices [magnetic(standard),ssd(gp2),io1,st1,sc1]. Amazon EBS Volume Types: http://docs.aws.amazon.com/AWSEC2/latest/UserGuide/EBSVolumeTypes.html
                    4)"DeleteOnTermination"   ->Optional value for ebs devices, indicates whether the EBS volume is deleted on instance termination. (boolean)
            Note: Example for drives configuration
                    "drives": {
                    			"root": {
                    				"type": "ebs",
                    				"size": 9,
                    				"medium": "ssd"
                    			},
                    			"xvdp": {
                           	          "type": "ebs",
                          	           "size": 11,
                          	           "medium": "ssd",
                          	           "DeleteOnTermination": true
                          	 	      },
                    			"sdh": {
                    				"type": "instance-store"
                    				}
                              }

##### Making scripts
1) Bash and PowerShell are supported
    1) Write the script like any other, once tested convert all dynamic variables to either custom or static environment variables
        1) i.e. When using s3 storage, environment must be used i.e. /test/apache.conf, change this to /{environment}/apache.conf
    2) Ensure your script shuts down the instance when it's done
        1) To debug, do not shut down the instance at the end of your script
            1) Remote into the machine
            2) Check the AWS log for any errors
                1) Linux: /var/log/cloud-init-output.log
                2) Windows: System Event Log / Viewer

2) Custom environment variables
    1) Use the configuration / command section of the script configuration file and add your custom environment variables
        1) Example: "{switchthis01}": "tothis01" will result in any {switchthis01} token getting replaced with "tothis01" (without quotes) after parsing

3) Static environment variables
    {name}               -> Name of the configuration
    {build}              -> Time the script was run, placed in build files on AMI creation
    {environment}        -> The environment the script is running under i.e. test/live (passed in CLI)
    {github}             -> Your 'personal access token' used for retrieving files from github via curl
    {stash}              -> base64(username:password) used for retrieving files from stash via curl
    {s3_region}          -> S3 region for current configuration
    {s3_key}             -> S3 key for current configuration
    {s3_secret}          -> S3 secret for current configuration
    {s3_bucket}          -> S3 bucket for current configuration
    {monitoring_url}     -> Monitoring URL for current environment
    {resemblance_apiurl} -> URL to resemblance API
    {slack_channel}      -> Slack channel - used to determine which slack channel to post to
    {slack_username}     -> Slack user name - used when posting to slack thread
    {slack_url}          -> Slack url - used for calling slack API with static credentials

4) Dynamic file deployment variables
    1) If under the deployment directory there is a matching folder for your script name, the script will be searched for a matching variable i.e. {proxy.conf}
        1) If a matching variable exists it will be replaced with the file content and sent along with the userdata when making the EC2 instance
        2) Any files that did not get integrated with the userdata will be uploaded to FirmstepDC and the related s3 bucket of the project you're deploying
    2) All files deployed to S3 and merged with UserData are subject to some minification (comments, blank lines)

5) Snippets
    1) Snippets are compact, resuable pieces of code that can be included in any script, the snippets are located under the /snippets folder
        1) To use a snippet simply use the {snip_yoursnippetnamehere} token, exclude the snippet extension, the token will be replaced with the appropriate snippet at runtime

6) Generic variables
    1) Generic variables are built into the /environments/commands.conf file and can be found under the /commands/generic branch. These are not tied to an environment
        1) To use a generic variable simply use the {yourgenericvariable} token, it will be replaced with the appropriate generic variable at runtime
