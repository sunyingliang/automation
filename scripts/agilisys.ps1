<powershell>

Set-ExecutionPolicy Unrestricted -Force

write-host "Configuring timezone"
tzutil.exe /s "{timezone}"

write-host "Creating default directories"
if (-Not (Test-Path "C:\TempDownloads")) { New-Item -ItemType directory -Path C:\TempDownloads | Out-Null }
if (-Not (Test-Path "C:\Firmstep")) { New-Item -ItemType directory -Path C:\Firmstep | Out-Null }

[Net.ServicePointManager]::SecurityProtocol = "tls12, tls11, tls"
$errors = ""

write-host "Downloading and installing 7-Zip"
(New-Object System.Net.WebClient).DownloadFile("http://www.7-zip.org/a/7z1514-x64.msi", "C:\TempDownloads\7zip.msi")
if (Test-Path C:\TempDownloads\7zip.msi) {
    msiexec /package "C:\TempDownloads\7zip.msi" /quiet /passive /qn /norestart | Out-Null
} else {
    $errors += "7-Zip not found. "
}

write-host "Downloading and installing Notepad++"
(New-Object System.Net.WebClient).DownloadFile("https://notepad-plus-plus.org/repository/6.x/6.9.2/npp.6.9.2.Installer.exe", "C:\TempDownloads\npp.exe")
if (Test-Path C:\TempDownloads\npp.exe) {
    C:\TempDownloads\npp.exe /Q /S | Out-Null
} else {
    $errors += "Notepad++ not found. "
}

write-host "Downloading and installing AWS CLI Tools"
(New-Object System.Net.WebClient).DownloadFile("https://s3.amazonaws.com/aws-cli/AWSCLI64.msi", "C:\TempDownloads\AWSCLI64.msi")
if (Test-Path C:\TempDownloads\AWSCLI64.msi) {
    msiexec /package "C:\TempDownloads\AWSCLI64.msi" /quiet /passive /qn /norestart | Out-Null
} else {
    $errors += "AWS CLI Tools not found. "
}

{snip_win_monitoring}

{snip_win_localadmin}

if ("{environment}" -eq "test") {
    {snip_win_domainjoin}
}

if ($errors) {
    Send-MailMessage -From "{name}@firmstep.com" -To "auckland-team@firmstep.com" -Subject "{name} Automation Error [{environment}]" -SmtpServer "smtp.firmstep.com" -Body "$errors"
}

Import-Module ServerManager
Add-WindowsFeature -Name Web-Static-Content,Web-Default-Doc,Web-Http-Errors,Web-Http-Redirect,Web-Asp-Net,Web-Net-Ext,Web-ISAPI-Ext,Web-Request-Monitor,Web-Performance,Web-Mgmt-Console -IncludeAllSubFeature | Out-Null
C:\Windows\Microsoft.NET\Framework\v4.0.30319\aspnet_regiis.exe -i | Out-Null

# Download and install SSL certificate
Copy-S3Object -AccessKey {s3_key} -SecretKey {s3_secret} -Region {s3_region} -BucketName fs-deploy -Key Configuration/Certificates/{environment}/certificate.pfx -LocalFile C:\TempDownloads\certificate.pfx
C:\Windows\System32\certutil.exe -f -p {sslcertpassword} -importpfx "C:\TempDownloads\certificate.pfx"
$cert = Get-ChildItem -Path Cert:\LocalMachine\My | Select-Object -First 1
New-WebBinding -Name "Default Web Site" -IP "*" -Port 443 -Protocol https
New-Item -Path IIS:\SslBindings\0.0.0.0!443 -Value $cert

#download latest code from github
Invoke-RestMethod -Uri https://api.github.com/repos/Firmstep/FS-Agilisys/zipball/{gitbranch} -Headers @{"Authorization" = "token {github}"} -OutFile C:\TempDownloads\Agilisys.zip
c:\Program` Files\7-Zip\7z.exe x c:\TempDownloads\Agilisys.zip -oc:\TempDownloads\ | Out-Null
Get-ChildItem C:\TempDownloads -Directory -Filter "Firmstep-FS-Agilisys-*" | Select-Object -First 1 -OutVariable out
Move-Item $out.FullName C:\Agilisys
Set-ItemProperty 'IIS:\Sites\Default Web Site\' -name physicalPath -value c:\Agilisys\Agilisys

#remove unnecessary folders
Remove-Item "C:\Agilisys\PortalEmulator" -Recurse -Force

# Install the SSO certificate into 'My' store
$FirmstepCertificate = new-object System.Security.Cryptography.X509Certificates.X509Certificate2
$FirmstepCertificate.import("C:\Agilisys\Certificates\Firmstep.pfx","{agilisyscertpassword}","PersistKeySet,MachineKeySet")
$LocalStore = new-object System.Security.Cryptography.X509Certificates.X509Store("My","LocalMachine")
$LocalStore.open("MaxAllowed")
$LocalStore.add($FirmstepCertificate)
$LocalStore.close()

# Add permission to IIS user
$fullPath="C:\ProgramData\Microsoft\Crypto\RSA\MachineKeys\"+$FirmstepCertificate.PrivateKey.CspKeyContainerInfo.UniqueKeyContainerName
Invoke-Expression "icacls $fullPath /grant IIS_IUSRS:RX" | Out-Null

# Create the Other People Certificate Store
New-Item -Path HKLM:\SOFTWARE\Microsoft\SystemCertificates -Name AddressBook

If ("{environment}" -ne "live")
{
	#in the test environment, the "foreign trust party" is still us!
	Copy-Item -Path "C:\Agilisys\Certificates\Firmstep.cer" -Destination "C:\Agilisys\Certificates\AgilisysPublicKey.cer" -Force 
}

# Install the certificate into 'Other People'
$AgilisysCertificate = new-object System.Security.Cryptography.X509Certificates.X509Certificate2
$AgilisysCertificate.import("C:\Agilisys\Certificates\AgilisysPublicKey.cer")
$AddressBookStore = new-object System.Security.Cryptography.X509Certificates.X509Store("AddressBook","LocalMachine")
$AddressBookStore.open("MaxAllowed")
$AddressBookStore.add($AgilisysCertificate)
$AddressBookStore.close()

#create local configuration file
$config = @" 
{
  "agilisys_key_thumbprint": "$($AgilisysCertificate.Thumbprint)",
  "agilisys_url": "{agilisys_url}",
  "agilisys_api_url": "{agilisys_api_url}",
  "agilisys_api_user": "{agilisys_api_user}",
  "agilisys_api_password": "{agilisys_api_password}",
  "firmstep_key_thumbprint": "$($FirmstepCertificate.Thumbprint)",
  "firmstep_url": "{firmstep_url}",
  "rrc_url": "{rrc_webservice}",
  "rrc_site": "{rrc_sitename}",
  "rrc_key": "{rrc_key}",
  "rrc_proxy_url": "",
  "auto_redirect": "true",
  "custom_errors_mode": "{custom_errors_mode}",
  "log4net_type": "database",
  "dblog_connection_string": "{log_db}",
  "environment": "{environment}"
}
"@ 
New-Item "C:\Agilisys\config.json" -type File -force -value $config 
#run powershell configuration script
Invoke-Expression "C:\Agilisys\Scripts\Setup.ps1"

#Build website
Invoke-Expression "C:\Agilisys\.nuget\NuGet.exe restore -PackagesDirectory C:\Agilisys\packages C:\Agilisys\Agilisys\packages.config"
Invoke-Expression "C:\Windows\Microsoft.NET\Framework64\v4.0.30319\MSBuild /t:Rebuild /p:Configuration=Release C:\Agilisys\Agilisys.sln"

#set app pool to "LoadUserProfile" = True
Set-ItemProperty -Path IIS:\AppPools\DefaultAppPool -Name "processModel.loadUserProfile" -Value "true"

write-host "Installing UrlRewrite2"
(New-Object System.Net.WebClient).DownloadFile("http://download.microsoft.com/download/C/9/E/C9E8180D-4E51-40A6-A9BF-776990D8BCA9/rewrite_amd64.msi", "C:\TempDownloads\rewrite_amd64.msi")
msiexec /package "C:\TempDownloads\rewrite_amd64.msi" /quiet /passive /qn /norestart /log c:/msi.log | Out-Host

# Redirect http to https
Add-WebConfigurationProperty -filter "system.webServer/rewrite/rules" -name "." -value @{name='Redirect to https'; enable='true'; patternSyntax='Wildcard'; stopProcessing='true'}
Set-WebConfigurationProperty -filter "system.webServer/rewrite/rules/rule[@name='Redirect to https']/match" -name "." -value @{url='*'; negate='false'}
Set-WebConfigurationProperty -filter "system.webServer/rewrite/rules/rule[@name='Redirect to https']/action" -name "." -value @{type='Redirect'; url='https://{HTTP_HOST}{REQUEST_URI}'; redirectType='Found'}
Set-WebConfigurationProperty -filter "system.webServer/rewrite/rules/rule[@name='Redirect to https']/conditions" -name "." -value @{logicalGrouping='MatchAny'}
Add-WebConfigurationProperty -filter "system.webServer/rewrite/rules/rule[@name='Redirect to https']/conditions[@logicalGrouping='MatchAny']" -name "." -value @{input='{HTTPS}'; pattern='off'}

# Create Build file in webroot folder
new-item "C:\inetpub\wwwroot\build.txt" -type File -force -value "Build: {name} {build}"

# Cleanup
Remove-Item "C:\TempDownloads" -Recurse -Force

Stop-Computer

</powershell>