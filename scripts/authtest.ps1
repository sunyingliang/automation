<powershell>

Set-ExecutionPolicy Unrestricted -Force

write-host "Configuring timezone"
tzutil.exe /s "{timezone}"

write-host "Creating default directories"
if (-Not (Test-Path "C:\TempDownloads")) { New-Item -ItemType directory -Path C:\TempDownloads | Out-Null }
if (-Not (Test-Path "C:\Firmstep")) { New-Item -ItemType directory -Path C:\Firmstep | Out-Null }

[Net.ServicePointManager]::SecurityProtocol = "tls12, tls11, tls"
$errors = ""

write-host "Downloading and installing 7-Zip"
(New-Object System.Net.WebClient).DownloadFile("http://www.7-zip.org/a/7z1514-x64.msi", "C:\TempDownloads\7zip.msi")
if (Test-Path C:\TempDownloads\7zip.msi) {
    msiexec /package "C:\TempDownloads\7zip.msi" /quiet /passive /qn /norestart | Out-Null
} else {
    $errors += "7-Zip not found. "
}

write-host "Downloading and installing Notepad++"
(New-Object System.Net.WebClient).DownloadFile("https://notepad-plus-plus.org/repository/6.x/6.9.2/npp.6.9.2.Installer.exe", "C:\TempDownloads\npp.exe")
if (Test-Path C:\TempDownloads\npp.exe) {
    C:\TempDownloads\npp.exe /Q /S | Out-Null
} else {
    $errors += "Notepad++ not found. "
}

write-host "Downloading and installing AWS CLI Tools"
(New-Object System.Net.WebClient).DownloadFile("https://s3.amazonaws.com/aws-cli/AWSCLI64.msi", "C:\TempDownloads\AWSCLI64.msi")
if (Test-Path C:\TempDownloads\AWSCLI64.msi) {
    msiexec /package "C:\TempDownloads\AWSCLI64.msi" /quiet /passive /qn /norestart | Out-Null
} else {
    $errors += "AWS CLI Tools not found. "
}

{snip_win_monitoring}

{snip_win_localadmin}

if ("{environment}" -eq "test") {
    {snip_win_domainjoin}
}

if ($errors) {
    Send-MailMessage -From "{name}@firmstep.com" -To "auckland-team@firmstep.com" -Subject "{name} Automation Error [{environment}]" -SmtpServer "smtp.firmstep.com" -Body "$errors"
}

Import-Module ServerManager
Add-WindowsFeature -Name Web-Static-Content,Web-Default-Doc,Web-Http-Errors,Web-Http-Redirect,Web-Asp,Web-Net-Ext,Web-ISAPI-Ext,Web-Request-Monitor,Web-Performance,Web-Mgmt-Console,WAS -IncludeAllSubFeature | Out-Null
C:\Windows\Microsoft.NET\Framework\v4.0.30319\aspnet_regiis.exe -i | Out-Null

write-host "Installing UrlRewrite2"
(New-Object System.Net.WebClient).DownloadFile("http://download.microsoft.com/download/C/9/E/C9E8180D-4E51-40A6-A9BF-776990D8BCA9/rewrite_amd64.msi", "C:\TempDownloads\rewrite_amd64.msi")
msiexec /package "C:\TempDownloads\rewrite_amd64.msi" /quiet /passive /qn /norestart /log c:/msi.log | Out-Host

# Install debug FAM
if (-Not (Test-Path "C:\FAM")) { New-Item -ItemType directory -Path C:\FAM | Out-Null }
Copy-S3Object -AccessKey {s3_key} -SecretKey {s3_secret} -Region {s3_region} -BucketName {s3_bucket} -Key FAMASP.zip -LocalFile C:/TempDownloads/FAMASP.zip | Out-Null
c:\Program` Files\7-Zip\7z.exe x c:\TempDownloads\FAMASP.zip -o"C:\FAM"
c:\Windows\SysWOW64\regsvr32.exe C:\FAM\VBCorLib.dll
c:\Windows\SysWOW64\regsvr32.exe C:\FAM\FAMASP.dll
New-WebAppPool -Name "FAM"
Set-ItemProperty -Path IIS:\AppPools\FAM -Name managedRuntimeVersion -Value ''
Set-ItemProperty -Path IIS:\AppPools\FAM -Name enable32BitAppOnWin64 -Value "true"
Set-ItemProperty "IIS:\Sites\Default Web Site" -Name applicationPool -Value FAM

# Remove IIS start page
Remove-Item C:\inetpub\wwwroot\iisstart.htm
Remove-Item C:\inetpub\wwwroot\welcome.png

# Create new home page
$homePage = @'
<%Option Explicit

If Not IsObject(Session("User")) Then
	Response.Redirect "fam.asp?ReturnURL=default.asp"
	Response.End
End If

Dim HasAccess
HasAccess = False

Dim FAMUser, Key, i

Set FAMUser = Session("User")
If FAMUser.Groups.Count > 0 Then
	For i = 0 To FAMUser.Groups.Count - 1
		If FAMUser.Groups.Item(i) = "Auckland Team" Then
			HasAccess = True
		End If
	Next
End If

If Not HasAccess Then
	Response.Status = 403
	Response.Write "Access denied!"
	Response.End
End If

Dim GroupsList
GroupsList = Array("Everyone", "Resemblance Users", "Resemblance Admins", "OAF Admin", "Auckland Team", "developers", "CMS Admins", "Platform Admins", "Platform Team", "Solutions Admins")

If Request.ServerVariables("HTTP_METHOD") = "POST" Then

	Dim FAMKey, FAMIV, FAMCipher

	FAMKey = "NZEwN7TDaDtNQ/Xnqfepd+9Y3sJN4yUdNkAGgO6qMNE="
	FAMIV = "mdPIckeljQUL7Qnsdro8WA=="
	FAMCipher = "AES"
	
	Dim FAM, AuthRequest, User
	Set FAM = Server.CreateObject("FAMASP.Util")
	AuthRequest = FAM.GenerateRequest(FAMKey, FAMIV, FAMCipher)
	Set User = FAM.DecodeRequest(FAMKey, FAMIV, FAMCipher, AuthRequest)
	
	User.UserName = FAMUser.UserName
	User.FullName = FAMUser.FullName
	User.EmailAddress = FAMUser.EmailAddress

	User.Profile.Add "remote_ip", Request.ServerVariables("REMOTE_ADDR")
	
	Dim ChosenGroups
	ChosenGroups = Split(Request.Form("Groups"),",")
	
	For i = 0 To UBound(ChosenGroups)
		User.Groups.Add Trim(ChosenGroups(i))
	Next

	Dim AuthResponse, DestURL
	AuthResponse = FAM.EncodeResponse(FAMKey, FAMIV, FAMCipher, User)
	
	If Request.Form("System") = "resemblance" Then
		DestURL = "https://resemblance.firmsteptest.com/famlogin.php?ReturnURL=/"
	Else
		DestURL = "https://admin.firmsteptest.com/famlogin.php?ReturnURL=/"
	End If

%><!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" >
<head><title>Logging in...</title></head>
<body>
<pre>
	<%=Server.HTMLEncode(User.ToSAML)%>
</pre>
<form action="<%=Server.HTMLEncode(DestURL)%>" method="post">
    <input type="hidden" name="AuthToken" id="AuthToken" value="<%=AuthResponse%>"/>
    <input type="hidden" name="DestPage" id="DestPage" value="/"/>
	<input type="submit" value="Go" />
</form>
<!--<script type="text/javascript">document.forms[0].submit();</script>-->
</body>
</html>
<%
	Response.End
End If

%><!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" >
<head><title>FAM Permissions Test Page</title></head>
<body>
<form action="default.asp" method="post">
  <div>
		System:&nbsp;&nbsp;&nbsp;
		Resemblance <input type="radio" name="System" id="SysResemblance" value="resemblance"/>&nbsp;&nbsp;&nbsp;
		Platform Admin <input type="radio" name="System" id="SysAdmin" value="admin" checked="checked"/><br/>
		Groups: <br/>
        <% For i = 0 To UBound(GroupsList) %>
		<%=GroupsList(i)%>: <input type="checkbox" name="Groups" id="grp<%=Replace(GroupsList(i)," ", "")%>" value="<%=GroupsList(i)%>" checked="checked" /><br/>
		<% Next %>
        <input type="submit" value="Go" />
  </div>  
</form>
</body>
</html>
'@
[IO.File]::WriteAllText("C:\inetpub\wwwroot\default.asp",$homePage)

# Create fam login page
$famPage = @'
<%
Option Explicit

Dim FAMKey, FAMIV, FAMCipher, FAMURL

FAMKey = "NZEwN7TDaDtNQ/Xnqfepd+9Y3sJN4yUdNkAGgO6qMNE="
FAMIV = "mdPIckeljQUL7Qnsdro8WA=="
FAMCipher = "AES"
FAMURL = "http://firmstepauth-test.appspot.com/famlogin"

Dim ReturnURL

Dim oFAM
Set oFAM = Server.CreateObject("FAMASP.Util")

Dim AuthToken
AuthToken = Request.Form("AuthToken")

If Len(AuthToken) = 0 Then
	
	ReturnURL = "http"
	If LCase(Request.ServerVariables("HTTPS")) = "on" Or Request.ServerVariables("HTTP_X_SSL_ON") = "1" Then
		ReturnURL = "https"
	End If
	ReturnURL = ReturnURL & "://" & Request.ServerVariables("HTTP_HOST") & Request.ServerVariables("SCRIPT_NAME")
	ReturnURL = ReturnURL & "?ReturnURL=" & Request.QueryString("ReturnURL")
	
	Dim AuthRequest
	AuthRequest = oFAM.GenerateRequest(FAMKey, FAMIV, FAMCipher)
%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" >
<head><title>Connecting to authentication server...</title></head>
<body>
<form action="<%=FAMURL%>" method="post">
  <div>
        Connecting to authentication server...
        <input type="hidden" name="AuthToken" id="AuthToken" value="<%=AuthRequest%>"/>
        <input type="hidden" name="DestPage" id="DestPage" value="<%=ReturnURL%>"/>
        <noscript<input type="submit" value="Press here to continue" /></noscript>
  </div>  
</form>
<script type="text/javascript">document.forms[0].submit();</script>
</body>
</html>
<%
Else
	
	Dim AuthUser
	Set AuthUser = oFAM.DecodeResponse(FAMKey, FAMIV, FAMCipher, AuthToken)
	
	Set Session("User") = AuthUser
	
	ReturnURL = Request.QueryString("ReturnURL")
	
	If Len(ReturnURL) = 0 Then
		ReturnURL = "/"
	End If
	
	Response.Redirect ReturnURL
	
End If
%>
'@
[IO.File]::WriteAllText("C:\inetpub\wwwroot\fam.asp",$famPage)

# Download and install SSL certificate
Copy-S3Object -AccessKey {s3_key} -SecretKey {s3_secret} -Region {s3_region} -BucketName fs-deploy -Key Configuration/Certificates/{environment}/certificate.pfx -LocalFile C:\TempDownloads\certificate.pfx
C:\Windows\System32\certutil.exe -f -p {sslcertpassword} -importpfx "C:\TempDownloads\certificate.pfx"
$cert = Get-ChildItem -Path Cert:\LocalMachine\My | Select-Object -First 1
New-WebBinding -Name "Default Web Site" -IP "*" -Port 443 -Protocol https
New-Item -Path IIS:\SslBindings\0.0.0.0!443 -Value $cert

# Redirect http to https
Add-WebConfigurationProperty -filter "system.webServer/rewrite/rules" -name "." -value @{name='Redirect to https'; enable='true'; patternSyntax='Wildcard'; stopProcessing='true'}
Set-WebConfigurationProperty -filter "system.webServer/rewrite/rules/rule[@name='Redirect to https']/match" -name "." -value @{url='*'; negate='false'}
Set-WebConfigurationProperty -filter "system.webServer/rewrite/rules/rule[@name='Redirect to https']/action" -name "." -value @{type='Redirect'; url='https://{HTTP_HOST}{REQUEST_URI}'; redirectType='Found'}
Set-WebConfigurationProperty -filter "system.webServer/rewrite/rules/rule[@name='Redirect to https']/conditions" -name "." -value @{logicalGrouping='MatchAny'}
Add-WebConfigurationProperty -filter "system.webServer/rewrite/rules/rule[@name='Redirect to https']/conditions[@logicalGrouping='MatchAny']" -name "." -value @{input='{HTTPS}'; pattern='off'}

# Create Build file in webroot folder
new-item "C:/inetpub/wwwroot/build.txt" -type File -force -value "Build: {name} {build}"

# Cleanup
Remove-Item "C:\Users\Default\Desktop\*.*"
Remove-Item "C:\TempDownloads" -Recurse -Force

Stop-Computer

</powershell>
