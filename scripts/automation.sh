#!/usr/bin/env bash

{snip_deb_sysctl}

# General commands
sudo add-apt-repository ppa:ondrej/php
apt update -y
apt install -y apache2 libapache2-mod-php7.2
apt install -y php7.2-cli php7.2-curl php7.2-xml
apt install -y awscli unzip
#apt upgrade -y

{snip_deb_security}

{snip_deb_timezone}

{snip_deb_swap}

# Download FS-Automation project from GitHub
curl -s -H "Authorization: token {github}" -o /tmp/{name}.tar -L https://api.github.com/repos/Firmstep/FS-Automation/tarball/{gitbranch}
tar xf /tmp/{name}.tar -C /tmp
outfolder=$( ls -1c /tmp | grep Firmstep-FS-Automation- | head -1)
mkdir -p /home/ubuntu/bin/{name}
cp -R /tmp/$outfolder/* /home/ubuntu/bin/{name}/
rm -R /tmp/$outfolder
rm /tmp/{name}.tar

# Download FS-Monitoring project from GitHub
curl -s -H "Authorization: token {github}" -o /tmp/{name}.tar -L https://api.github.com/repos/Firmstep/FS-Monitoring/tarball/master
tar xf /tmp/{name}.tar -C /tmp
outfolder=$( ls -1c /tmp | grep Firmstep-FS-Monitoring- | head -1)
mkdir -p /home/ubuntu/bin/PHP-Client
cp -R /tmp/$outfolder/PHP-Client/* /home/ubuntu/bin/PHP-Client
rm -R /tmp/$outfolder
rm /tmp/{name}.tar

# Copy custom files from S3
export AWS_ACCESS_KEY_ID={s3_key}
export AWS_SECRET_ACCESS_KEY={s3_secret}
export AWS_DEFAULT_REGION={s3_region}
aws s3 cp s3://{s3_bucket}/Configuration/{name}/{environment}/aws.conf /home/ubuntu/bin/{name}/environments/aws.conf
aws s3 cp s3://{s3_bucket}/Configuration/{name}/{environment}/git.conf /home/ubuntu/bin/{name}/environments/git.conf
aws s3 cp s3://{s3_bucket}/Configuration/{name}/{environment}/common.php /home/ubuntu/bin/PHP-Client/common.php

# Create build file
printf "Build: {name} {build}" > /var/www/html/build
rm /var/www/html/index.html
a2dismod autoindex -f

# Install and configure monitoring agent
mkdir -p /home/ubuntu/bin

# Download project from GitHub
curl -s -H "Authorization: token {github}" -o /tmp/Monitoring.tar -L https://api.github.com/repos/Firmstep/FS-Monitoring/tarball/master
tar xf /tmp/Monitoring.tar -C /tmp
outfolder=$( ls -1c /tmp | grep Firmstep-FS-Monitoring- | head -1)
cp -R /tmp/$outfolder/Bash/monitoring /home/ubuntu/bin/monitoring
rm -R /tmp/$outfolder
rm /tmp/Monitoring.tar

# Replace the token using a global monitoring token in environments/commands
cd /home/ubuntu/bin/
cat monitoring | tr -d '\r' > monitoring_tmp && mv monitoring_tmp monitoring
sed -i -e "s@###serverid###@{name}@g" -e "s@###monurl###@https://infrastructure.firmsteptest.com/api/monitoring/log@g" -e "s@###appendinstanceid###@{monitoring_appendinstanceid}@g" -e "s@###montoken###@46F3A4C9-0290-40EB-B7E9-4A5495785E0C@g" monitoring
sed -i -e '$i \nohup /sbin/start-stop-daemon -S -b --name monitoring --exec /home/ubuntu/bin/monitoring &\n' /etc/rc.local

# Setup permissions
chown -R ubuntu:ubuntu /home/ubuntu/bin
chmod -R 770 /home/ubuntu/bin

# Setup crontab
echo "*/15 * * * * sudo sh -c 'echo 1 > /proc/sys/vm/drop_caches'
* * * * * php /home/ubuntu/bin/PHP-Client/monitoring.php" >> mycron
cat /home/ubuntu/bin/{name}/shared/automation.cron | tr -d '\r' > cron_tmp && mv cron_tmp /home/ubuntu/bin/{name}/shared/automation.cron
cat /home/ubuntu/bin/{name}/shared/automation.cron >> mycron
crontab -u ubuntu mycron
rm mycron

# Finally shut down the instance for AMI creation
sudo shutdown -h now
