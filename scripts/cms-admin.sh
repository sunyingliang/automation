#!/usr/bin/env bash

{snip_deb_sysctl}

# General commands
sudo add-apt-repository ppa:ondrej/php
apt update -y
apt install -y apache2 php7.2-cli libapache2-mod-php7.2 php7.2-mysql php7.2-curl php7.2-mbstring php7.2-xml
apt install -y awscli unzip nfs-common mysql-client
#apt upgrade -y

{snip_deb_security}

{snip_deb_timezone}

# Prepare directories
rm -r /var/www/*

# Download project from GitHub
curl -s -H "Authorization: token {github}" -o /tmp/{name}.tar -L https://api.github.com/repos/Firmstep/FS-CMS-Admin/tarball/{gitbranch}
tar xf /tmp/{name}.tar -C /tmp
outfolder=$( ls -1c /tmp | grep Firmstep-FS-CMS-Admin- | head -1)
mkdir -p /var/www/{name}
cp -R /tmp/$outfolder/* /var/www/{name}/
rm -R /tmp/$outfolder
rm /tmp/{name}.tar

# Copy custom files from S3
export AWS_ACCESS_KEY_ID={s3_key}
export AWS_SECRET_ACCESS_KEY={s3_secret}
export AWS_DEFAULT_REGION={s3_region}
aws s3 cp s3://{s3_bucket}/Configuration/{name}/{environment}/common.php /var/www/{name}/config/common.php
aws s3 cp s3://{s3_bucket}/Configuration/Certificates/{environment}/certificate.txt /etc/apache2/certificate.txt
aws s3 cp s3://{s3_bucket}/Configuration/Certificates/{environment}/key.txt /etc/apache2/key.txt
aws s3 cp s3://{s3_bucket}/Configuration/Certificates/{environment}/chain.txt /etc/apache2/chain.txt

# Generate VirtualHost File
echo "<VirtualHost *:80>
    RewriteEngine On
    RewriteRule (.*) https://%{SERVER_NAME}\$1 [R,L]
</VirtualHost>
<VirtualHost *:443>
    DocumentRoot /var/www/{name}/public
    ErrorLog /var/log/apache2/error.log
    CustomLog /var/log/apache2/access.log combined
    <Directory /var/www/{name}/public>
        Options +Includes -Indexes +FollowSymLinks +MultiViews
        AllowOverride All
        Require all granted
		RewriteEngine on
		RewriteRule (.*) /index.php [L,QSA]
    </Directory>
	SSLEngine on
    SSLCertificateFile /etc/apache2/certificate.txt
    SSLCertificateKeyFile /etc/apache2/key.txt
    SSLCertificateChainFile /etc/apache2/chain.txt
</VirtualHost>" > /etc/apache2/sites-available/{name}.conf

{snip_deb_mpmconfig}

# Create build file
printf "Build: {name} {build}" > /var/www/{name}/public/build

{snip_deb_opcache}

{snip_deb_monitoring}

{snip_deb_permissions}

# Finalise apache deployment environment
a2dissite 000-default
a2enmod rewrite
a2ensite {name}
a2enmod ssl
rm -f /etc/apache2/sites-available/000-default.conf
rm -f /etc/apache2/sites-available/default-ssl.conf
service apache2 restart

# Setup NFS mountpoint
mkdir /efs
echo "{nfs_mount} /efs nfs4 nfsvers=4.1,rsize=1048576,wsize=1048576,hard,timeo=600,retrans=2 0 0" >> /etc/fstab
mount /efs

# Setup crontab
echo "*/15 * * * * sudo sh -c 'echo 1 > /proc/sys/vm/drop_caches'
* * * * * php /var/www/{name}/scripts/token_cache.php
* * * * * sudo php /var/www/{name}/scripts/process_queue.php
{cron_backup_efs} sudo php /var/www/{name}/scripts/backup_cms_efs.php
0 * * * * sudo php /var/www/{name}/scripts/backup_cms_db.php
{orphan_report} sudo php /var/www/{name}/scripts/orphan_report.php
0 * * * * php /var/www/{name}/scripts/tunnel_healthcheck.php" >> mycron
crontab -u ubuntu mycron
rm mycron

# Setup rc.local (ubuntu on safe reboot)
sed -i -e '$i \nohup mount /efs &\n' /etc/rc.local

{snip_deb_efs}

# Finally shut down the instance for AMI creation
sudo shutdown -h now
