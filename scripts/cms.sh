#!/usr/bin/env bash

{snip_deb_sysctl}

# Purge any php v7.x related items in preperation for v5.6
#apt purge `dpkg -l | grep php| awk '{print $2}' |tr "\n" " "`

# Add legacy php repository
#add-apt-repository ppa:ondrej/php

# General commands
sudo add-apt-repository ppa:ondrej/php
apt update -y
#apt install -y cron awscli apache2 php5.6 libapache2-mod-php5.6 php5.6-mysql php5.6-curl php5.6-soap php5.6-mcrypt php5.6-gd php5.6-dev php5.6-snmp php5.6-xsl php-apc unzip nfs-common snmp composer unattended-upgrades
apt install -y apache2 php7.2-cli libapache2-mod-php7.2 php7.2-mysql php7.2-curl php7.2-soap php7.2-mbstring php7.2-gd php7.2-dev php7.2-snmp php7.2-xsl php-apcu php7.2-sqlite
apt install -y awscli unzip nfs-common snmp composer
apt install -y build-essential git libfuse-dev libcurl4-openssl-dev libxml2-dev mime-support automake libtool pkg-config libssl-dev
apt install -y varnish
DEBIAN_FRONTEND=noninteractive apt -yq install nullmailer
#apt upgrade -y

# NewRelic Installation
echo 'deb http://apt.newrelic.com/debian/ newrelic non-free' | sudo tee /etc/apt/sources.list.d/newrelic.list
wget -O- https://download.newrelic.com/548C16BF.gpg | sudo apt-key add -
apt-get update
DEBIAN_FRONTEND=noninteractive apt-get -y install newrelic-php5
sed -e "s@newrelic.license = \"\"@newrelic.license = \"e37b9eb512a1437ca8b2e575377fa86e34bcc8d5\"@g" /etc/php/7.0/mods-available/newrelic.ini > /tmp/relic_tmp && mv /tmp/relic_tmp /etc/php/7.0/mods-available/newrelic.ini
sed -e "s@newrelic.appname = \"PHP Application\"@newrelic.appname = \"All Hosts;{name} [{environment}]\"@g" /etc/php/7.0/mods-available/newrelic.ini > /tmp/relic_tmp && mv /tmp/relic_tmp /etc/php/7.0/mods-available/newrelic.ini

{snip_deb_security}

{snip_deb_timezone}

{snip_deb_swap}

# Prepare directories
rm -r /var/www/*

# Update composer to latest version
php -r "copy('https://getcomposer.org/installer', '/tmp/composer-setup.php');"
php /tmp/composer-setup.php --install-dir=/usr/local/bin --filename=composer
rm -rf /tmp/composer-setup.php

# Install Drush
sudo -H -u ubuntu bash -c "composer global require drush/drush:8.*"
sudo ln -s /home/ubuntu/.composer/vendor/drush/drush/drush /usr/local/bin

# Setup nullmailer
echo "firmstep.com" >> /etc/nullmailer/defaultdomain
echo "{smtp_relay_host}" >> /etc/nullmailer/remotes

# Update Varnish default.vcl
echo "vcl 4.0;
backend default {
    .host = \"127.0.0.1\";
    .port = \"8080\";
    .max_connections = 20;
    .connect_timeout = 600s;
    .first_byte_timeout = 600s;
    .between_bytes_timeout = 600s;
}
sub vcl_backend_error {
    if (beresp.status >= 500) {
        return(retry);
    }
}
sub vcl_hash {
    if (req.http.X-Forwarded-Proto) {
        hash_data(req.http.X-Forwarded-Proto);
    }
}" > /etc/varnish/default.vcl

{snip_deb_opcache}

# Install and configure monitoring agent
{snip_deb_monitoring}

# Update PHP/Apache Memory limit
#cd /etc/php5/apache2/
cd /etc/php/7.0/apache2/
sed -e "s@post_max_size = 8M@post_max_size = 128M@g" php.ini > /tmp/php_tmp && mv /tmp/php_tmp php.ini
sed -e "s@upload_max_filesize = 2M@upload_max_filesize = 128M@g" php.ini > /tmp/php_tmp && mv /tmp/php_tmp php.ini
sed -e "s@memory_limit = 128M@memory_limit = 512M@g" php.ini > /tmp/php_tmp && mv /tmp/php_tmp php.ini
sed -e "s@; max_input_vars = 1000@max_input_vars = 4096@g" php.ini > /tmp/php_tmp && mv /tmp/php_tmp php.ini

# Update varnish conf to use port 80
# FOR 16.04:
service varnish stop
mkdir /etc/systemd/system/varnish.service.d
echo "[Service]
ExecStart=
ExecStart=/usr/sbin/varnishd -j unix,user=vcache -F -a :80 -T localhost:6082 -f /etc/varnish/default.vcl -S /etc/varnish/secret -s malloc,256m" >> /etc/systemd/system/varnish.service.d/customexec.conf
service varnish start
# FOR 14.04:
#cd /etc/default/
#sed -e "s@6081@80@g" varnish > varnish_tmp && mv varnish_tmp varnish

# Update apache conf to use port 8080
cd /etc/apache2/
sed -e "s@Listen 80@Listen 8080@g" ports.conf > ports_tmp && mv ports_tmp ports.conf

# Setup NFS mountpoint
mkdir /efs
echo "{nfs_mount} /efs nfs4 nfsvers=4.1,rsize=1048576,wsize=1048576,hard,timeo=600,retrans=2 0 0" >> /etc/fstab
mount /efs

# Download and install default unavailable site
mkdir /var/www/default
cd /var/www/default/
curl -H 'Authorization: token {github}' -H 'Accept: application/vnd.github.v3.raw' -O -L https://api.github.com/repos/Firmstep/FS-Automation/contents/shared/site-unavailable.html
mv site-unavailable.html index.html
cat index.html | tr -d '\r' > index_tmp && mv index_tmp index.html

# Download and install PHP SimpleSAML
cd /var
git clone https://github.com/simplesamlphp/simplesamlphp.git simplesaml
cd /var/simplesaml
cp -r config-templates/* config/
cp -r metadata-templates/* metadata/
composer install

# Do SimpleSAML setup for config.php
cd /var/simplesaml/config
sed -i -e "s@'baseurlpath' => 'simplesaml/'@'baseurlpath' => 'https://' . $_SERVER['HTTP_HOST'] . '/simplesaml/'@g" config.php
sed -i -e "s@'session.phpsession.cookiename' => 'SimpleSAML'@'session.phpsession.cookiename' => null@g" config.php
sed -i -e "s@'store.type'                    => 'phpsession'@'store.type'                    => 'sql'@g" config.php
sed -i -e "s@'store.sql.dsn'                 => 'sqlite:/path/to/sqlitedatabase.sq3'@'store.sql.dsn'                 => 'mysql:host=rds-cms-01.cv0ldt5kzfz7.eu-west-1.rds.amazonaws.com;dbname=firmstep-kb'@g" config.php
sed -i -e "s@'store.sql.username' => null@'store.sql.username' => 'firmstepkb'@g" config.php
sed -i -e "s@'store.sql.password' => null@'store.sql.password' => 'F1rmst3pKB'@g" config.php
sed -i -e "s@'memcache_store.prefix' => null@'memcache_store.prefix' => 'saml'@g" config.php

# Do SimpleSAML setup for saml20-idp-remote.php
cd /var/simplesaml/metadata
echo "<?php
\$metadata['customers.firmstep.com'] = array (
  'metadata-set' => 'saml20-idp-remote',
  'entityid' => 'customers.firmstep.com',
  'SingleSignOnService' =>
  array (
    0 =>
    array (
      'Binding' => 'urn:oasis:names:tc:SAML:2.0:bindings:HTTP-Redirect',
      'Location' => 'https://customers.firmstep.com/saml/saml2/idp/SSOService.php',
    ),
  ),
  'SingleLogoutService' =>
  array (
    0 =>
    array (
      'Binding' => 'urn:oasis:names:tc:SAML:2.0:bindings:HTTP-Redirect',
      'Location' => 'https://customers.firmstep.com/saml/saml2/idp/SingleLogoutService.php',
    ),
  ),
  'certData' => 'MIIFmTCCA4GgAwIBAgIJANEpKKdv2QsbMA0GCSqGSIb3DQEBBQUAMGMxCzAJBgNVBAYTAkdCMQ8wDQYDVQQIDAZMb25kb24xDzANBgNVBAcMBkxvbmRvbjERMA8GA1UECgwIRmlybXN0ZXAxHzAdBgNVBAMMF$
  'NameIDFormat' => 'urn:oasis:names:tc:SAML:1.1:nameid-format:emailAddress',
);" > saml20-idp-remote.php

# Create build file
echo "Build: {name} {build}" > /var/www/default/build

# Download and install rebuild script
cd /home/ubuntu/bin/
curl -H 'Authorization: token {github}' -H 'Accept: application/vnd.github.v3.raw' -O -L https://api.github.com/repos/Firmstep/FS-Automation/contents/shared/cms-rebuild.php
cat cms-rebuild.php | tr -d '\r' > cms_tmp && mv cms_tmp cms-rebuild.php

# Setup default-000.conf
echo "<VirtualHost *:8080>
    ServerAdmin webmaster@localhost
    DocumentRoot /var/www/default
    <Directory />
        Options +FollowSymLinks
        AllowOverride None
    </Directory>
</VirtualHost>" > /etc/apache2/sites-available/000-default.conf

{snip_deb_permissions}

# Finalise apache deployment environment
a2enmod rewrite
a2enmod ssl
a2enmod cgi
a2enmod expires
a2enmod headers
a2enmod reqtimeout
rm -f /etc/apache2/sites-available/default-ssl.conf
service apache2 restart
service varnish restart

# Setup rc.local (ubuntu on safe reboot)
sed -i -e '$i \nohup mount /efs &\n' /etc/rc.local

{snip_deb_efs}

# Setup cron
echo "*/15 * * * * sudo sh -c 'echo 1 > /proc/sys/vm/drop_caches'" >> mycron

# Conditional environment setup
if [ "{env}" == "Dev" ] || [ "{env}" == "Staging" ]; then
    echo "*/5 * * * * sudo php /home/ubuntu/bin/cms-rebuild.php \"{endpoint}\" \"{token}\" \"{env}\"" >> mycron

    # Add rebuild script to rc.local to ensure it's the last thing completed at the end of boot
    sed -i -e '$i \nohup sudo php /home/ubuntu/bin/cms-rebuild.php \"{endpoint}\" \"{token}\" \"{env}\" &\n' /etc/rc.local
elif [ "{env}" == "Live" ]; then
    # Run rebuild script immediately and never again to ensure reboot will not affect Live
    php /home/ubuntu/bin/cms-rebuild.php "{endpoint}" "{token}" "{env}"
fi

crontab -u ubuntu mycron
rm mycron

# NewRelic - For live only
#wget -O - https://download.newrelic.com/548C16BF.gpg | sudo apt-key add -
#sh -c 'echo "deb http://apt.newrelic.com/debian/ newrelic non-free" > /etc/apt/sources.list.d/newrelic.list'
#DEBIAN_FRONTEND=noninteractive apt -yq install newrelic-php5
#newrelic-install install
#echo 'newrelic.license="814f85093da4edf372e3e688c540c8a611f3cf1b"' >> /etc/php5/apache2/conf.d/20-newrelic.ini

# Finally shut down the instance for AMI creation
sudo shutdown -h now
