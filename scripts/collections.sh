#!/usr/bin/env bash

{snip_deb_sysctl}

# General commands
export DEBIAN_FRONTEND=noninteractive
apt-get update -y
apt-get install -y -f apache2 php5-cli php5 libapache2-mod-php5 libapache2-mod-rpaf php5-mysql php5-curl apache2-utils curl awscli unzip git
curl -sS https://getcomposer.org/installer | sudo php -- --install-dir=/usr/local/bin --filename=composer -y
#apt-get upgrade -y

{snip_deb_timezone}

{snip_deb_swap}

# Prepare directories, disable xdebug to improve performance
rm -r /var/www/*
php5dismod xdebug
service apache2 restart

# Download project from GitHub
curl -s -H "Authorization: token {github}" -o /tmp/FS-Collections.tar -L https://api.github.com/repos/Firmstep/FS-Collections-API/tarball/{gitbranch}
tar xf /tmp/FS-Collections.tar -C /tmp
outfolder=$( ls -1c /tmp | grep Firmstep-FS-Collections- | head -1)
cp -R /tmp/$outfolder /var/www/{name}
rm -R /tmp/$outfolder
rm /tmp/FS-Collections.tar

# Allow ubuntu user to read www files
usermod -a -G www-data ubuntu
chown -R ubuntu:ubuntu /var/www
chmod -R 775 /var/www

# Remove deploy/configuration files from legacy setup
rm -R /var/www/{name}/app/config/deploy/*
rm /var/www/{name}/app/config/config_*.yml
rm /var/www/{name}/web/app_*.php
rm /var/www/{name}/web/tests.js
rm /var/www/{name}/web/framework.js

# Copy any required custom files
export AWS_ACCESS_KEY_ID={s3_key}
export AWS_SECRET_ACCESS_KEY={s3_secret}
export AWS_DEFAULT_REGION={s3_region}
aws s3 cp s3://{s3_bucket}/Configuration/{name}/{environment}/common.php /var/www/{name}/config/common.php
aws s3 cp s3://{s3_bucket}/Configuration/{name}/{environment}/log4php.xml /var/www/{name}/config/log4php.xml

# Add /status URL for ELB health-check
mkdir /var/www/default
echo "Status_OK" > /var/www/default/status

# Create build file
printf "Build: {name} {build}" > /var/www/default/build

# VirtualHost 000-default.conf
echo '<VirtualHost *:80>
    DocumentRoot /var/www/default
    ErrorDocument 404 "Not found"
</VirtualHost>' > /etc/apache2/sites-available/000-default.conf

# RPAF Configuration
echo "<IfModule mod_rpaf.c>
    RPAFenable On
    RPAFsethostname On
#OLD CONFIG: RPAFproxy_ips 10.64.1.181 10.33.207.15 10.59.143.13 10.235.97.245 10.105.139.84 10.34.170.97 10.208.49.125 10.209.208.151 10.34.186.91 10.89.140.102 10.76.138.42 10.104.5.238
    RPAFproxy_ips 10. 172.31.
</IfModule>" > /etc/apache2/mods-enabled/rpaf.conf

# Bugfix for broken bootstrap.php.cache after Symfony/Composer clash [1/2]
cp /var/www/Collections/app/bootstrap.php.cache /var/www/Collections/app/bootstrap.php.cache.bak

# Install the project dependencies
sudo -H -u ubuntu bash -c 'sudo composer install --working-dir=/var/www/{name}'

# Update project dependencies
sudo -H -u ubuntu bash -c 'sudo php /var/www/{name}/bin/vendors install'

# Run initial Collections setup script (Note: includes schema rebuild)
sudo -H -u ubuntu bash -c 'sudo php /var/www/{name}/scripts/rebuild.php'

# Bugfix for broken bootstrap.php.cache after Symfony/Composer clash [2/2]
mv -f /var/www/Collections/app/bootstrap.php.cache.bak /var/www/Collections/app/bootstrap.php.cache

{snip_deb_monitoring}

{snip_deb_permissions}

# Finalise apache deployment environment
a2dismod autoindex
a2enmod rewrite
php5enmod mysql
rm -f /etc/apache2/sites-available/default-ssl.conf
service apache2 reload

{snip_deb_dropcache}

# Setup rc.local (ubuntu on safe reboot)
sed -i -e '$i \nohup php /var/www/{name}/scripts/rebuild.php &\n' /etc/rc.local

# Finally shut down the instance for AMI creation
sudo shutdown -h now
