<powershell>
function DownloadFromGit {
param([string]$file)
$r = Invoke-WebRequest -URI "https://api.github.com/repos/Firmstep/FS-Automation/contents/shared/$file" -Headers @{"Authorization"="token {github}"}
$c = $r.Content | ConvertFrom-Json
$decoded = [System.Convert]::FromBase64String($c.content)
[IO.File]::WriteAllBytes("C:\Firmstep\$file",$decoded)
}
function Unzip {
param([string]$archive,[string]$outdir)
c:\Program` Files\7-Zip\7z.exe x $archive -o"$outdir" > $null
}

Set-ExecutionPolicy Unrestricted -Force

# Configuring timezone
tzutil.exe /s "{timezone}"

# Installing IIS
Import-Module ServerManager
Add-WindowsFeature -Name Web-Static-Content,Web-Default-Doc,Web-Http-Errors,Web-Http-Redirect,Web-Asp-Net,Web-Asp-Net45,Web-Net-Ext,Web-Net-Ext45,Web-CGI,Web-ISAPI-Ext,Web-ISAPI-Filter,Web-Http-Logging,Web-Custom-Logging,Web-ODBC-Logging,Web-Performance,Web-Mgmt-Console,Web-Mgmt-Compat,NET-WCF-HTTP-Activation45 -IncludeAllSubFeature > $null
C:\Windows\Microsoft.NET\Framework\v4.0.30319\aspnet_regiis.exe -i > $null

#Create the directories
$download = "C:\TempDownloads"
if (-Not (Test-Path "$download")) { ni -ItemType directory -Path $download > $null }
if (-Not (Test-Path "C:\Firmstep")) { ni -ItemType directory -Path C:\Firmstep > $null }

[Net.ServicePointManager]::SecurityProtocol = "tls12, tls11, tls"
$errors = ""
$client = New-Object System.Net.WebClient

# Installing UrlRewrite2
$client.DownloadFile("http://download.microsoft.com/download/C/9/E/C9E8180D-4E51-40A6-A9BF-776990D8BCA9/rewrite_amd64.msi", "$download\rewrite_amd64.msi")
if (Test-Path $download\rewrite_amd64.msi){
msiexec /package "$download\rewrite_amd64.msi" /quiet /passive /qn /norestart /log c:/msi.log | Out-Host
} else {
$errors += "UrlRewrite2 not found. "
}

# Downloading and installing 7zip
$client.DownloadFile("http://www.7-zip.org/a/7z1514-x64.msi", "$download\7zip.msi")
if (Test-Path $download\7zip.msi){
msiexec /package "$download\7zip.msi" /quiet /passive /qn /norestart > $null
} else {
$errors += "7zip not found. "
}

# Downloading and installing Notepad++
$client.DownloadFile("https://notepad-plus-plus.org/repository/6.x/6.9.2/npp.6.9.2.Installer.exe", "$download\npp.exe")
if (Test-Path $download\npp.exe){
& $download\npp.exe /Q /S > $null
} else {
$errors += "Notepad++ not found. "
}

# Downloading and installing C++ 2015 Redistributable
# Used for PHP
$client.DownloadFile("https://download.microsoft.com/download/9/3/F/93FCF1E7-E6A4-478B-96E7-D4B285925B00/vc_redist.x64.exe", "$download\vcredist_2015.exe")
if (Test-Path $download\vcredist_2015.exe){
& $download\vcredist_2015.exe /Q /S > $null
} else {
$errors += "C++ 2015 Redistributable not found. "
}

# Downloading and installing MSSQL ODBC Driver
$client.DownloadFile("https://download.microsoft.com/download/5/7/2/57249A3A-19D6-4901-ACCE-80924ABEB267/ENU/x64/msodbcsql.msi", "$download\mssql.msi")
if (Test-Path $download\mssql.msi){
msiexec /package "$download\mssql.msi" /quiet /passive /qn /norestart IACCEPTMSODBCSQLLICENSETERMS=YES > $null
} else {
$errors += "MSSQL ODBC Driver not found. "
}

# "Downloading and installing AWS CLI Tools
$client.DownloadFile("https://s3.amazonaws.com/aws-cli/AWSCLI64.msi", "$download\AWSCLI64.msi")
if (Test-Path $download\AWSCLI64.msi){
msiexec /package "$download\AWSCLI64.msi" /quiet /passive /qn /norestart > $null
} else {
$errors += "AWS CLI Tools not found. "
}

# Downloading and installing Simple Network Time Protocol (SNTP) client
$client.DownloadFile("http://www.timesynctool.com/NetTimeSetup-314.exe", "$download\nettime.exe")
if (Test-Path $download\nettime.exe){
& $download\nettime.exe /verysilent > $null
} else {
$errors += "NetTime not found. "
}

# Downloading and installing MySQL Connector Net
$client.DownloadFile("https://cdn.mysql.com/Downloads/Connector-Net/mysql-connector-net-6.7.9-noinstall.zip", "$download\mysql-connector-net.zip")
if (Test-Path $download\mysql-connector-net.zip){
Unzip $download\mysql-connector-net.zip $download\mysql-connector-net
} else {
$errors += "MySQL Connector Net not found. "
}

# Downloading and installing PHP with User agent spoofing workaround
$client.Headers.Add('user-agent', 'Mozilla/5.0 (Windows NT 6.1; Win64; x64)')
$client.DownloadFile("http://windows.php.net/downloads/releases/archives/php-7.0.25-Win32-VC14-x64.zip", "$download\php.zip")
if (Test-Path $download\php.zip){
Unzip $download\php.zip c:\PHP > $null
} else {
$errors += "PHP not found. "
}

DownloadFromGit "ComponentsCheck.ps1"
DownloadFromGit "SQSWatcher.ps1"
DownloadFromGit "UploadLogs.ps1"

if ($errors) {
Send-MailMessage -From "components@firmstep.com" -To "auckland-team@firmstep.com" -Subject "Components Automation Error [{environment}]" -SmtpServer "smtp.firmstep.com" -Body "$errors"
}

$appCmd = "C:\Windows\system32\inetsrv\appcmd.exe"
& $appCmd --% set apppool "DefaultAppPool" /processModel.maxProcesses:"{maxprocesses}" /commit:apphost

Import-Module WebAdministration

Set-WebConfigurationProperty -Filter System.Applicationhost/Sites/SiteDefaults/logfile -Name LogExtFileFlags -Value "Date,Time,ClientIP,UserName,SiteName,ComputerName,ServerIP,Method,UriStem,UriQuery,HttpStatus,Win32Status,BytesSent,BytesRecv,TimeTaken,ServerPort,UserAgent,Cookie,Referer,ProtocolVersion,Host,HttpSubStatus"
Set-WebConfigurationProperty -Filter System.Applicationhost/Sites/SiteDefaults -Name logFile -Value @{period="Hourly"}

# Configuring AWS interaction
Set-AWSCredentials -AccessKey {s3_key} -SecretKey {s3_secret} -StoreAs default
Set-DefaultAWSRegion -Region {s3_region}

# Configuring domain
$localcomputer = [ADSI]"WinNT://$env:computername"
$localadmin = $localcomputer.Create("User", "LocalAdmin")
$localadmin.SetPassword("{adminpassword}")
$localadmin.UserFlags = 64 + 65536
$localadmin.SetInfo()
$usergroup = [ADSI]"WinNT://$env:computername/Users,group"
$usergroup.psbase.Invoke("Add",$localadmin.path)
$admingroup = [ADSI]"WinNT://$env:computername/Administrators,group"
$admingroup.psbase.Invoke("Add",$localadmin.path)

if ("{environment}" -eq "test") {
{snip_win_domainjoin}
}

if (-Not (Test-Path "C:\PaymentConnectors")) { ni -ItemType directory -Path C:\PaymentConnectors > $null }
if (-Not (Test-Path "C:\PlatformComponents")) { ni -ItemType directory -Path C:\PlatformComponents > $null }
if (-Not (Test-Path "C:\FillTask")) { ni -ItemType directory -Path C:\FillTask > $null }
if (-Not (Test-Path "C:\LIM")) { ni -ItemType directory -Path C:\LIM > $null }
if (-Not (Test-Path "C:\RRCService")) { ni -ItemType directory -Path C:\RRCService > $null }
if (-Not (Test-Path "C:\Connector")) { ni -ItemType directory -Path C:\Connector > $null }

# Downloading and installing MySQL ODBC Driver
Copy-S3Object -BucketName {s3_bucket} -Key "mysql-connector-odbc-5.3.4-winx64.msi" -LocalFile "$download\mysql.msi" > $null
if (Test-Path $download\mysql.msi){
msiexec /package "$download\mysql.msi" /quiet /passive /qn /norestart > $null
} else {
$errors += "MySQL ODBC Driver not found. "
}

# Getting latest code from S3
Copy-S3Object -BucketName {s3_bucket} -Key paymentconnectors.zip -LocalFile C:/TempDownloads/paymentconnectors.zip > $null
Copy-S3Object -BucketName {s3_bucket} -Key platformcomponents.zip -LocalFile C:/TempDownloads/platformcomponents.zip > $null
Copy-S3Object -BucketName {s3_bucket} -Key FillTaskWebservice.zip -LocalFile C:/TempDownloads/FillTaskWebservice.zip > $null
Copy-S3Object -BucketName {s3_bucket} -Key LIM-deploy.zip -LocalFile C:/TempDownloads/LIM.zip > $null
Copy-S3Object -BucketName {s3_bucket} -Key R4RRCService.zip -LocalFile C:/TempDownloads/RRCService.zip > $null
Copy-S3Object -BucketName {s3_bucket} -Key NZLumberjack.dll -LocalFile C:/TempDownloads/NZLumberjack.dll > $null

Invoke-RestMethod -Uri https://api.github.com/repos/Firmstep/FS-Payment-Connectors-PHP/zipball/{gitbranch} -Headers @{"Authorization" = "token {github}"} -OutFile C:/TempDownloads/Connector.zip
Invoke-RestMethod -Uri https://api.github.com/repos/Firmstep/FS-Common/zipball/{commonBranch} -Headers @{"Authorization" = "token {github}"} -OutFile C:/TempDownloads/Common.zip

Unzip $download\paymentconnectors.zip c:\PaymentConnectors
Unzip $download\platformcomponents.zip c:\PlatformComponents
Unzip $download\FillTaskWebservice.zip c:\FillTask
Unzip $download\LIM.zip c:\LIM
Unzip $download\RRCService.zip c:\RRCService
Unzip $download\Connector.zip $download\Connector
Unzip $download\Common.zip $download\Common
$gitConnectorName = ls $download\Connector\Firmstep-FS-Payment-Connectors-PHP*
$gitCommonName = ls $download\Common\Firmstep-FS-Common*
mv $gitConnectorName\* c:\Connector
mv $gitCommonName\nzlog\current\php\* c:\Connector\classes\Common
cp $download\NZLumberjack.dll -Destination c:\FillTask\bin
cp $download\NZLumberjack.dll -Destination c:\RRCService\bin
mv $download\mysql-connector-net\v2.0\MySql.Data.dll c:\LIM\bin
rm c:\Connector\public\test -recurse

# Iterate through all the subdirectories of PaymentConnectors, and create a webapp for each
$sites = ls C:\PaymentConnectors -Directory
foreach ($site in $sites) {
$newpath = "IIS:\Sites\Default Web Site\$site"
$oldpath = "C:\PaymentConnectors\$site"
ni $newpath -physicalPath $oldpath -type Application > $null
cp $download\NZLumberjack.dll -Destination "$oldpath\bin"
}

# Same with PlatformComponents
$sites = ls C:\PlatformComponents -Directory
foreach ($site in $sites) {
$newpath = "IIS:\Sites\Default Web Site\$site"
$oldpath = "C:\PlatformComponents\$site"
ni $newpath -physicalPath $oldpath -type Application > $null
}

ni "IIS:\Sites\Default Web Site\LIM" -physicalPath "C:\LIM" -type Application > $null
ni "IIS:\Sites\Default Web Site\FillTask" -physicalPath "C:\FillTask" -type Application > $null
ni "IIS:\Sites\Default Web Site\RRCService" -physicalPath "C:\RRCService" -type Application > $null
ni "IIS:\Sites\Default Web Site\Connector" -physicalPath "C:\Connector\public" -type Application > $null

# Create Build file in webroot folder
ni "C:/inetpub/wwwroot/build.txt" -type File -force -value "Build: {name} {build}"

# Create php IIS handler
Add-WebConfiguration "system.webserver/fastcgi" -value @{"FullPath" = "C:\php\php-cgi.exe"}
Add-WebConfiguration "system.webServer/fastCgi/application[@fullPath='C:\php\php-cgi.exe']/environmentVariables" -Value @{"Name" = "PHP_FCGI_MAX_REQUESTS"; Value = 100}
New-WebHandler -Name "PHP" -Path "*.php" -Verb 'GET,POST' -Modules FastCgiModule -scriptProcessor 'C:\PHP\php-cgi.exe' -ResourceType File

Add-WebConfigurationProperty -filter "system.webServer/rewrite/rules" -name "." -value @{name='Connector rewrite'}
Set-WebConfigurationProperty -filter "system.webServer/rewrite/rules/rule[@name='Connector rewrite']/match" -name "url" -value "^connector/([a-z]+)/(init|callback|statuscheck)"
Set-WebConfigurationProperty -filter "system.webServer/rewrite/rules/rule[@name='Connector rewrite']/action" -name "type" -value "Rewrite"
Set-WebConfigurationProperty -filter "system.webServer/rewrite/rules/rule[@name='Connector rewrite']/action" -name "url" -value "connector/index.php"

$s3base = "Configuration/{name}/{environment}"
Copy-S3Object -BucketName {s3_bucket} -Key "$s3base/log.xml" -LocalFile C:/Firmstep/log.xml > $null

Copy-S3Object -BucketName {s3_bucket} -Key "$s3base/LIM/log4net.config" -LocalFile C:/LIM/log4net.config > $null
Copy-S3Object -BucketName {s3_bucket} -Key "$s3base/LIM/lim.config" -LocalFile C:/LIM/lim.config > $null

Copy-S3Object -BucketName {s3_bucket} -Key "$s3base/PaymentConnectors/lumberjacknet.xml" -LocalFile C:/PaymentConnectors/lumberjack.xml > $null
Copy-S3Object -BucketName {s3_bucket} -Key "$s3base/PaymentConnectors/settings.config" -LocalFile C:/PaymentConnectors/settings.config > $null

Copy-S3Object -BucketName {s3_bucket} -Key "$s3base/RRCService/rrc.config" -LocalFile C:/RRCService/rrc.config > $null
Copy-S3Object -BucketName {s3_bucket} -Key "$s3base/RRCService/log4net.config" -LocalFile C:/RRCService/log4net.config | Out-Null
Copy-S3Object -BucketName {s3_bucket} -Key "$s3base/RRCService/lumberjack.xml" -LocalFile C:/RRCService/lumberjack.xml > $null

If ("{environment}" -eq "test") {
Copy-S3Object -BucketName {s3_bucket} -Key "$s3base/RRCService/firmsteptest_settings.json" -LocalFile C:/RRCService/firmsteptest_settings.json > $null
Copy-S3Object -BucketName {s3_bucket} -Key "$s3base/RRCService/customer_settings.json" -LocalFile C:/RRCService/customer_settings.json > $null
}
If ("{environment}" -eq "devops") {
Copy-S3Object -BucketName {s3_bucket} -Key "$s3base/RRCService/devops_settings.json" -LocalFile C:/RRCService/devops_settings.json > $null
}

Copy-S3Object -BucketName {s3_bucket} -Key "$s3base/FillTask/settings.config" -LocalFile C:/FillTask/settings.config > $null
Copy-S3Object -BucketName {s3_bucket} -Key "$s3base/FillTask/lumberjack.xml" -LocalFile C:/FillTask/lumberjack.xml > $null

Copy-S3Object -BucketName {s3_bucket} -Key "$s3base/PaymentConnectors/lumberjackphp.xml" -LocalFile C:/Connector/config/lumberjack.xml > $null
Copy-S3Object -BucketName {s3_bucket} -Key "$s3base/PaymentConnectors/common.php" -LocalFile C:/Connector/config/common.php > $null

Copy-S3Object -BucketName {s3_bucket} -Key PHP/cacert.pem -LocalFile C:/PHP/cacert.pem > $null
Copy-S3Object -BucketName {s3_bucket} -Key PHP/php-Component.ini -LocalFile C:/PHP/php.ini > $null
Copy-S3Object -BucketName {s3_bucket} -Key PHP/web.config -LocalFile C:/Connector/public/web.config > $null

#Setup RRC archive versions
$rrcList = Get-S3Object -BucketName {s3_bucket} -KeyPrefix RRCService/RRCService
foreach ($rrc in $rrcList) {
$file = $rrc.Key.Split('/')[1]
$version = $file -Replace '[^0-9]'
Copy-S3Object -BucketName {s3_bucket} -Key $rrc.Key -LocalFile C:/TempDownloads/$file > $null
Unzip $download\$file C:\RRCService\$version
cp C:\RRCService\* C:\RRCService\$version -include *.config,*.json,*.xml -exclude web.config
ni "IIS:\Sites\Default Web Site\RRCService\$version" -physicalPath C:\RRCService\$version -type Application > $null
}

# Setting up monitoring
if (-Not (Test-Path "C:\Program Files\Monitoring")) { ni -ItemType directory -Path C:\Program` Files\Monitoring > $null }
Copy-S3Object -BucketName {s3_bucket} -Key monitoring.zip -LocalFile C:/TempDownloads/monitoring.zip > $null
Unzip $download\monitoring.zip C:\Program` Files\Monitoring
C:\Program` Files\Monitoring\MonitoringAgent.exe install -url {monitoring_url} -token {monitoring_token} -appendid true -name {name} | Out-File -FilePath C:\monitorlogs.txt
Start-Service -name FSAgent

C:\Windows\system32\schtasks.exe /create /sc ONSTART /tn StatusCheck /tr "powershell.exe C:\Firmstep\ComponentsCheck.ps1 -environment {environment}" /RU System /RL HIGHEST /DELAY 0015:00
C:\Windows\system32\schtasks.exe /create /sc DAILY /st 00:05 /ri 60 /du 24:00 /tn UploadLogs /tr "powershell.exe C:\Firmstep\UploadLogs.ps1 -name {name} -environment {environment} -config C:\Firmstep\log.xml" /RU System /RL HIGHEST > $null
C:\Windows\system32\schtasks.exe /create /sc DAILY /st 00:02 /ri 2 /du 24:00 /tn SQSWatcher /tr "powershell.exe C:\Firmstep\SQSWatcher.ps1 -name {name} -environment {environment} -config C:\Firmstep\log.xml -scriptPath C:\Firmstep\UploadLogs.ps1" /RU System /RL HIGHEST > $null

rm $download -recurse
Stop-Computer
</powershell>