<powershell>

Set-ExecutionPolicy Unrestricted -Force

write-host "Configuring timezone"
tzutil.exe /s "{timezone}"

write-host "Creating default directories"
if (-Not (Test-Path "C:\TempDownloads")) { New-Item -ItemType directory -Path C:\TempDownloads | Out-Null }
if (-Not (Test-Path "C:\Firmstep")) { New-Item -ItemType directory -Path C:\Firmstep | Out-Null }

[Net.ServicePointManager]::SecurityProtocol = "tls12, tls11, tls"
$errors = ""

write-host "Downloading and installing 7-Zip"
(New-Object System.Net.WebClient).DownloadFile("http://www.7-zip.org/a/7z1514-x64.msi", "C:\TempDownloads\7zip.msi")
if (Test-Path C:\TempDownloads\7zip.msi) {
    msiexec /package "C:\TempDownloads\7zip.msi" /quiet /passive /qn /norestart | Out-Null
} else {
    $errors += "7-Zip not found. "
}

write-host "Downloading and installing Notepad++"
(New-Object System.Net.WebClient).DownloadFile("https://notepad-plus-plus.org/repository/6.x/6.9.2/npp.6.9.2.Installer.exe", "C:\TempDownloads\npp.exe")
if (Test-Path C:\TempDownloads\npp.exe) {
    C:\TempDownloads\npp.exe /Q /S | Out-Null
} else {
    $errors += "Notepad++ not found. "
}

write-host "Downloading and installing AWS CLI Tools"
(New-Object System.Net.WebClient).DownloadFile("https://s3.amazonaws.com/aws-cli/AWSCLI64.msi", "C:\TempDownloads\AWSCLI64.msi")
if (Test-Path C:\TempDownloads\AWSCLI64.msi) {
    msiexec /package "C:\TempDownloads\AWSCLI64.msi" /quiet /passive /qn /norestart | Out-Null
} else {
    $errors += "AWS CLI Tools not found. "
}

{snip_win_monitoring}

{snip_win_localadmin}

if ("{environment}" -eq "test") {
    {snip_win_domainjoin}
}

if ($errors) {
    Send-MailMessage -From "{name}@firmstep.com" -To "auckland-team@firmstep.com" -Subject "{name} Automation Error [{environment}]" -SmtpServer "smtp.firmstep.com" -Body "$errors"
}

# Run shared LIM setup script
if (-Not (Test-Path "C:\Firmstep")) { New-Item -ItemType directory -Path C:\Firmstep | Out-Null }
$r = Invoke-WebRequest -URI https://api.github.com/repos/Firmstep/FS-Automation/contents/shared/lim-common.ps1 -Headers @{"Authorization"="token {github}"}
$c = $r.Content | ConvertFrom-Json
$decoded = [System.Convert]::FromBase64String($c.content)
[IO.File]::WriteAllBytes("C:\Firmstep\lim-common.ps1",$decoded)
Invoke-Expression "C:\Firmstep\lim-common.ps1"
Remove-Item "C:\Firmstep\lim-common.ps1"

# Install environment-specific LIM config files
Copy-S3Object -AccessKey {s3_key} -SecretKey {s3_secret} -Region {s3_region} -BucketName {s3_bucket} -Key Configuration/{name}/{environment}/log4net.config -LocalFile C:/LIM/log4net.config | Out-Null
Copy-S3Object -AccessKey {s3_key} -SecretKey {s3_secret} -Region {s3_region} -BucketName {s3_bucket} -Key Configuration/{name}/{environment}/lim.config -LocalFile C:/LIM/lim.config | Out-Null

# Download folder permissions script and set it to run on next reboot
# (Can't run it during automation as the D: drive won't be attached)
$r = Invoke-WebRequest -URI https://api.github.com/repos/Firmstep/FS-Automation/contents/shared/lim-folder-permissions.ps1 -Headers @{"Authorization"="token {github}"}
$c = $r.Content | ConvertFrom-Json
$decoded = [System.Convert]::FromBase64String($c.content)
[IO.File]::WriteAllBytes("C:\Firmstep\lim-folder-permissions.ps1",$decoded)
C:\Windows\system32\schtasks.exe /create /sc ONSTART /tn LIMFolderPermissions /tr "powershell.exe C:\Firmstep\lim-folder-permissions.ps1 -NoLogo -NonInteractive -WindowStyle Hidden" /RU System

# Download FAM setup script and set it to run on next reboot
# (Can't run it during automation as the D: drive won't be attached)
$r = Invoke-WebRequest -URI https://api.github.com/repos/Firmstep/FS-Automation/contents/shared/DebugFAMSetup.ps1 -Headers @{"Authorization"="token {github}"}
$c = $r.Content | ConvertFrom-Json
$decoded = [System.Convert]::FromBase64String($c.content)
[IO.File]::WriteAllBytes("C:\Firmstep\DebugFAMSetup.ps1",$decoded)
C:\Windows\system32\schtasks.exe /create /sc ONSTART /tn FAMFolderPermissions /tr "powershell.exe C:\Firmstep\DebugFAMSetup.ps1 -NoLogo -NonInteractive -WindowStyle Hidden" /RU System

# Install debug FAM
Add-WindowsFeature -Name Web-Asp
Import-Module WebAdministration
if (-Not (Test-Path "C:\FAM")) { New-Item -ItemType directory -Path C:\FAM | Out-Null }
Copy-S3Object -AccessKey {s3_key} -SecretKey {s3_secret} -Region {s3_region} -BucketName {s3_bucket} -Key FAMASP.zip -LocalFile C:/TempDownloads/FAMASP.zip | Out-Null
c:\Program` Files\7-Zip\7z.exe x c:\TempDownloads\FAMASP.zip -o"C:\FAM"
c:\Windows\SysWOW64\regsvr32.exe C:\FAM\VBCorLib.dll
c:\Windows\SysWOW64\regsvr32.exe C:\FAM\FAMASP.dll
New-WebAppPool -Name "FAM"
Set-ItemProperty -Path IIS:\AppPools\FAM -Name managedRuntimeVersion -Value ''
Set-ItemProperty -Path IIS:\AppPools\FAM -Name enable32BitAppOnWin64 -Value "true"

# Webhooks installation/deployment
if (-Not (Test-Path "C:\TempDownloads")) { New-Item -ItemType directory -Path C:\TempDownloads | Out-Null }
write-host "Downloading and installing NET4.5"
(New-Object System.Net.WebClient).DownloadFile("https://download.microsoft.com/download/B/A/4/BA4A7E71-2906-4B2D-A0E1-80CF16844F5F/dotNetFx45_Full_setup.exe", "C:\TempDownloads\net45.exe")
C:\TempDownloads\net45.exe /Q /S | Out-Null

Copy-S3Object -AccessKey {s3_key} -SecretKey {s3_secret} -Region {s3_region} -BucketName {s3_bucket} -Key WebHooks.zip -LocalFile C:/TempDownloads/webhooks.zip | Out-Null
C:\Program` Files\7-Zip\7z.exe x C:\TempDownloads\webhooks.zip -oC:\Webhooks | Out-Null

Import-Module WebAdministration
New-Item "IIS:\Sites\Default Web Site\Webhooks" -physicalPath "C:\Webhooks" -type Application | Out-Null

# Create Build file in webroot folder
new-item "C:/inetpub/wwwroot/build.txt" -type File -force -value "Build: {name} {build}"

# Cleanup
Remove-Item "C:\Users\Default\Desktop\*.*"
Remove-Item "C:\TempDownloads" -Recurse -Force

Stop-Computer

</powershell>
