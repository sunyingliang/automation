#!/usr/bin/env bash

{snip_deb_sysctl}

# General commands
apt update -y
apt install -y apache2 libapache2-mod-php7.0
apt install -y php7.0-cli php7.0-mysql php7.0-curl php7.0-mbstring php7.0-xml
apt install -y awscli unzip
#apt upgrade -y

{snip_deb_security}

{snip_deb_timezone}

{snip_deb_swap}

# Download project from GitHub
curl -s -H "Authorization: token {github}" -o /tmp/{name}.tar -L https://api.github.com/repos/Firmstep/FS-Infrastructure/tarball/{gitbranch}
tar xf /tmp/{name}.tar -C /tmp
outfolder=$( ls -1c /tmp | grep Firmstep-FS-Infrastructure- | head -1)
mkdir -p /home/ubuntu/bin/{name}
cp -R /tmp/$outfolder/CronTab/* /home/ubuntu/bin/{name}/
rm -R /tmp/$outfolder
rm /tmp/{name}.tar

# Copy custom files from S3
export AWS_ACCESS_KEY_ID={s3_key}
export AWS_SECRET_ACCESS_KEY={s3_secret}
export AWS_DEFAULT_REGION={s3_region}
aws s3 cp s3://{s3_bucket}/Configuration/{name}/{environment}/common.php /home/ubuntu/bin/{name}/config/common.php

# Create build file
printf "Build: {name} {build}" > /var/www/html/build
rm /var/www/html/index.html
a2dismod autoindex -f

{snip_deb_monitoring}

# Setup permissions
chown -R ubuntu:ubuntu /home/ubuntu/bin
chmod -R 770 /home/ubuntu/bin

# Setup crontab
echo "*/15 * * * * sudo sh -c 'echo 1 > /proc/sys/vm/drop_caches'
*/30 * * * * sudo php /home/ubuntu/bin/{name}/job_scheduler.php
*/5 * * * * sudo php /home/ubuntu/bin/{name}/auto_reconciliation.php" >> mycron
crontab -u ubuntu mycron
rm mycron

# Setup rc.local (ubuntu on safe reboot)
sed -i -e '$i \nohup php /home/ubuntu/bin/{name}/job_scheduler.php &\n' /etc/rc.local

# Finally shut down the instance for AMI creation
sudo shutdown -h now
