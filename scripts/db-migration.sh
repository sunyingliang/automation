#!/usr/bin/env bash

{snip_deb_sysctl}

sudo debconf-set-selections <<< 'mysql-server mysql-server/root_password password root'
sudo debconf-set-selections <<< 'mysql-server mysql-server/root_password_again password root'

# General commands
apt update -y
apt install -y apache2 php7.0-cli libapache2-mod-php7.0 php7.0-curl php7.0-mcrypt php7.0-mysql php7.0-xml php7.0-soap mysql-server-5.7
apt install -y awscli unzip
#apt upgrade -y

{snip_deb_security}

{snip_deb_timezone}

{snip_deb_swap}

# Prepare directories
rm -r /var/www/*

# Set AWS S3 credentials
mkdir /root/.aws/
printf "[default]\naws_access_key_id = {s3_key}\naws_secret_access_key = {s3_secret}\nregion = {s3_region}" > /root/.aws/config

# Copy custom files from S3
aws s3 cp s3://{s3_bucket}/Configuration/DB-Migration/{environment}/common.php /var/www/{name}/config/common.php
aws s3 cp s3://{s3_bucket}/Configuration/Certificates/{environment}/certificate.txt /etc/apache2/certificate.txt
aws s3 cp s3://{s3_bucket}/Configuration/Certificates/{environment}/key.txt /etc/apache2/key.txt
aws s3 cp s3://{s3_bucket}/Configuration/Certificates/{environment}/chain.txt /etc/apache2/chain.txt
rm /root/.aws/config

# Download project from GitHub
curl -s -H "Authorization: token {github}" -o /tmp/{name}.tar -L https://api.github.com/repos/Firmstep/FS-Common/tarball/{gitbranch}
tar xf /tmp/{name}.tar -C /tmp
outfolder=$( ls -1c /tmp | grep Firmstep-FS-Common- | head -1)
mkdir -p /var/www/{name}/
cp -R /tmp/$outfolder/db-migration/* /var/www/{name}/
rm -R /tmp/$outfolder
rm /tmp/{name}.tar

# Generate VirtualHost File
echo "<VirtualHost *:80>
    DocumentRoot /var/www/{name}/public
    ErrorLog /var/log/apache2/error.log
    CustomLog /var/log/apache2/access.log combined
    <Directory /var/www/{name}/public>
        Options +Includes -Indexes +FollowSymLinks +MultiViews
        AllowOverride All
        Require all granted
		RewriteEngine on
        RewriteCond %{REQUEST_FILENAME} -f
        RewriteRule ^ - [L]
        RewriteCond %{REQUEST_URI} api
        RewriteRule ^api/(.*)$ api/index.php/\$1 [L]
        RewriteRule ^ index.php [L]
    </Directory>
</VirtualHost>" > /etc/apache2/sites-available/{name}.conf

# Setup mpm_prefork_module
echo "<IfModule mpm_prefork_module>
    StartServers              {solutions_startservers}
    MinSpareServers           {solutions_minspareservers}
    MaxSpareServers           {solutions_maxspareservers}
    MaxRequestWorkers         {solutions_maxrequestworkers}
    MaxConnectionsPerChild    {solutions_maxconnectionsperchild}
</IfModule>" > /etc/apache2/mods-available/mpm_prefork.conf

# Create build file
printf "Build: {name} {build}" > /var/www/{name}/public/build

{snip_deb_opcache}

{snip_deb_permissions}

# Finalise apache deployment environment
a2dissite 000-default
a2ensite {name}
a2enmod rewrite
a2enmod ssl
a2enmod headers
rm -f /etc/apache2/sites-available/000-default.conf
rm -f /etc/apache2/sites-available/default-ssl.conf
service apache2 restart

{snip_deb_dropcache}

# Setup rc.local (ubuntu on safe reboot)        
sed -i -e '$i \nohup php /var/www/{name}/script/database.php \n' /etc/rc.local

# Download migration files from target GitHub
curl -s -H "Authorization: token {github}" -o /tmp/{name}.tar -L https://api.github.com/repos/Firmstep/{migration_repo}/tarball/{migration_branch}
tar xf /tmp/{name}.tar -C /tmp
outfolder=$( ls -1c /tmp | grep Firmstep-{migration_repo}- | head -1)
mkdir /var/www/{name}/migration
cp -R /tmp/$outfolder/{migration_dir_master}/* /var/www/{name}/migration

if [[ "{migration_dir_slave}" != "{opt_start}migration_dir_slave{opt_end}" ]]; then
    mkdir -p /var/www/{name}/migration/children
    cp -R /tmp/$outfolder/{migration_dir_slave}/* /var/www/{name}/migration/children
fi

rm -R /tmp/$outfolder
rm /tmp/{name}.tar

# Trigger event from rc.local
if [ "{migration_sql}" != "{opt_start}migration_sql{opt_end}" ] && [ "{migration_dir_slave}" != "{opt_start}migration_dir_slave{opt_end}" ]; then
    sed -i -e '$i \nohup php /var/www/{name}/script/migration.php --interactive={migration_interactive} --autoskip=false --target="{migration_target}" --host="{migration_host}" --database="{migration_database}" --username="{migration_username}" --password="{migration_password}" --sql="{migration_sql}" &\n' /etc/rc.local
else
    sed -i -e '$i \nohup php /var/www/{name}/script/migration.php --interactive={migration_interactive} --autoskip=false --target="{migration_target}" --host="{migration_host}" --database="{migration_database}" --username="{migration_username}" --password="{migration_password}" &\n' /etc/rc.local
fi

# Setup crontab
echo "* * * * * sudo php /var/www/{name}/script/shutdown.php" >> mycron
crontab -u ubuntu mycron
rm mycron

# Finally shut down the instance for AMI creation
sudo shutdown -h now
