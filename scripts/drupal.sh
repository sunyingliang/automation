#!/usr/bin/env bash

{snip_deb_sysctl}

# General commands
sudo add-apt-repository ppa:ondrej/php
apt update -y
apt install -y apache2 php7.2-cli libapache2-mod-php7.2 php7.2-mysql php7.2-curl php7.2-soap php7.2-mbstring php7.2-gd php7.2-xml
apt install -y awscli unzip drush
#apt upgrade -y

# Upgrading Drush
cd ~
php -r "readfile('http://files.drush.org/drush.phar');" > drush
chmod +x ~/drush
cp /usr/bin/drush /usr/bin/drush_old
rm /usr/bin/drush
mv ~/drush /usr/bin/drush
/usr/bin/drush init

{snip_deb_security}

{snip_deb_timezone}

{snip_deb_swap}

# Prepare directories
rm -r /var/www/*

cd /var/www

mkdir -p /var/www/{name}

# index.php file for Drupal site
echo '<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <title>Firmstep Drupal Demo Site</title>
	<style type="text/css">
body {
    background-color:#FFFFFF;
    font:12pt Arial,Helvetica,Verdana sans-serif;
    margin:0;
    padding: 0;
}
header {
    width: 100%;
    background-color: #000;
    margin: 0 auto;
}
.top-bar {
    background-color: #000;
    height: 50px;
    width: 80%;
    margin: 0 auto;
    color: #fff;
}
.top-bar a{
    text-decoration:none;
	color:#fff;
	font-size:40px;
}
.contentdiv{
	width:80%;
	margin:0 auto;
	color:#000;
	padding-top:100px;
}
.contentdiv a{
	text-decoration:none;
	padding:10px;
	font-size:70px;
}
</style>
</head>
<body>
	<header>
        <div class="top-bar">
		 <a href="#">Firmstep</a>
        </div>
	</header>
	<div class="contentdiv">
		<p>Welcome to Firmstep Drupal demo site. Please click on the links to see Drupal 7/8 sites</p>
		<p><a target="_blank" href="https://drupal7.{base_domain}">Drupal7 Site</a></p>
		<a target="_blank" href="https://drupal8.{base_domain}">Drupal8 Site</a>
	</div>
</body>
</html>' > /var/www/{name}/index.php

# Install Drupal7 and Drupal8 into respective folders
drush dl drupal-7.56
drush dl drupal-8.3.4

# Copy Drupal to webroot
cp -a drupal-7.56/. /var/www/{name}7/
cp -a drupal-8.3.4/. /var/www/{name}8/
rm -r drupal-*

cd /var/www
# Download Drupal Modules from GitHub and Save them in respective sites
curl -s -H "Authorization: token {github}" -o /tmp/{name}.tar -L https://api.github.com/repos/Firmstep/FS-CMS-Modules/tarball/{gitbranch}
tar xf /tmp/{name}.tar -C /tmp
outfolder=$( ls -1c /tmp | grep Firmstep-FS-CMS-Modules- | head -1)
mkdir -p /var/www/{name}7/sites/all/modules/FAMLogin
cp -R /tmp/$outfolder/FAMModuleDrupal7/* /var/www/{name}7/sites/all/modules/FAMLogin/
mkdir -p /var/www/{name}7/sites/all/modules/RRCModule
cp -R /tmp/$outfolder/RRCModuleDrupal7/* /var/www/{name}7/sites/all/modules/RRCModule/
mkdir -p /var/www/{name}7/sites/all/modules/LIMEmailModule
cp -R /tmp/$outfolder/LIMEmailModuleDrupal7/* /var/www/{name}7/sites/all/modules/LIMEmailModule/

mkdir -p /var/www/{name}8/modules/FAMLogin
cp -R /tmp/$outfolder/FAMModuleDrupal8/* /var/www/{name}8/modules/FAMLogin/
mkdir -p /var/www/{name}8/modules/RRCModule
cp -R /tmp/$outfolder/RRCModuleDrupal8/* /var/www/{name}8/modules/RRCModule/
rm -R /tmp/$outfolder
rm /tmp/{name}.tar

# Install SMTP module for Drupal7 and Drupal8
cd /var/www/{name}7/
drush dl smtp-7.1*
cd /var/www/{name}8/
drush dl smtp-7.1*

# Copy any required custom files
export AWS_ACCESS_KEY_ID={s3_key}
export AWS_SECRET_ACCESS_KEY={s3_secret}
export AWS_DEFAULT_REGION={s3_region}
aws s3 cp s3://{s3_bucket}/Configuration/{name}/{environment}/db7_settings.php /var/www/{name}7/sites/default/db_settings.php
aws s3 cp s3://{s3_bucket}/Configuration/{name}/{environment}/db8_settings.php /var/www/{name}8/sites/default/db_settings.php
aws s3 cp s3://{s3_bucket}/Configuration/Certificates/{environment}/certificate.txt /etc/apache2/certificate.txt
aws s3 cp s3://{s3_bucket}/Configuration/Certificates/{environment}/key.txt /etc/apache2/key.txt
aws s3 cp s3://{s3_bucket}/Configuration/Certificates/{environment}/chain.txt /etc/apache2/chain.txt

# Generate VirtualHost File
echo "<VirtualHost *:80>
    RewriteEngine On
    RewriteRule (.*) https://%{SERVER_NAME}$1 [R,L]
</VirtualHost>"  > /etc/apache2/sites-available/000-default.conf

# VirtualHost drupal.conf
echo "<VirtualHost *:443>
    ServerName drupal.{base_domain}
    DocumentRoot /var/www/{name}
    ErrorLog /var/log/apache2/error.log
    CustomLog /var/log/apache2/access.log combined
    <Directory /var/www/{name}>
        Options +Includes -Indexes +FollowSymLinks +MultiViews
        AllowOverride All
    </Directory>
    Include sites-available/ssl.conf
</VirtualHost>
<VirtualHost *:443>
    ServerName drupal7.{base_domain}
    DocumentRoot /var/www/{name}7
    ErrorLog /var/log/apache2/error.log
    CustomLog /var/log/apache2/access.log combined
    <Directory /var/www/{name}7>
        Include sites-available/directory.conf
    </Directory>
    Include sites-available/ssl.conf
</VirtualHost>
<VirtualHost *:443>
    ServerName drupal8.{base_domain}
    DocumentRoot /var/www/{name}8
    ErrorLog /var/log/apache2/error.log
    CustomLog /var/log/apache2/access.log combined
    <Directory /var/www/{name}8>
        Include sites-available/directory.conf
    </Directory>
    Include sites-available/ssl.conf
</VirtualHost>" > /etc/apache2/sites-available/default-ssl.conf

echo "SSLEngine on
SSLCertificateFile /etc/apache2/certificate.txt
SSLCertificateKeyFile /etc/apache2/key.txt
SSLCertificateChainFile /etc/apache2/chain.txt" > /etc/apache2/sites-available/ssl.conf

echo "Options +Includes -Indexes +FollowSymLinks +MultiViews
AllowOverride All
RewriteEngine On
RewriteBase /
RewriteRule ^index\.php$ - [L]
RewriteCond %{REQUEST_FILENAME} !-f
RewriteCond %{REQUEST_FILENAME} !-d
RewriteRule . /index.php [L]" > /etc/apache2/sites-available/directory.conf

# Creating and setting up configurations for Drupal7
cd /var/www/{name}7/sites/default
cp default.settings.php settings.php
echo "require __DIR__ . '/db_settings.php';" >> settings.php
mkdir files

# Creating and setting up configurations for Drupal8
cd /var/www/{name}8/sites/default
cp default.settings.php settings.php
echo "require __DIR__ . '/db_settings.php';" >> settings.php
mkdir files
mkdir sync

# Create build files
printf "Build: {name} {build}" > /var/www/{name}/build
printf "Build: {name} {build}" > /var/www/{name}7/build
printf "Build: {name} {build}" > /var/www/{name}8/build

{snip_deb_opcache}

{snip_deb_monitoring}

{snip_deb_permissions}

# Finalise apache deployment environment
a2ensite default-ssl
a2enmod rewrite
a2enmod ssl
service apache2 restart

# Put Drupal7 site in maintainance mode and take backup and update
cd /var/www/{name}7
drush vset --exact maintenance_mode 1
drush cache-clear all
drush pm-update
drush vset --exact maintenance_mode 0
drush cache-clear all

# Put Drupal8 site in maintainance mode and take backup and update
cd /var/www/{name}8
drush sset system.maintenance_mode 1
drush cr
drush up drupal
drush sset system.maintenance_mode 0
drush cr

{snip_deb_dropcache}

# Finally shut down the instance for AMI creation
sudo shutdown -h now
