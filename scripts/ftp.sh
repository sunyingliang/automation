#!/usr/bin/env bash

{snip_deb_sysctl}

# General commands
export DEBIAN_FRONTEND=noninteractive
apt update -y
apt install -y apache2 libapache2-mod-php7.0
apt install -y cron awscli php7.0-cli php7.0-mysql php7.0-curl vsftpd libpam-mysql iptables-persistent
#apt upgrade -y

{snip_deb_security}

{snip_deb_timezone}

{snip_deb_swap}

# libpam-mysql fix
wget -P /tmp http://www.dinofly.com/files/linux/libpam-mysql_0.7~RC1-4ubuntu3_amd64.deb
dpkg -i /tmp/libpam-mysql_0.7~RC1-4ubuntu3_amd64.deb

# Copy custom files from S3
export AWS_ACCESS_KEY_ID={s3_key}
export AWS_SECRET_ACCESS_KEY={s3_secret}
export AWS_DEFAULT_REGION={s3_region}
aws s3 cp s3://{s3_bucket}/Configuration/{name}/{environment}/common.php /home/ubuntu/bin/{name}/config/common.php
mkdir -p /etc/ssl/private
aws s3 cp s3://{s3_bucket}/Configuration/{name}/{environment}/vsftpd.pem /etc/ssl/private/vsftpd.pem

# Download project from GitHub
curl -s -H "Authorization: token {github}" -o /tmp/FS-Solutions.tar -L https://api.github.com/repos/Firmstep/FS-Solutions/tarball/{gitbranch}
tar xf /tmp/FS-Solutions.tar -C /tmp
outfolder=$( ls -1c /tmp | grep Firmstep-FS-Solutions- | head -1)
cp -R /tmp/$outfolder/{name} /home/ubuntu/bin
rm -R /tmp/$outfolder
rm /tmp/FS-Solutions.tar

# Add VSFTPD user
useradd --home /home/vsftpd --gid nogroup -m --shell /bin/false vsftpd

# VSFTPD config file
cp /etc/vsftpd.conf /etc/vsftpd.conf_orig
cat /dev/null > /etc/vsftpd.conf
echo "listen=YES
anonymous_enable=NO
local_enable=YES
write_enable=YES
local_umask=022
dirmessage_enable=YES
xferlog_enable=YES
port_enable=YES
connect_from_port_20=YES
nopriv_user=vsftpd
chroot_local_user=YES
secure_chroot_dir=/var/run/vsftpd
pam_service_name=vsftpd
guest_enable=YES
guest_username=vsftpd
local_root=/home/vsftpd/\$USER
user_sub_token=\$USER
virtual_use_local_privs=YES
user_config_dir=/etc/vsftpd_user_conf
pasv_enable=Yes
pasv_min_port={ftp_min_port}
pasv_max_port={ftp_max_port}
pasv_address={ftp_address}
pasv_addr_resolve=YES" > /etc/vsftpd.conf
mkdir /etc/vsftpd_user_conf

iptables -I INPUT -p tcp --destination-port {ftp_min_port}:{ftp_max_port_is} -j ACCEPT
ufw allow {ftp_min_port}:{ftp_max_port_is}/tcp

sudo netfilter-persistent save

# PAM config files for VSFTPD
cp /etc/pam.d/vsftpd /etc/pam.d/vsftpd_orig
cat /dev/null > /etc/pam.d/vsftpd
echo "auth required pam_mysql.so user={pam_mysql_user} passwd={pam_mysql_pass} host={pam_mysql} db={pam_mysql_db} table={pam_mysql_table} usercolumn=name passwdcolumn=ftp_pass crypt=3
account required pam_mysql.so user={pam_mysql_user} passwd={pam_mysql_pass} host={pam_mysql} db={pam_mysql_db} table={pam_mysql_table} usercolumn=name passwdcolumn=ftp_pass crypt=3" > /etc/pam.d/vsftpd

# Restart Vsftpd service
/etc/init.d/vsftpd restart

# Enable SSL/TLS
cp /etc/vsftpd.conf /etc/vsftpd-es.conf
sed -i -e 's/^pasv_min_port={ftp_min_port}$/pasv_min_port={ftp_min_port_es}/' /etc/vsftpd-es.conf
sed -i -e 's/^pasv_max_port={ftp_max_port}$/pasv_max_port={ftp_max_port_es}/' /etc/vsftpd-es.conf
echo "
listen_port={listen_port_es}
ftp_data_port={ftp_data_port_es}
ssl_enable=YES
allow_anon_ssl=YES
force_local_logins_ssl=YES
force_local_data_ssl=YES
ssl_tlsv1=YES
ssl_sslv2=YES
ssl_sslv3=YES
ssl_ciphers=TLSv1.2
require_ssl_reuse=NO
rsa_cert_file=/etc/ssl/private/vsftpd.pem" >> /etc/vsftpd-es.conf

cp /etc/vsftpd.conf /etc/vsftpd-is.conf
sed -i -e 's/^pasv_min_port={ftp_min_port}$/pasv_min_port={ftp_min_port_is}/' /etc/vsftpd-is.conf
sed -i -e 's/^pasv_max_port={ftp_max_port}$/pasv_max_port={ftp_max_port_is}/' /etc/vsftpd-is.conf
echo "
listen_port={listen_port_is}
ftp_data_port={ftp_data_port_is}
ssl_enable=YES
allow_anon_ssl=YES
force_local_logins_ssl=YES
force_local_data_ssl=YES
ssl_tlsv1=YES
ssl_sslv2=YES
ssl_sslv3=YES
ssl_ciphers=TLSv1.2
require_ssl_reuse=NO
rsa_cert_file=/etc/ssl/private/vsftpd.pem
implicit_ssl=YES" >> /etc/vsftpd-is.conf

# Create build file
printf "Build: {name} {build}" > /var/www/html/build
rm /var/www/html/index.html
a2dismod autoindex -f

{snip_deb_monitoring}

# Setup permissions
chown -R ubuntu:ubuntu /home/ubuntu/bin
chmod -R 770 /home/ubuntu/bin

# Make data directory for mountpoint
mkdir /data

# Setup crontab
echo "*/15 * * * * sudo sh -c 'echo 1 > /proc/sys/vm/drop_caches'
{sync_cronkey} sudo php /home/ubuntu/bin/{name}/script/syncFiles.php
{migrate_cronkey} sudo php /home/ubuntu/bin/{name}/script/migrate.php
* * * * * sudo php /home/ubuntu/bin/{name}/script/checkUser.php" >> mycron
crontab -u ubuntu mycron
rm mycron

# Configure sftp
apt-get install -y libnss-mysql-bg
sed -i -e '/^passwd/s/compat/compat mysql/g' /etc/nsswitch.conf
sed -i -e '/^group/s/compat/compat mysql/g' /etc/nsswitch.conf
sed -i -e '/^shadow/s/compat/compat mysql/g' /etc/nsswitch.conf

echo "getpwnam    SELECT name,'x',uid,gid,name,'/data','/bin/bash' \
            FROM {pam_mysql_table} \
            WHERE name='%1\$s' \
            LIMIT 1
getpwuid    SELECT name,'x',uid,gid,name,'/data','/bin/bash' \
            FROM {pam_mysql_table} \
            WHERE uid='%1\$u' \
            LIMIT 1
getspnam    SELECT name,ftp_pass,'',0,0,7,-1,-1,'A' \
            FROM {pam_mysql_table} \
            WHERE name='%1\$s' \
            LIMIT 1
getpwent    SELECT name,'x',uid,gid,name,'/data','/bin/bash' \
            FROM {pam_mysql_table}
getspent    SELECT name,ftp_pass,'',0,0,7,-1,-1,'A' \
            FROM {pam_mysql_table}
getgrnam    SELECT name,password,gid \
            FROM groups \
            WHERE name='%1\$s' \
            LIMIT 1
getgrgid    SELECT name,password,gid \
            FROM groups \
            WHERE gid='%1\$u' \
            LIMIT 1
getgrent    SELECT name,password,gid \
            FROM groups
memsbygid   SELECT username \
            FROM grouplist \
            WHERE gid='%1\$u'
gidsbymem   SELECT gid \
            FROM grouplist \
            WHERE username='%1\$s'

host        {pam_mysql}
database    {pam_mysql_db}
username    {pam_mysql_user}
password    {pam_mysql_pass}
" > /etc/libnss-mysql.cfg

sed -i -e '/^username/s/nss-root/{pam_mysql_user}/g' /etc/libnss-mysql-root.cfg
sed -i -e '/^password/s/rootpass/{pam_mysql_pass}/g' /etc/libnss-mysql-root.cfg

echo "account sufficient      pam_mysql.so config_file=/etc/pam-mysql.conf
account required        pam_unix.so" > /etc/pam.d/common-account
echo "session sufficient      pam_mysql.so config_file=/etc/pam-mysql.conf
session required        pam_unix.so" > /etc/pam.d/common-session
echo "auth    sufficient      pam_mysql.so config_file=/etc/pam-mysql.conf
auth    required        pam_unix.so nullok_secure" > /etc/pam.d/common-auth
echo "password        sufficient      pam_mysql.so config_file=/etc/pam-mysql.conf
password        required        pam_unix.so nullok obscure sha512" > /etc/pam.d/common-password
echo "users.host 		    = {pam_mysql}
users.database 		= {pam_mysql_db}
users.db_user 		= {pam_mysql_user}
users.db_passwd		= {pam_mysql_pass}
users.table 		    = {pam_mysql_table}
users.user_column 	= name
users.password_column = ftp_pass
users.password_crypt 	= 3" > /etc/pam-mysql.conf

groupadd -r -g 10000 sftp
sed -i -e '/^PasswordAuthentication/s/no/yes/g' /etc/ssh/sshd_config
sed -i -e '/^Subsystem/s/sftp.*/sftp internal-sftp/g' /etc/ssh/sshd_config
echo "Match Group sftp
ChrootDirectory /sftp/%u
ForceCommand internal-sftp
PermitTunnel no
AllowAgentForwarding no
AllowTcpForwarding no
X11Forwarding no" >> /etc/ssh/sshd_config
service ssh restart

# Setup rc.local (ubuntu on safe reboot)
sed -i -e '$i \nohup sudo mount /dev/xvdf /data &\n' /etc/rc.local
sed -i -e '$i \nohup netfilter-persistent reload &\n' /etc/rc.local
sed -i -e '$i \nohup bash -c "sleep 180 && sudo /etc/init.d/vsftpd restart" &\n' /etc/rc.local
sed -i -e '$i \nohup sleep 190 && sudo /usr/sbin/vsftpd /etc/vsftpd-es.conf &\n' /etc/rc.local
sed -i -e '$i \nohup sleep 200 && sudo /usr/sbin/vsftpd /etc/vsftpd-is.conf &\n' /etc/rc.local
sed -i -e '$i \nohup sleep 210 && service ssh restart &\n' /etc/rc.local
# Add public ip address in hosts
if [ "{environment}" = "test" ]; then
    cp /etc/hosts /etc/hosts.orig
    sed -i -e '$i rm /etc/hosts && sed "2i $(curl http://169.254.169.254/latest/meta-data/public-ipv4) {ftp_address}" /etc/hosts.orig > /etc/hosts \n' /etc/rc.local
fi

# Finally shut down the instance for AMI creation
sudo shutdown -h now
