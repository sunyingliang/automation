<powershell>

function PostSlackMessage
{
	param([string]$message)
	$json = @{"channel"="auckland-bot"; "username"="LIMTestHarness"; "text"=$message; "icon_emoji"=":computer:"} | ConvertTo-Json
	Invoke-WebRequest -Method POST -Uri https://hooks.slack.com/services/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 -Body $json
}

Set-ExecutionPolicy Unrestricted -Force

write-host "Configuring timezone"
tzutil.exe /s "{timezone}"

write-host "Creating default directories"
if (-Not (Test-Path "C:\TempDownloads")) { New-Item -ItemType directory -Path C:\TempDownloads | Out-Null }
if (-Not (Test-Path "C:\Firmstep")) { New-Item -ItemType directory -Path C:\Firmstep | Out-Null }

[Net.ServicePointManager]::SecurityProtocol = "tls12, tls11, tls"
$errors = ""

write-host "Downloading and installing 7-Zip"
(New-Object System.Net.WebClient).DownloadFile("http://www.7-zip.org/a/7z1514-x64.msi", "C:\TempDownloads\7zip.msi")
if (Test-Path C:\TempDownloads\7zip.msi) {
    msiexec /package "C:\TempDownloads\7zip.msi" /quiet /passive /qn /norestart | Out-Null
} else {
    $errors += "7-Zip not found. "
}

write-host "Downloading and installing Notepad++"
(New-Object System.Net.WebClient).DownloadFile("https://notepad-plus-plus.org/repository/6.x/6.9.2/npp.6.9.2.Installer.exe", "C:\TempDownloads\npp.exe")
if (Test-Path C:\TempDownloads\npp.exe) {
    C:\TempDownloads\npp.exe /Q /S | Out-Null
} else {
    $errors += "Notepad++ not found. "
}

write-host "Downloading and installing AWS CLI Tools"
(New-Object System.Net.WebClient).DownloadFile("https://s3.amazonaws.com/aws-cli/AWSCLI64.msi", "C:\TempDownloads\AWSCLI64.msi")
if (Test-Path C:\TempDownloads\AWSCLI64.msi) {
    msiexec /package "C:\TempDownloads\AWSCLI64.msi" /quiet /passive /qn /norestart | Out-Null
} else {
    $errors += "AWS CLI Tools not found. "
}

{snip_win_monitoring}

{snip_win_localadmin}

if ("{environment}" -eq "test") {
    {snip_win_domainjoin}
}

write-host "Downloading and installing SSMS"
(New-Object System.Net.WebClient).DownloadFile("https://download.microsoft.com/download/4/2/A/42A5A62F-9290-45CB-84CF-6A4E17888FDE/SQLManagementStudio_x64_ENU.exe", "C:\TempDownloads\ssms.exe")
if (Test-Path C:\TempDownloads\ssms.exe){
    C:\TempDownloads\ssms.exe /QUIET /IACCEPTSQLSERVERLICENSETERMS /FEATURES=SSMS /ACTION=Install | Out-Null
} else {
    $errors += "SSMS not found. "
}

write-host "Downloading and installing SQLEXPR_x64_ENU.exe" 
(New-Object System.Net.WebClient).DownloadFile("https://download.microsoft.com/download/0/4/B/04BE03CD-EAF3-4797-9D8D-2E08E316C998/SQLEXPR_x64_ENU.exe", "C:\TempDownloads\SQLEXPR_x64_ENU.exe") 
if (Test-Path C:\TempDownloads\SQLEXPR_x64_ENU.exe) { 
    C:\TempDownloads\SQLEXPR_x64_ENU.exe /Q /Action=Install /IACCEPTSQLSERVERLICENSETERMS /FEATURES=SQLEngine,BC,Conn /InstanceName=MSSQLSERVER /SECURITYMODE=SQL "/SAPWD={adminpassword}" /TCPENABLED=1 /SQLSVCACCOUNT=SYSTEM | Out-Null 
} else { 
    $errors += "SQLEXPR_x64_ENU not found. " 
} 

if ($errors) {
    Send-MailMessage -From "{name}@firmstep.com" -To "auckland-team@firmstep.com" -Subject "{name} Automation Error [{environment}]" -SmtpServer "smtp.firmstep.com" -Body "$errors"
}

Import-Module ServerManager
Add-WindowsFeature -Name Web-Common-Http,Web-Net-Ext,Web-CGI,Web-ISAPI-Ext,Web-ISAPI-Filter,Web-Http-Logging,Web-Filtering,Web-Performance,Web-Mgmt-Console,Web-Mgmt-Compat,WAS -IncludeAllSubFeature | Out-Null
C:\Windows\Microsoft.NET\Framework\v4.0.30319\aspnet_regiis.exe -i | Out-Null

write-host "Getting latest code from S3"
Copy-S3Object -AccessKey {s3_key} -SecretKey {s3_secret} -Region {s3_region} -BucketName {s3_bucket} -Key LIMTestHarness.zip -LocalFile C:/TempDownloads/LIMTestHarness.zip | Out-Null

if (-Not (Test-Path "C:\MvcApplication")) { New-Item -ItemType directory -Path C:\MvcApplication | Out-Null }
if (-Not (Test-Path "C:\inetpub\wwwroot\northgatewsdl")) { New-Item -ItemType directory -Path C:\inetpub\wwwroot\northgatewsdl | Out-Null }
c:\Program` Files\7-Zip\7z.exe x c:\TempDownloads\LIMTestHarness.zip -oc:\MvcApplication | Out-Null
Move-Item c:\MvcApplication\northgatewsdl\*.wsdl c:\inetpub\wwwroot\northgatewsdl
Move-Item c:\MvcApplication\*.wsdl c:\inetpub\wwwroot

# Create a webapp
Import-Module WebAdministration
New-Item "IIS:\Sites\Default Web Site\MvcApplication" -physicalPath "C:\MvcApplication" -type Application | Out-Null

# Download and install SSL certificate
Copy-S3Object -AccessKey {s3_key} -SecretKey {s3_secret} -Region {s3_region} -BucketName fs-deploy -Key Configuration/Certificates/{environment}/certificate.pfx -LocalFile C:\TempDownloads\certificate.pfx
C:\Windows\System32\certutil.exe -f -p {sslcertpassword} -importpfx "C:\TempDownloads\certificate.pfx"
$cert = Get-ChildItem -Path Cert:\LocalMachine\My | Select-Object -First 1
New-WebBinding -Name "Default Web Site" -IP "*" -Port 443 -Protocol https
New-Item -Path IIS:\SslBindings\0.0.0.0!443 -Value $cert

# Create Build file in webroot folder
new-item "C:\inetpub\wwwroot\build.txt" -type File -force -value "Build: {name} {build}"

#Verify that SQL Server installation has completed, then create blank database
$connectionString = "Server=localhost;uid=sa;pwd={adminpassword};"
$connectCount = 0
$databaseName = "LimTestHarness"

While (1) {
	Try {
		$conn = New-Object System.Data.SQLClient.SQLConnection($connectionString)
		$conn.Open()
		Break
	} Catch {
		# 10m timeout, check once per 10s
		$connectCount++
		Start-Sleep -s (10)
		if ($connectCount -eq 60) {
			PostSlackMessage "LimTestHarness SQL Server connection failed - automation aborted!"
			Exit
		}
		else {
			PostSlackMessage "LIMTestHarness SQL Server connection error: $($_.Exception.Message), retrying in 10 sec"
		}			
	}
}
$cmd = New-Object System.Data.SQLClient.SQLCommand("CREATE DATABASE $databaseName", $conn)
$cmd.ExecuteNonQuery()

$user="{sqlUser}"
$password = "{sqlPassword}"

$cmd = New-Object System.Data.SQLClient.SQLCommand("Use $databaseName; CREATE LOGIN $user WITH PASSWORD = '$password', CHECK_POLICY=OFF", $conn)
$cmd.ExecuteNonQuery()
$cmd = New-Object System.Data.SQLClient.SQLCommand("Use $databaseName; CREATE USER $user FOR LOGIN $user", $conn)
$cmd.ExecuteNonQuery()
$cmd = New-Object System.Data.SQLClient.SQLCommand("exec sp_addrolemember 'db_owner', '$user'", $conn)
$cmd.ExecuteNonQuery()
$conn.close();

$conn = New-Object System.Data.SQLClient.SQLConnection("Server=127.0.0.1;Database=$databaseName;User Id=$user;Password=$password;")
$conn.Open()
$sql = [Io.File]::ReadAllText("C:/MvcApplication/Scripts/dbsetup.sql");
$queries = [regex]::Split($sql,"^\s*GO\s*","Multiline")
ForEach ($query in $queries) {
	if (-not [string]::IsNullOrWhiteSpace($query)) {
		$cmd = New-Object System.Data.SqlClient.SqlCommand($query,$conn)
		$cmd.ExecuteNonQuery()
	}
}
$conn.Close()

# Open port for SQL Server
netsh advfirewall firewall add rule name="Microsoft SQL Server" dir=in action=allow protocol=TCP localport=1433

# Cleanup
Remove-Item "C:\TempDownloads" -Recurse -Force
Remove-Item "C:\MvcApplication\Scripts" -Recurse -Force

Stop-Computer

</powershell>