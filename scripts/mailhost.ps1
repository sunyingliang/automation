<powershell>

Set-ExecutionPolicy Unrestricted -Force

write-host "Configuring timezone"
tzutil.exe /s "{timezone}"

write-host "Creating default directories"
if (-Not (Test-Path "C:\TempDownloads")) { New-Item -ItemType directory -Path C:\TempDownloads | Out-Null }
if (-Not (Test-Path "C:\Firmstep")) { New-Item -ItemType directory -Path C:\Firmstep | Out-Null }

[Net.ServicePointManager]::SecurityProtocol = "tls12, tls11, tls"
$errors = ""

write-host "Downloading and installing 7-Zip"
(New-Object System.Net.WebClient).DownloadFile("http://www.7-zip.org/a/7z1514-x64.msi", "C:\TempDownloads\7zip.msi")
if (Test-Path C:\TempDownloads\7zip.msi) {
    msiexec /package "C:\TempDownloads\7zip.msi" /quiet /passive /qn /norestart | Out-Null
} else {
    $errors += "7-Zip not found. "
}

write-host "Downloading and installing Notepad++"
(New-Object System.Net.WebClient).DownloadFile("https://notepad-plus-plus.org/repository/6.x/6.9.2/npp.6.9.2.Installer.exe", "C:\TempDownloads\npp.exe")
if (Test-Path C:\TempDownloads\npp.exe) {
    C:\TempDownloads\npp.exe /Q /S | Out-Null
} else {
    $errors += "Notepad++ not found. "
}

write-host "Downloading and installing AWS CLI Tools"
(New-Object System.Net.WebClient).DownloadFile("https://s3.amazonaws.com/aws-cli/AWSCLI64.msi", "C:\TempDownloads\AWSCLI64.msi")
if (Test-Path C:\TempDownloads\AWSCLI64.msi) {
    msiexec /package "C:\TempDownloads\AWSCLI64.msi" /quiet /passive /qn /norestart | Out-Null
} else {
    $errors += "AWS CLI Tools not found. "
}

{snip_win_monitoring}

{snip_win_localadmin}

if ("{environment}" -eq "test") {
    {snip_win_domainjoin}
}

if ($errors) {
    Send-MailMessage -From "{name}@firmstep.com" -To "auckland-team@firmstep.com" -Subject "{name} Automation Error [{environment}]" -SmtpServer "smtp.firmstep.com" -Body "$errors"
}

Add-WindowsFeature Telnet-Client

write-host "Downloading and installing HMail"
(New-Object System.Net.WebClient).DownloadFile("https://www.hmailserver.com/download_file?downloadid=254", "C:\TempDownloads\hmail.exe")
C:\TempDownloads\hmail.exe /verysilent | Out-Null
Stop-Service hMailServer -ErrorAction SilentlyContinue

$r = Invoke-WebRequest -URI https://api.github.com/repos/Firmstep/FS-Automation/contents/shared/createmailhost.sql -Headers @{"Authorization"="token {github}"}
$c = $r.Content | ConvertFrom-Json
$decoded = [System.Convert]::FromBase64String($c.content)
[IO.File]::WriteAllBytes("C:\TempDownloads\createmailhost.sql",$decoded)

$md5 = new-object -TypeName System.Security.Cryptography.MD5CryptoServiceProvider
$utf8 = new-object -TypeName System.Text.UTF8Encoding
$md5hash = [System.BitConverter]::ToString($md5.ComputeHash($utf8.GetBytes("{hmail_password}"))).Replace("-","").ToLower()

# Create ini file
$hMailConfig = @"
[Directories]
ProgramFolder=C:\Program Files (x86)\hMailServer
DatabaseFolder=
DataFolder=C:\Program Files (x86)\hMailServer\Data
LogFolder=C:\Program Files (x86)\hMailServer\Logs
TempFolder=C:\Program Files (x86)\hMailServer\Temp
EventFolder=C:\Program Files (x86)\hMailServer\Events
[GUILanguages]
ValidLanguages=english
[Security]
AdministratorPassword=${md5hash}
[Database]
Type=MSSQL
Username={database_user}
Password={database_password}
PasswordEncryption=0
Port=0
Server={database_server}
Database={database_name}
Internal=0
"@

[IO.File]::WriteAllText("C:\Program Files (x86)\hMailServer\Bin\hMailServer.INI",$hMailConfig)

write-host "Creating HMail database"
$connectionString = "Server={database_server};Database=master;uid={database_user};pwd={database_password};"

$connection = New-Object System.Data.SQLClient.SQLConnection($connectionString)
$connection.Open()
$command = New-Object System.Data.SQLClient.SQLCommand("ALTER DATABASE {database_name} SET SINGLE_USER WITH ROLLBACK IMMEDIATE; DROP DATABASE {database_name};", $connection)
$command.ExecuteNonQuery()
$command = New-Object System.Data.SQLClient.SQLCommand("CREATE DATABASE {database_name}", $connection)
$command.ExecuteNonQuery()
$connection.Close()

$connectionString = "Server={database_server};Database={database_name};uid={database_user};pwd={database_password};"

$commandfull = get-content -path C:\TempDownloads\createmailhost.sql | ? {$_.trim() -ne "" } | Out-String
$commandarray = $commandfull -split "go`r`n"

$connection = New-Object System.Data.SQLClient.SQLConnection($connectionString)
$connection.Open()

foreach ($commandtext in $commandarray)
{
    $commandtext
    $command = New-Object System.Data.SQLClient.SQLCommand($commandtext, $connection)
    $command.ExecuteNonQuery()
}

$command = New-Object System.Data.SQLClient.SQLCommand("UPDATE hm_settings SET settingstring = 'smtp.{hmail_domain}' WHERE settingname='smtprelayer'", $connection)
$command.ExecuteNonQuery()
$command = New-Object System.Data.SQLClient.SQLCommand("UPDATE hm_settings SET settingstring = 'imap.{hmail_domain}' WHERE settingname='hostname'", $connection)
$command.ExecuteNonQuery()

$connection.Close()

Start-Service hMailServer 

$hm = New-Object -ComObject hMailServer.Application
$hm.Authenticate("Administrator", "{hmail_password}") | Out-Null

$domainname = "{hmail_domain}"
$domain = $hm.Domains.Add()
$domain.Name = $domainname
$domain.Active = $true
$domain.Postmaster = "admin@{hmail_domain}"
$domain.Save()

$hmdom = $hm.Domains.ItemByName("$domainname")

$newAcct = $hmdom.Accounts.Add()
$newAcct.Address = "admin@{hmail_domain}"
$newAcct.Active = $true
$newAcct.Password = "{hmail_password}"
$newAcct.Save()

for ($i = 1; $i -le 5; $i++) {
    $newAcct = $hmdom.Accounts.Add()
    $newAcct.Address = "test$i@{hmail_domain}"
    $newAcct.Active = $true
    $newAcct.Password = "{accountpw}$i"
    $newAcct.Save()
}

# Enable mail protocols in windows firewall
netsh advfirewall firewall add rule name=IMAP dir=in action=allow protocol=TCP localport=143
netsh advfirewall firewall add rule name=IMAPS dir=in action=allow protocol=TCP localport=993
netsh advfirewall firewall add rule name=SMTP dir=in action=allow protocol=TCP localport=25

# Cleanup
Remove-Item "C:\Users\Default\Desktop\*.*"
Remove-Item "C:\TempDownloads" -Recurse -Force

Stop-Computer

</powershell>
