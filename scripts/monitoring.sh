#!/usr/bin/env bash

{snip_deb_sysctl}

# General commands
apt update -y
apt install -y apache2 php7.0-cli libapache2-mod-php7.0 php7.0-mysql php7.0-curl php7.0-mbstring php7.0-xml
apt install -y awscli unzip composer
#apt upgrade -y

{snip_deb_security}

{snip_deb_timezone}

{snip_deb_swap}

# Prepare directories
rm -r /var/www/*

# Download code from GitHub
curl -s -H "Authorization: token {github}" -o /tmp/{name}.tar -L https://api.github.com/repos/Firmstep/FS-Monitoring/tarball/{gitbranch}
tar xf /tmp/{name}.tar -C /tmp
outfolder=$( ls -1c /tmp | grep Firmstep-FS-Monitoring- | head -1)
mkdir -p /var/www/{name}
cp -R /tmp/$outfolder/PHP/* /var/www/{name}/
rm -R /tmp/$outfolder
rm /tmp/{name}.tar

# Copy any required custom files
export AWS_ACCESS_KEY_ID={s3_key}
export AWS_SECRET_ACCESS_KEY={s3_secret}
export AWS_DEFAULT_REGION={s3_region}
aws s3 cp s3://{s3_bucket}/Configuration/{name}/{environment}/common.php /var/www/{name}/config/common.php

# Install the project dependencies
sudo -H -u ubuntu bash -c 'sudo composer install --working-dir=/var/www/{name}'

# VirtualHost tracking.conf
echo "<VirtualHost *:80>
    DocumentRoot /var/www/{name}/public
    ErrorLog /var/log/apache2/error.log
    CustomLog /var/log/apache2/access.log combined
    <Directory /var/www/{name}/public>
        Options +Includes -Indexes +FollowSymLinks
        AllowOverride All
        Require all granted
    </Directory>
</VirtualHost>" > /etc/apache2/sites-available/{name}.conf

echo "<IfModule mpm_prefork_module>
    StartServers              5
    MinSpareServers           5
    MaxSpareServers           25
    MaxRequestWorkers         150
    MaxConnectionsPerChild    10000
</IfModule>" > /etc/apache2/mods-available/mpm_prefork.conf

# Create build file
printf "Build: {name} {build}" > /var/www/{name}/public/build

{snip_deb_opcache}

#{snip_deb_monitoring}

{snip_deb_permissions}

# Finalise apache deployment environment
a2dissite 000-default
a2ensite {name}
a2enmod rewrite
rm -f /etc/apache2/sites-available/000-default.conf
rm -f /etc/apache2/sites-available/default-ssl.conf
service apache2 restart

# Setup crontab 
#echo "*/15 * * * * sudo sh -c 'echo 1 > /proc/sys/vm/drop_caches' 
#0 * * * * php /var/www/{name}/scripts/truncate_monitoring.php 
#* * * * * php /var/www/{name}/scripts/monitoring_check.php 
#* * * * * php /var/www/{name}/scripts/trigger_slack_message.php 
#* * * * * php /var/www/{name}/scripts/update_pingdom_cache.php" >> mycron 
#crontab -u ubuntu mycron 
#rm mycron 

{snip_deb_dropcache}

# Setup rc.local (ubuntu on safe reboot)
sed -i -e '$i \nohup sudo -H -u ubuntu bash -c "php /var/www/{name}/scripts/database.php" &\n' /etc/rc.local

# Finally shut down the instance for AMI creation
sudo shutdown -h now
