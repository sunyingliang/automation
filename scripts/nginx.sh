#!/usr/bin/env bash

{snip_deb_sysctl}

# General commands
sudo add-apt-repository ppa:ondrej/php
apt update -y
apt install -y awscli unzip nginx php7.2-fpm php7.2-cli php7.2-curl php7.2-xml php7.2-soap

#apt upgrade -y

{snip_deb_security}

{snip_deb_timezone}

{snip_deb_swap}

# Prepare directories
rm -r /var/www
mkdir /var/www
mkdir /var/www/RRC
mkdir /var/www/LIM

# Copy custom files from S3
export AWS_ACCESS_KEY_ID={s3_key}
export AWS_SECRET_ACCESS_KEY={s3_secret}
export AWS_DEFAULT_REGION={s3_region}
aws s3 cp s3://{s3_bucket}/Configuration/Certificates/{environment}/certificate.txt /etc/nginx/certificate.txt
aws s3 cp s3://{s3_bucket}/Configuration/Certificates/{environment}/key.txt /etc/nginx/key.txt
aws s3 cp s3://{s3_bucket}/Configuration/Certificates/{environment}/chain.txt /etc/nginx/chain.txt
cp /etc/nginx/certificate.txt /etc/nginx/combined.txt
echo >> /etc/nginx/combined.txt
cat /etc/nginx/chain.txt >> /etc/nginx/combined.txt

#fetch latest RRC code from github
curl -s -H "Authorization: token {github}" -o /tmp/RRC.tar -L https://api.github.com/repos/Firmstep/FS-RRC-Clients/tarball/{rrc_gitbranch}
tar xf /tmp/RRC.tar -C /tmp
outfolder=$( ls -1c /tmp | grep Firmstep-FS-RRC- | head -1)
cp -R /tmp/$outfolder/PHP/*.php /var/www/RRC/
cp -R /tmp/$outfolder/PHP/FirmstepRRC /var/www/RRC/
rm -R /tmp/$outfolder
rm /tmp/RRC.tar

echo "<?php
\$rrc->SiteUrl = '{rrc_sitename}';
\$rrc->WebServiceUrl = '{rrc_webservice}';
\$rrc->CustomerKey = '{rrc_key}';
\$rrc->ExternalReferenceUrl = '/RRC/popup.php';
\$rrc->AuthenticationType = 'none';" > /var/www/RRC/settings.php

echo "<html><title>NGINX menu page</title>
<a href='/RRC/'>RRC Test Harness</a><br/>
<a href='/RRC/events.html'>RRC Events Test Page</a><br/>
<a href='/LIM/'>LIM Test Harness</a><br/>
</html>" > /var/www/index.html

echo "<html><title>RRC Events Test Page</title>
<b><a href='./events.php?process={testcase_smd_pin_suu}'>Test case 1:</a></b> Submission message disabled, no payment integration present, no submission URL, no submit event<br/>
Expected result: Submit form, &apos;The form has finished!&apos; message displayed immediately<br/>
<br/>
<b><a href='./events.php?process={testcase_smd_pip_suu}'>Test case 2:</a></b> Submission message disabled, payment integration present, no submission URL, no submit event<br/>
Expected result: Submit form, taken to payment screen, after completing payment &apos;The form has finished!&apos; message displayed immediately<br/>
<br/>
<b><a href='./events.php?process={testcase_sme_pin_suu}'>Test case 3:</a></b> Submission message enabled, no payment integration present, no submission URL, no submit event<br/>
Expected result: Submit form, submission message displayed, press Continue button and &apos;The form has finished!&apos; message displayed<br/>
<br/>
<b><a href='./events.php?process={testcase_sme_pip_suu}'>Test case 4:</a></b> Submission message enabled, payment integration present, no submission URL, no submit event<br/>
Expected result: Submit form, taken to payment screen, after completing payment submission message displayed, press Continue button and &apos;The form has finished!&apos; message displayed<br/>
<br/>
<b><a href='./events.php?process={testcase_smd_pin_sus}'>Test case 5:</a></b> Submission message disabled, no payment integration present, submission URL set, no submit event<br/>
Expected result: Submit form, redirected to http://www.firmsteptest.com/?action=submit&amp;reference=&lt;reference&gt;<br/>
<br/>
<b><a href='./events.php?process={testcase_sme_pin_sus}'>Test case 6:</a></b> Submission message enabled, no payment integration present, submission URL set, no submit event<br/>
Expected result: Submit form, submission message displayed, press Continue button and redirected to http://www.firmsteptest.com/?action=submit&amp;reference=&lt;reference&gt;<br/>
<br/>
<b><a href='./events.php?process={testcase_smd_pip_sus}'>Test case 7:</a></b> Submission message disabled, payment integration present, submission URL set, no submit event<br/>
Expected result: Submit form, taken to payment screen, after completing payment redirected to http://www.firmsteptest.com/?action=submit&amp;reference=&lt;reference&gt;<br/>
<br/>
<b><a href='./events.php?process={testcase_sme_pip_sus}'>Test case 8:</a></b> Submission message enabled, payment integration present, submission URL set, no submit event<br/>
Expected result: Submit form, taken to payment screen, after completing payment submission message displayed, press Continue button and redirected to http://www.firmsteptest.com/?action=submit&amp;reference=&lt;reference&gt;<br/>
<br/>
<b><a href='./events.php?process={testcase_smd_pin_suu}&amp;submitevent=1'>Test case 9:</a></b> Submission message disabled, no payment integration present, no submission URL, submit event enabled<br/>
Expected result: Submit form, &apos;Form submitted with reference: &lt;reference&gt;&apos; message displayed immediately<br/>
<br/>
<b><a href='./events.php?process={testcase_smd_pip_suu}&amp;submitevent=1'>Test case 10:</a></b> Submission message disabled, payment integration present, no submission URL, submit event enabled<br/>
Expected result: Submit form, taken to payment screen, after completing payment &apos;Form submitted with reference: &lt;reference&gt;&apos; message displayed immediately<br/>
<br/>
<b><a href='./events.php?process={testcase_sme_pin_suu}&amp;submitevent=1'>Test case 11:</a></b> Submission message enabled, no payment integration present, no submission URL, submit event enabled<br/>
Expected result: Submit form, submission message displayed, press Continue button and &apos;Form submitted with reference: &lt;reference&gt;&apos; message displayed<br/>
<br/>
<b><a href='./events.php?process={testcase_sme_pip_suu}&amp;submitevent=1'>Test case 12:</a></b> Submission message enabled, payment integration present, no submission URL, submit event enabled<br/>
Expected result: Submit form, taken to payment screen, after completing payment submission message displayed, press Continue button and &apos;Form submitted with reference: &lt;reference&gt;&apos; message displayed<br/>
<br/>
<b><a href='./events.php?process={testcase_smd_pin_sus}&amp;submitevent=1'>Test case 13:</a></b> Submission message disabled, no payment integration present, submission URL set, submit event enabled<br/>
Expected result: Submit form, &apos;Form submitted with reference: &lt;reference&gt;&apos; message displayed immediately<br/>
<br/>
<b><a href='./events.php?process={testcase_sme_pin_sus}&amp;submitevent=1'>Test case 14:</a></b> Submission message enabled, no payment integration present, submission URL set, submit event enabled<br/>
Expected result: Submit form, submission message displayed, press Continue button and &apos;Form submitted with reference: &lt;reference&gt;&apos; message displayed<br/>
<br/>
<b><a href='./events.php?process={testcase_smd_pip_sus}&amp;submitevent=1'>Test case 15:</a></b> Submission message disabled, payment integration present, submission URL set, submit event enabled<br/>
Expected result: Submit form, taken to payment screen, after completing payment &apos;Form submitted with reference: &lt;reference&gt;&apos; message displayed immediately<br/>
<br/>
<b><a href='./events.php?process={testcase_sme_pip_sus}&amp;submitevent=1'>Test case 16:</a></b> Submission message enabled, payment integration present, submission URL set, submit event enabled<br/>
Expected result: Submit form, taken to payment screen, after completing payment submission message displayed, press Continue button and &apos;Form submitted with reference: &lt;reference&gt;&apos; message displayed<br/>
<br/>
</html>" > /var/www/RRC/events.html

#create events.php as a clone of render.php with event handling enabled and using process ID from URL
#start by stripping all comments
cat /var/www/RRC/render.php | sed "s/\/\*//g" | sed "s/\*\///g" > /var/www/RRC/events1.php
#re-apply comments around certain text in the page
cat /var/www/RRC/events1.php | sed "s/To automatically create an account/\/\//" | sed "s/To check for the FormSaved event/\/\//" | sed "s/To check for the FormSubmitted event/\/\//" | sed "s/the PageFinished event/\/\//" | sed "s/If other javascripts on this page/\/\//" | sed "s/set this flag to true to enable compatibility/\/\//" | sed "s/If you want to supply your own/\/\*/" | sed "s/generated it/\*\//" > /var/www/RRC/events2.php
#don't run user registration code
cat /var/www/RRC/events2.php | sed "s/\$rrc\->UserFound/TRUE/" > /var/www/RRC/events3.php
#show submitted message based on conditional URL parameter
cat /var/www/RRC/events3.php | sed "s/\$rrc\->FormSubmitted/\$rrc\->FormSubmitted\&\&isset\(\$_GET\[\'submitevent\'\]\)/" > /var/www/RRC/events4.php
#use pre-built URL including passed-in process ID
cat /var/www/RRC/events4.php | sed "s/\$_SESSION\['SelectedPageUrl'\]/\'\/R4\/Render\/\?process=\'\.\$_GET\[\'process\'\]/" > /var/www/RRC/events5.php
#modify hard-coded return URL
cat /var/www/RRC/events5.php | sed "s/chooser\.php/events\.html/g" | sed "s/chooser/events/g" > /var/www/RRC/events.php
rm /var/www/RRC/events?.php

echo "server {
	tcp_nodelay on;
	listen       80;
	listen       443 ssl;
	server_name  nginx.firmsteptest.com;

	ssl_certificate      /etc/nginx/combined.txt;
	ssl_certificate_key  /etc/nginx/key.txt;
	ssl_session_cache    shared:SSL:1m;
	ssl_session_timeout  5m;
	ssl_ciphers AES128+EECDH:AES128+EDH:!aNULL;
	ssl_prefer_server_ciphers  on;
	ssl_protocols TLSv1 TLSv1.1 TLSv1.2;

	root /var/www;
	try_files \$uri \$uri/index.html \$uri/index.php;

	location ~ \.php$ {
		fastcgi_pass   unix:/run/php/php7.2-fpm.sock;
		fastcgi_index  index.php;
		fastcgi_param  SCRIPT_FILENAME  \$document_root\$fastcgi_script_name;
		include        fastcgi_params;
	}
}" > /etc/nginx/sites-available/default

# Download Common auth library from GitHub
curl -s -H "Authorization: token {github}" -o /tmp/Common.tar -L https://api.github.com/repos/Firmstep/FS-Common/tarball/{gitbranch}
tar xf /tmp/Common.tar -C /tmp
outfolder=$( ls -1c /tmp | grep Firmstep-FS-Common- | head -1)
mkdir -p /var/www/Common
cp -R /tmp/$outfolder/* /var/www/Common/
rm -R /tmp/$outfolder
rm /tmp/Common.tar


echo '<?php
function send_response($doc)
{
	foreach($doc->getElementsByTagName("RequestResponse") as $element)
	{
		if($element->getAttribute("id") == "httpcommandid")
		{
			//check for error response first
			$errorNode = $element->getElementsByTagName("Error")->item(0);
			if ($errorNode != null)
			{
				$errorDescription = $errorNode->getElementsByTagName("Description")->item(0);
				http_response_code(500);
				echo $errorDescription->nodeValue;
			}
			else
			{
				$responseCode = $element->getElementsByTagName("ResponseCode")->item(0);
				http_response_code($responseCode->nodeValue);

				$headers = $element->getElementsByTagName("Header");
				foreach($headers as $header)
				{
					if (strpos($header->nodeValue, "Content-Length") === false)
					{
						header($header->nodeValue);
					}
				}

				$body = $element->getElementsByTagName("Body")->item(0);
				echo $body->nodeValue;
			}
		}
	}
}' > /var/www/LIM/http_response.php

echo "<?php

require_once '../Common/lim/current/client/LIM/FirmstepLIM.php';
require_once 'http_response.php';

\$requests = <<<'EOT'
<Requests>
	<Request provider=\"http\" id=\"httpcommandid\">
		<Command>
		<Method>GET</Method>
		<Url>https://tls12-test.firmstep.com/json.txt</Url>
		<Headers>
			<Header>User-Agent: Firmstep LIM Client</Header>
		</Headers>
		<Body></Body>
		</Command>
	</Request>
</Requests>
EOT;

\$url = '{lim_url}';
\$key = '{lim_key}';
\$iv = '{lim_iv}';

\$lim = new FS\Common\LIM\FirmstepLIM(\$url, \$key, \$iv);
\$doc = new DomDocument('1.0');
\$xml = \$lim->run(\$requests);
\$doc->loadXML(\$xml);

send_response(\$doc);
" > /var/www/LIM/index.php




# Create build file
printf "Build: {name} {build}" > /var/www/build

{snip_deb_monitoring}

{snip_deb_permissions}

{snip_deb_dropcache}

# Shut down the instance
shutdown -h now
