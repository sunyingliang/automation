<powershell>

function PostSlackMessage
{
    param([string]$message)
    Write-Output $message
    $json = @{"channel"="auckland-bot"; "username"="{name} [{environment}]"; "text"=$message; "icon_emoji"=":cloud:"} | ConvertTo-Json
    Invoke-WebRequest -Method POST -Uri https://hooks.slack.com/services/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 -Body $json | Out-Null
}

Set-ExecutionPolicy Unrestricted -Force

Write-Output "Configuring timezone"
tzutil.exe /s "{timezone}"

Write-Output "Installing IIS"
Import-Module ServerManager
Add-WindowsFeature -Name AS-NET-Framework,AS-Web-Support,AS-HTTP-Activation,Web-Server,Web-WebServer,Web-Common-Http,Web-Static-Content,Web-Default-Doc,Web-Http-Errors,Web-App-Dev,Web-Asp-Net,Web-Net-Ext,Web-ISAPI-Ext,Web-ISAPI-Filter,Web-Http-Logging,Web-Log-Libraries,Web-Performance,Web-Stat-Compression,Web-Dyn-Compression,Web-Mgmt-Tools,Web-Mgmt-Console,Web-Scripting-Tools,Web-Mgmt-Service,Web-Metabase,NET-Framework,NET-Framework-Core,NET-HTTP-Activation,PowerShell-ISE,WAS,WAS-Process-Model,WAS-NET-Environment,WAS-Config-APIs | Out-Null

Write-Output "Creating default directories"
if (-Not (Test-Path "C:\TempDownloads")) { New-Item -ItemType directory -Path C:\TempDownloads | Out-Null }
if (-Not (Test-Path "C:\Firmstep")) { New-Item -ItemType directory -Path C:\Firmstep | Out-Null }

[Net.ServicePointManager]::SecurityProtocol = "tls12, tls11, tls"
$errors = ""

Write-Output "Downloading and installing 7-Zip"
(New-Object System.Net.WebClient).DownloadFile("http://www.7-zip.org/a/7z1514-x64.msi", "C:\TempDownloads\7zip.msi")
if (Test-Path C:\TempDownloads\7zip.msi) {
    msiexec /package "C:\TempDownloads\7zip.msi" /quiet /passive /qn /norestart | Out-Null
} else {
    $errors += "7-Zip not found. "
}

Write-Output "Downloading and installing Notepad++"
(New-Object System.Net.WebClient).DownloadFile("https://notepad-plus-plus.org/repository/7.x/7.5.1/npp.7.5.1.Installer.exe", "C:\TempDownloads\npp.exe")
if (Test-Path C:\TempDownloads\npp.exe) {
    C:\TempDownloads\npp.exe /Q /S | Out-Null
} else {
    $errors += "Notepad++ not found. "
}

Write-Output "Downloading and installing AWS CLI Tools"
(New-Object System.Net.WebClient).DownloadFile("https://s3.amazonaws.com/aws-cli/AWSCLI64.msi", "C:\TempDownloads\AWSCLI64.msi")
if (Test-Path C:\TempDownloads\AWSCLI64.msi) {
    msiexec /package "C:\TempDownloads\AWSCLI64.msi" /quiet /passive /qn /norestart | Out-Null
} else {
    $errors += "AWS CLI Tools not found. "
}

Write-Output "Downloading and installing C++ 2010 Redistributable"
(New-Object System.Net.WebClient).DownloadFile("https://download.microsoft.com/download/3/2/2/3224B87F-CFA0-4E70-BDA3-3DE650EFEBA5/vcredist_x64.exe", "C:\TempDownloads\vcredist_2010.exe")
if (Test-Path C:\TempDownloads\vcredist_2010.exe){
    C:\TempDownloads\vcredist_2010.exe /Q /S | Out-Null
} else {
    $errors += "C++ 2010 Redistributable not found. "
}

Write-Output "Downloading and installing MySQL ODBC Driver"
(New-Object System.Net.WebClient).DownloadFile("https://downloads.mysql.com/archives/get/file/mysql-connector-odbc-5.3.4-winx64.msi", "C:\TempDownloads\mysql.msi")
if (Test-Path C:\TempDownloads\mysql.msi){
    msiexec /package "C:\TempDownloads\mysql.msi" /quiet /passive /qn /norestart | Out-Null
} else {
    $errors += "MySQL ODBC Driver not found. "
}

Write-Output "Installing Immunet"
(New-Object System.Net.WebClient).DownloadFile("{download_immunet}", "C:\TempDownloads\immunet.exe")
if (Test-Path C:\TempDownloads\immunet.exe){
	C:\TempDownloads\immunet.exe /S | Out-Null
} else {
    $errors += "Immunet not found. "
}

Write-Output "Installing MSChart"
Copy-S3Object -AccessKey {s3_key} -SecretKey {s3_secret} -Region {s3_region} -BucketName {s3_bucket} -Key OAF/MSChart.exe -LocalFile C:\TempDownloads\MSChart.exe
if (Test-Path C:\TempDownloads\MSChart.exe){
	C:\TempDownloads\MSChart.exe /q /norestart | Out-Null
} else {
    $errors += "MSCharts not found. "
}

# Must be done after Add-WindowsFeature as it depends on IIS
Write-Output "Downloading and installing StripHeaders"
(New-Object System.Net.WebClient).DownloadFile("https://github.com/Dionach/StripHeaders/releases/download/v1.0.5/iis_stripheaders_module_1.0.5.msi", "C:\TempDownloads\stripheaders.msi")
if (Test-Path C:\TempDownloads\stripheaders.msi){
	msiexec /package "C:\TempDownloads\stripheaders.msi" /quiet /passive /qn /norestart | Out-Null
} else {
    $errors += "Strip Headers not found. "
}

if ($errors) {
    Send-MailMessage -From "{name}@firmstep.com" -To "auckland-team@firmstep.com" -Subject "{name} Automation Error [{environment}]" -SmtpServer "smtp.firmstep.com" -Body "$errors"
	Exit
}

# Enable ClamAV for Immunet
$service = Get-Service "Immunet*"
$service.Stop()
$config = (Get-Content "C:\Program Files\Immunet\local.xml") -as [xml]
$scanSettings = $config.config.agent.scansettings
$clamav = $config.CreateElement("clamav")
$d = $config.CreateElement("defInit")
$e.InnerText = "0"
$clamav.AppendChild($d)
$e = $config.CreateElement("enable")
$e.InnerText = "1"
$clamav.AppendChild($e)
$u = $config.CreateElement("updater")
$e = $config.CreateElement("enable")
$e.InnerText = "1"
$u.AppendChild($e)
$clamav.AppendChild($u)
$scanSettings.AppendChild($clamav)
$config.Save("C:\Program Files\Immunet\local.xml")

{snip_win_localadmin}

PostSlackMessage "Starting phase 1"

# Uninstall and block re-installation of IE 11,10,9
$regversion = (New-Object -TypeName System.Version -ArgumentList (Get-ItemProperty 'HKLM:\SOFTWARE\Microsoft\Internet Explorer').Version).Major
$svcversion = (New-Object -TypeName System.Version -ArgumentList (Get-ItemProperty 'HKLM:\SOFTWARE\Microsoft\Internet Explorer').SvcVersion).Major
$maxversion = ($regversion, $svcversion | Measure -Max).Maximum
$pkgmgr = "C:\Windows\System32\PkgMgr.exe"

while ($maxversion -gt 8) {
    $items = Get-ChildItem C:\Windows\servicing\Packages\ | Where-Object {$_.Name -match "Microsoft-Windows-InternetExplorer-Package-TopLevel.*~$maxversion.*" -and $_.Extension -match ".mum"}
    foreach ($item in $items) {
		PostSlackMessage "Uninstalling update $($item.BaseName)"
        $filename = $item.BaseName.ToString()
        & $pkgmgr /up:$filename /norestart | Out-Null
    }
    $maxversion--
}

PostSlackMessage "Blocking IE 9,10,11"

$FoundIE = $TRUE

while ($FoundIE) {
	$FoundIE = $FALSE
	$Session = New-Object -com "Microsoft.Update.Session"
	$Searcher = $Session.CreateUpdateSearcher()
	$Results = $Searcher.Search("IsInstalled=0 and IsHidden=0")
	$Results.Updates | ForEach-Object { 
		if ($_.Title -Match "Internet Explorer 11" -Or $_.Title -Match "Internet Explorer 10" -Or $_.Title -Match "Internet Explorer 9") {
			PostSlackMessage "Blocking $($_.Title)" 
			$_.IsHidden = $TRUE
			$FoundIE = $TRUE
		}
	}
}

# Prevent IE8 first-time run screen
New-Item -Path "HKLM:\Software\Policies\Microsoft\Internet Explorer"
New-Item -Path "HKLM:\Software\Policies\Microsoft\Internet Explorer\Main"
New-ItemProperty -Path "HKLM:\Software\Policies\Microsoft\Internet Explorer\Main" -Name DisableFirstRunCustomize -Value 1 -PropertyType DWORD

PostSlackMessage "Configuring Windows Update"

New-ItemProperty -Path "HKLM:\SOFTWARE\Microsoft\Windows\CurrentVersion\WindowsUpdate\Auto Update" -Name AUOptions -Value 3 -PropertyType DWORD
New-ItemProperty -Path "HKLM:\SOFTWARE\Microsoft\Windows\CurrentVersion\WindowsUpdate\Auto Update" -Name IncludeRecommendedUpdates -Value 1 -PropertyType DWORD
New-ItemProperty -Path "HKLM:\SOFTWARE\Microsoft\Windows\CurrentVersion\WindowsUpdate\Auto Update" -Name ElevateNonAdmins -Value 1 -PropertyType DWORD

$updateScript = @'
function PostSlackMessage
{
    param([string]$message)
    Write-Output $message
    $json = @{"channel"="auckland-bot"; "username"="{name} [{environment}]"; "text"=$message; "icon_emoji"=":cloud:"} | ConvertTo-Json
    Invoke-WebRequest -Method POST -Uri https://hooks.slack.com/services/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 -Body $json | Out-Null
}

function UpdatesDone
{
	# Re-enable user-data plugin on next reboot
	$EC2SettingsFile="C:\Program Files\Amazon\Ec2ConfigService\Settings\Config.xml"
	$xml = [xml](get-content $EC2SettingsFile)
	$xmlElement = $xml.get_DocumentElement()
	$xmlElementToModify = $xmlElement.Plugins
	foreach ($element in $xmlElementToModify.Plugin)
	{
		if ($element.name -eq "Ec2HandleUserData")
		{
			$element.State="Enabled"
		}
	}
	$xml.Save($EC2SettingsFile)
	C:\Windows\system32\schtasks.exe /delete /tn "InstallUpdates" /f
	Stop-Computer
	Exit
}

PostSlackMessage "Starting phase 2"

$Session = New-Object -com "Microsoft.Update.Session"
$Searcher = $Session.CreateUpdateSearcher()
$AvailableUpdates = $Searcher.Search("IsInstalled=0 and IsHidden=0")

if ($AvailableUpdates.Updates.Count -lt 1) {
	PostSlackMessage "No more updates found, shutting down"
	UpdatesDone
}

$UpdatesAvailable = $FALSE
PostSlackMessage "Checking for updates"
$InstallCollection = New-Object -com "Microsoft.Update.UpdateColl"
$AvailableUpdates.Updates | ForEach-Object {
	if ($_.Title -Match "Internet Explorer 11" -Or $_.Title -Match "Internet Explorer 10" -Or $_.Title -Match "Internet Explorer 9") {
			PostSlackMessage "Blocking $($_.Title)" 
			$_.IsHidden = $TRUE
	} elseif ($_.InstallationBehavior.CanRequestUserInput -ne $TRUE) {
		PostSlackMessage "Including update $($_.Title)"
		if($_.EulaAccepted -eq 0) { 
			$_.AcceptEula() 
		}
		$InstallCollection.Add($_) | Out-Null
		$UpdatesAvailable = $TRUE
	}
}

if (-Not $UpdatesAvailable) {
	PostSlackMessage "No updates suitable for install"
	UpdatesDone
}

PostSlackMessage "Downloading updates"

$Downloader = $Session.CreateUpdateDownloader() 
$Downloader.Updates = $InstallCollection

Try {
	$Results = $Downloader.Download()
	PostSlackMessage "Download results: $($Results | Select *)"
} Catch {
	PostSlackMessage "Download error: $($_)"
}

PostSlackMessage "Installing updates"

$Installer = $Session.CreateUpdateInstaller()
$Installer.Updates = $InstallCollection

Try {
	$Results = $Installer.Install()
	PostSlackMessage "Install results: $($Results | Select *)"
} Catch {
	PostSlackMessage "Install error: $($_)"
}

PostSlackMessage "Install complete, rebooting"
Restart-Computer
'@

[IO.File]::WriteAllText("C:\Firmstep\InstallUpdates.ps1",$updateScript)

C:\Windows\system32\schtasks.exe /create /sc ONSTART /tn InstallUpdates /tr "powershell.exe C:\Firmstep\InstallUpdates.ps1" /RU System /RL HIGHEST | Out-Null

PostSlackMessage "Rebooting phase 1"

Remove-Item C:\TempDownloads -recurse

Restart-Computer

</powershell>
