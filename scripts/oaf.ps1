<powershell>

Set-ExecutionPolicy Unrestricted -Force

write-host "Configuring timezone"
tzutil.exe /s "{timezone}"

write-host "Creating default directories"
if (-Not (Test-Path "C:\TempDownloads")) { New-Item -ItemType directory -Path C:\TempDownloads | Out-Null }
if (-Not (Test-Path "C:\Firmstep")) { New-Item -ItemType directory -Path C:\Firmstep | Out-Null }

[Net.ServicePointManager]::SecurityProtocol = "tls12, tls11, tls"
$errors = ""

{snip_win_monitoring}

if ("{environment}" -eq "test") {
    {snip_win_domainjoin}
}

# Download repo from github
$wr = [System.Net.HttpWebRequest]::Create("https://api.github.com/repos/Firmstep/FS-Platform-Base/zipball/{gitbranch}")
$wr.Headers.Add("Authorization", "token {github}")
$wr.UserAgent = "Firmstep"
$resp = $wr.GetResponse()
$str = $resp.GetResponseStream()
$file = [System.IO.File]::Create("C:\TempDownloads\FS-Platform-Base.zip")
$str.CopyTo($file)
$file.Close()

c:\Program` Files\7-Zip\7z.exe x C:\TempDownloads\FS-Platform-Base.zip -oC:\TempDownloads\Extracted\ | Out-Null
$folder = Get-ChildItem C:\TempDownloads\Extracted\* | Select -ExpandProperty FullName
move $folder C:\FS-Platform-Base
Remove-Item C:\FS-Platform-Base\LPAConfigTool -recurse
Remove-Item C:\FS-Platform-Base\Source -recurse

#turn-off help links since Toni has hidden the online pages
Move-Item -Path C:\FS-Platform-Base\FS2\Forms\AF\Configuration\HelpLinks.txt -Destination C:\FS-Platform-Base\FS2\Forms\AF\Configuration\OldHelpLinks.txt

Copy-S3Object -AccessKey {s3_key} -SecretKey {s3_secret} -Region {s3_region} -BucketName {s3_bucket} -Key Configuration/LegacyPlatform/{environment}/fsplatform.xml -LocalFile C:/FS-Platform-Base/fsplatform.xml | Out-Null
Copy-S3Object -AccessKey {s3_key} -SecretKey {s3_secret} -Region {s3_region} -BucketName {s3_bucket} -Key Configuration/LegacyPlatform/{environment}/config.xml -LocalFile C:/FS-Platform-Base/Maintenance/config.xml | Out-Null

# Configure FS2 logging to use D: drive
$logConfig = Get-Content C:\FS-Platform-Base\FS2\LogConfig.xml -Raw
$logConfig = $logConfig -Replace "c:\\platform-logs\\", "D:\platform-logs\"
[IO.File]::WriteAllText("C:\FS-Platform-Base\FS2\LogConfig.xml",$logConfig)

# Create emails folder and set appropriate permissions
mkdir "C:\FS-Platform-Base\FS2\Forms\AF\Data\Emails" | Out-Null
icacls "C:\FS-Platform-Base\FS2\Forms\AF\Data\Emails" /grant "Everyone:(OI)(CI)F" | Out-Null

write-host "Installing services"
C:\FS-Platform-Base\FS2\firmstep2.exe install firmstep firmstep | Out-Null
C:\FS-Platform-Base\FS2\Tools\nssm.exe install NodeJS "C:\FS-Platform-Base\node\bin\node.exe" "webroot/js/r3/server.r3.js" | Out-Null
reg add HKLM\SYSTEM\CurrentControlSet\services\NodeJS\Parameters /v AppDirectory /t REG_SZ /d "C:\FS-Platform-Base" /f | Out-Null
reg add HKLM\SYSTEM\CurrentControlSet\services\NodeJS\Parameters\AppExit /ve /t REG_SZ /d Restart /f | Out-Null

write-host "Installing ABCPDF"
regsvr32 /s "C:\FS-Platform-Base\FS2\Tools\pdfgen\ABCpdf4.dll" | Out-Null
regedit /S "C:\FS-Platform-Base\FS2\Tools\pdfgen\ABCpdf Registration 64bit.reg" | Out-Null
netsh advfirewall firewall add rule name=Firmstep2 dir=in action=allow protocol=TCP localport=81

Import-Module WebAdministration

# Setup default platform site
Set-ItemProperty "IIS:\Sites\Default Web Site" -name physicalPath -value C:\FS-Platform-Base\Webroot 
new-item "IIS:\Sites\Default Web Site\API" -physicalPath "C:\FS-Platform-Base\Webroot\API" -type Application | Out-Null
new-item "IIS:\Sites\Default Web Site\FAMLogin" -physicalPath "C:\FS-Platform-Base\Webroot\FAMLogin" -type Application | Out-Null
new-item "IIS:\Sites\Default Web Site\Login" -physicalPath "C:\FS-Platform-Base\Webroot\Login" -type Application | Out-Null

# Remove unnecessary IIS bindings
Remove-ItemProperty "IIS:\Sites\Default Web Site" -Name Bindings -AtElement @{protocol="net.tcp"}
Remove-ItemProperty "IIS:\Sites\Default Web Site" -Name Bindings -AtElement @{protocol="net.pipe"}
Remove-ItemProperty "IIS:\Sites\Default Web Site" -Name Bindings -AtElement @{protocol="net.msmq"}
Remove-ItemProperty "IIS:\Sites\Default Web Site" -Name Bindings -AtElement @{protocol="msmq.formatname"}

# Setup AFProxy site
new-item IIS:\Sites\AFProxy -bindings @{protocol="http";bindingInformation=":8082:"} -physicalPath C:\FS-Platform-Base\AFProxy
netsh advfirewall firewall add rule name=AFProxy dir=in action=allow protocol=TCP localport=8082

# Set custom AppPool settings
Set-ItemProperty -Path "IIS:\AppPools\DefaultAppPool" -Name Recycling.periodicRestart.time -Value "04:00:00"
Set-ItemProperty -Path "IIS:\AppPools\DefaultAppPool" -Name ProcessModel.maxProcesses -Value "{iis_workers}"

# Prevent IE8 first-time run screen
New-Item -Path "HKLM:\Software\Policies\Microsoft\Internet Explorer"
New-Item -Path "HKLM:\Software\Policies\Microsoft\Internet Explorer\Main"
New-ItemProperty -Path "HKLM:\Software\Policies\Microsoft\Internet Explorer\Main" -Name DisableFirstRunCustomize -Value 1 -PropertyType DWORD

# Custom monitoring config for OAF only
Stop-Service -name FSAgent
$config = (Get-Content "C:\Program Files\Monitoring\MonitoringAgent.exe.config") -as [Xml]
$appSettings = $config.configuration.appSettings
($appSettings.add | Where-Object { $_.Key -eq "serverID"}).Value = "invalid"

#<add key="serviceName" value="firmstep"/>
$node = $config.CreateElement("add")
$attr = $config.CreateAttribute("key")
$attr.Value = "serviceName"
$node.Attributes.Append($attr)
$attr = $config.CreateAttribute("value")
$attr.Value = "firmstep"
$node.Attributes.Append($attr)
$appSettings.AppendChild($node)

#<add key="emailFolder" value="C:\FS-Platform-Base\FS2\Forms\AF\Data\Emails"/>
$node = $config.CreateElement("add")
$attr = $config.CreateAttribute("key")
$attr.Value = "emailFolder"
$node.Attributes.Append($attr)
$attr = $config.CreateAttribute("value")
$attr.Value = "C:\FS-Platform-Base\FS2\Forms\AF\Data\Emails"
$node.Attributes.Append($attr)
$appSettings.AppendChild($node)

#<add key="sessionURL" value="http://localhost:81/Util/SessionInfo/"/>
$node = $config.CreateElement("add")
$attr = $config.CreateAttribute("key")
$attr.Value = "sessionURL"
$node.Attributes.Append($attr)
$attr = $config.CreateAttribute("value")
$attr.Value = "http://localhost:81/Util/SessionInfo/"
$node.Attributes.Append($attr)
$appSettings.AppendChild($node)
 
#<add key="serverIDFile" value="C:\FS-Platform-Base\FS2\overrides.txt"/>
$node = $config.CreateElement("add")
$attr = $config.CreateAttribute("key")
$attr.Value = "serverIDFile"
$node.Attributes.Append($attr)
$attr = $config.CreateAttribute("value")
$attr.Value = "C:\FS-Platform-Base\FS2\overrides.txt"
$node.Attributes.Append($attr)
$appSettings.AppendChild($node)

#<add key="sessionTimeout" value="120"/>
$node = $config.CreateElement("add")
$attr = $config.CreateAttribute("key")
$attr.Value = "sessionTimeout"
$node.Attributes.Append($attr)
$attr = $config.CreateAttribute("value")
$attr.Value = "120"
$node.Attributes.Append($attr)
$appSettings.AppendChild($node)

#<add key="smtpServer" value=""/>
$node = $config.CreateElement("add")
$attr = $config.CreateAttribute("key")
$attr.Value = "smtpServer"
$node.Attributes.Append($attr)
$attr = $config.CreateAttribute("value")
$attr.Value = "{monitoring_smtp_server}"
$node.Attributes.Append($attr)
$appSettings.AppendChild($node)

#<add key="emailTo" value=""/>
$node = $config.CreateElement("add")
$attr = $config.CreateAttribute("key")
$attr.Value = "emailTo"
$node.Attributes.Append($attr)
$attr = $config.CreateAttribute("value")
$attr.Value = "{monitoring_to_address}"
$node.Attributes.Append($attr)
$appSettings.AppendChild($node)

#<add key="emailFrom" value=""/>
$node = $config.CreateElement("add")
$attr = $config.CreateAttribute("key")
$attr.Value = "emailFrom"
$node.Attributes.Append($attr)
$attr = $config.CreateAttribute("value")
$attr.Value = "{monitoring_from_address}"
$node.Attributes.Append($attr)
$appSettings.AppendChild($node)

$config.Save("C:\Program Files\Monitoring\MonitoringAgent.exe.config")
Start-Service -name FSAgent

write-host "Installing scheduled tasks"
C:\Windows\system32\schtasks.exe /create /tn AutoConfig /xml C:\FS-Platform-Base\Maintenance\autoconfig.xml | Out-Null
C:\Windows\system32\schtasks.exe /create /tn "Email Cleanup" /xml C:\FS-Platform-Base\Maintenance\emailcleanup.xml | Out-Null
C:\Windows\system32\schtasks.exe /create /tn "Move Logs" /xml C:\FS-Platform-Base\Maintenance\movelogs.xml | Out-Null
C:\Windows\system32\schtasks.exe /create /tn "Email Fetch" /xml C:\FS-Platform-Base\Maintenance\emailfetch.xml | Out-Null

#load shutdown registry settings
REG IMPORT C:\FS-Platform-Base\Maintenance\shutdown.reg | Out-Null

#create shutdown ini file
$shutdownIni = @"

[Shutdown]
0CmdLine=C:\FS-Platform-Base\Maintenance\emailcleanup.ps1
0Parameters=

"@.Replace("`r","`r`n")
[IO.File]::WriteAllText("C:\TempDownloads\psscripts.ini",$shutdownIni)
New-Item -ItemType Directory -Force -Path C:\Windows\System32\GroupPolicy\Machine\Scripts\
New-Item -ItemType Directory -Force -Path C:\Windows\System32\GroupPolicy\Machine\Scripts\Startup
New-Item -ItemType Directory -Force -Path C:\Windows\System32\GroupPolicy\Machine\Scripts\Shutdown
New-Item -ItemType Directory -Force -Path C:\Windows\System32\GroupPolicy\User
Copy-Item -force -path C:\TempDownloads\psscripts.ini -destination C:\Windows\System32\GroupPolicy\Machine\Scripts\psscripts.ini
gpupdate /force
Remove-Item C:\TempDownloads -recurse

# Set drive labels
$drive = gwmi win32_volume -Filter "DriveLetter = 'C:'"
$drive.Label = "Platform"
$drive.put() 
$drive = gwmi win32_volume -Filter "DriveLetter = 'D:'"
$drive.Label = "Logs"
$drive.put() 

# Create build file in webroot folder
New-Item "C:/FS-Platform-Base/Webroot/build.txt" -type File -force -value "Build: {name} {build}"

# Add 'P3P: CP="IDC CUR OUR NOR STA"   custom IIS header
[System.Reflection.Assembly]::LoadWithPartialName("Microsoft.Web.Administration") | Out-Null
$iis = new-object Microsoft.Web.Administration.ServerManager
$config =$iis.GetApplicationHostConfiguration()
$httpProtocolSection = $config.GetSection("system.webServer/httpProtocol")
$customHeadersCollection = $httpProtocolSection.GetCollection("customHeaders")
$addElement = $customHeadersCollection.CreateElement("add")
$addElement["name"] = "P3P"
$addElement["value"] = "CP=`"IDC CUR OUR NOR STA`""
$customHeadersCollection.Add($addElement)
$iis.CommitChanges()

# Create desktop shortcut for cleanup script
$WScriptShell = New-Object -ComObject WScript.Shell
$Shortcut = $WScriptShell.CreateShortcut("C:\Users\Public\Desktop\Cleanup.lnk")
$Shortcut.TargetPath = "powershell.exe"
$Shortcut.Arguments  = "C:\FS-Platform-Base\Maintenance\drainstop.ps1 -force"
$Shortcut.Save()

# Cleanup
Remove-Item "C:\Users\Default\Desktop\*.*"
Remove-Item "C:\TempDownloads" -Recurse -Force

Stop-Computer

</powershell>
