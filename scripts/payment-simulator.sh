#!/usr/bin/env bash

{snip_deb_sysctl}

# General commands
apt update -y
#apt upgrade -y

#Install web server with php extensions
apt install -y apache2 libapache2-mod-php7.0 awscli unzip
apt install -y php7.0-cli php7.0-curl php7.0-mbstring php7.0-mcrypt php7.0-dom php7.0-xml php7.0-mysql

{snip_deb_security}

{snip_deb_timezone}

{snip_deb_swap}

# Prepare html directory
rm -r /var/www/*

# Install PHP Unit
curl -s -o /tmp/phpunit.phar -L https://phar.phpunit.de/phpunit.phar
chmod +x /tmp/phpunit.phar
mv /tmp/phpunit.phar /usr/local/bin/phpunit

# Download code from GitHub
curl -s -H "Authorization: token {github}" -o /tmp/{name}.tar -L https://api.github.com/repos/Firmstep/FS-Payment-Simulator/tarball/{gitbranch}
tar xf /tmp/{name}.tar -C /tmp
outfolder=$( ls -1c /tmp | grep FS-Payment-Simulator- | head -1)
mkdir -p /var/www/{name}
cp -R /tmp/$outfolder/* /var/www/{name}/
rm -R /tmp/$outfolder
rm /tmp/{name}.tar

# Copy custom files from S3
export AWS_ACCESS_KEY_ID={s3_key}
export AWS_SECRET_ACCESS_KEY={s3_secret}
export AWS_DEFAULT_REGION={s3_region}
aws s3 cp s3://{s3_bucket}/Configuration/{name}/{environment}/common.php /var/www/{name}/config/common.php
aws s3 cp s3://{s3_bucket}/Configuration/Certificates/{environment}/certificate.txt /etc/apache2/certificate.txt
aws s3 cp s3://{s3_bucket}/Configuration/Certificates/{environment}/key.txt /etc/apache2/key.txt
aws s3 cp s3://{s3_bucket}/Configuration/Certificates/{environment}/chain.txt /etc/apache2/chain.txt

# Create build file
printf "Build: {name} {build}" > /var/www/{name}/public/build

# Generate VirtualHost File
echo "<VirtualHost *:80>
    RewriteEngine On
    RewriteRule (.*) https://%{SERVER_NAME}$1 [R,L]
</VirtualHost>
<VirtualHost *:443>
    Header always set Strict-Transport-Security \"max-age=2592000; includeSubDomains\"
    DocumentRoot /var/www/{name}/public
    ErrorLog /var/log/apache2/error.log
    CustomLog /var/log/apache2/access.log combined
    <Directory /var/www/{name}/public>
        Options +Includes -Indexes +FollowSymLinks +MultiViews
        AllowOverride All
        Require all granted
        RewriteEngine on
        RewriteCond %{REQUEST_FILENAME} -f
        RewriteRule ^ - [L]
        RewriteRule ^ index.php [L]
    </Directory>
    SSLEngine on
    SSLCertificateFile /etc/apache2/certificate.txt
    SSLCertificateKeyFile /etc/apache2/key.txt
    SSLCertificateChainFile /etc/apache2/chain.txt
</VirtualHost>" > /etc/apache2/sites-available/{name}.conf

{snip_deb_opcache}

{snip_deb_monitoring}

{snip_deb_permissions}

# Finalise apache deployment environment
a2dissite 000-default
a2enmod rewrite
a2enmod ssl
a2enmod headers
a2ensite {name}
rm -f /etc/apache2/sites-available/000-default.conf
rm -f /etc/apache2/sites-available/default-ssl.conf
rm -f /var/www/{name}/config/common_example.php
service apache2 restart

# Finally shut down the instance for AMI creation
sudo shutdown -h now
