#!/usr/bin/env bash

{snip_deb_sysctl}

{snip_deb_timezone}

{snip_deb_swap}

# General commands
echo "Updating ubuntu"
apt-get -y update
apt-get -y upgrade
apt-get install -y cron awscli unzip git

echo "Installing Apache"
apt-get install apache2 apache2-dev -y

echo "installing PHP"
apt-get install php5 php5-dev php5-cli libapache2-mod-php5 -y
#apt upgrade -y

echo "Installing php extensions"
apt-get install curl php5-curl php5-mcrypt php5-mysql php5-xdebug php5-memcached php5-memcache dos2unix -y

echo "mysql config and install"
apt-get -y install mysql-client libapache2-mod-auth-mysql php5-mysql mysql-client libmysqlclient-dev libaprutil1-dbd-mysql vim php5-xsl memcached

echo "enable apache mods"
a2enmod reqtimeout
a2enmod dbd
a2enmod headers
a2enmod rewrite
a2enmod authz_groupfile
a2enmod proxy_http
rm -f /etc/apache2/sites-enabled/000-default.conf
service apache2 restart

echo "enable php modules"
php5enmod mcrypt

# Prepare directories
rm -r /var/www*
mkdir /var/www

# Download code from GitHub
git clone -b {gitbranch}  https://{github}@github.com/Firmstep/FS-Platform.git /var/www/

rm /tmp/platform.tar
#make /vagrant folder exist, pointing to Vagrant folder in repo
ln -s /var/www/Vagrant /vagrant

echo "modmyvhost"
tar -xzf /vagrant/default_files/mod_myvhost-0.21.tar.gz
cd mod_myvhost-0.21
make
make install

# After myvhost files are created we have to load libphp5 for myvhost
cp /vagrant/default_files/myvhost.load /etc/apache2/mods-available/
cat /vagrant/default_files/hosts >> /etc/hosts
service apache2 restart

echo "node dependencies"
apt-get install -y libxml2 libxml2-dev libxslt1.1 libxslt1-dev

echo "Installing Node"

curl -sL https://deb.nodesource.com/setup | sudo bash -
apt-get install -y nodejs

export HOME=/root/

cd /var/www/
npm install --no-bin-links
npm install -g coffee-script grunt-cli LiveScript@1.2.0 mocha bless@3.0.3
npm install selenium-webdriver
npm install cheerio
npm install grunt-recurse
npm install argparse
npm install node-expat
npm install libxmljs xml2json node_xslt jsdom

apt-get install git-all -y
cd /var/www/
git submodule update --init

echo "replacing create settings"
mv /var/www/FS-Automate/deployment/build_scripts/create_settings.sh /var/www/FS-Automate/deployment/build_scripts/create_settings.sh.backup
cp /vagrant/default_files/create_settings.sh /var/www/FS-Automate/deployment/build_scripts/create_settings.sh
dos2unix /var/www/FS-Automate/deployment/build_scripts/create_settings.sh

cd /var/www/FS-Automate/deployment/build_scripts && livescript deploy applications -d /var/www
chmod a+w /var/www/fs-civi/sites/default/files
service apache2 restart

echo "restoring create settings"
rm /var/www/FS-Automate/deployment/build_scripts/create_settings.sh
mv /var/www/FS-Automate/deployment/build_scripts/create_settings.sh.backup /var/www/FS-Automate/deployment/build_scripts/create_settings.sh

echo "Installing composer..."
curl -Ss https://getcomposer.org/installer | php -- --install-dir=/usr/local/bin --filename=composer
echo "Composer successfully installed globally"

echo "Composer install Laravel"
cd /var/www/LaraApp
composer install

# Copy custom files from S3
export AWS_ACCESS_KEY_ID={s3_key}
export AWS_SECRET_ACCESS_KEY={s3_secret}
export AWS_DEFAULT_REGION={s3_region}
aws s3 cp s3://{s3_bucket}/Configuration/{name}/{environment}/vhosts.conf /etc/apache2/sites-enabled/vhosts.conf
aws s3 cp s3://{s3_bucket}/Configuration/{name}/{environment}/database-settings.js /var/www/database-settings.js
aws s3 cp s3://{s3_bucket}/Configuration/{name}/{environment}/config-local.php /var/www/FS-API/config-local.php
aws s3 cp s3://{s3_bucket}/Configuration/{name}/{environment}/config-local.php /var/www/fs-ref-doc-api/config-local.php
aws s3 cp s3://{s3_bucket}/Configuration/{name}/{environment}/queue-config.php /var/www/FS-Queue/config.php
aws s3 cp s3://{s3_bucket}/Configuration/{name}/{environment}/queue-config.js /var/www/FS-Node/build/src/config.js
aws s3 cp s3://{s3_bucket}/Configuration/{name}/{environment}/rest.php /var/www/fs-civi/sites/all/modules/civicrm/extern/rest.php
aws s3 cp s3://{s3_bucket}/Configuration/{name}/{environment}/civicrm.settings.php /var/www/fs-civi/sites/default/civicrm.settings.php
aws s3 cp s3://{s3_bucket}/Configuration/{name}/{environment}/settings.php /var/www/fs-civi/sites/default/settings.php

# Grab bugsnag settings file from Prod site
curl -s https://aucklandteam-forms.achieveservice.com/library/bugsnag-settings.js -o /var/www/FS-Library/build/src/bugsnag-settings.js

# Create build file
printf "Build: {name} {build}" > /var/www/build

{snip_deb_monitoring}

{snip_deb_permissions}

# Setup crontab
echo "* * * * * php /var/www/FS-Queue/index.php queue-1" >> mycron
crontab -u ubuntu mycron
rm mycron

# Setup rc.local (ubuntu on safe reboot)
sed -i -e '$i \nohup sudo -H -u ubuntu bash -c "cd /var/www/FS-Node && /usr/bin/node build/src/index.js" &\n' /etc/rc.local
sed -i -e '$i \nohup sudo -H -u ubuntu bash -c "cd /var/www/FS-Automate/customer && livescript customer update" &\n' /etc/rc.local

# Finally shut down the instance
shutdown -h now
