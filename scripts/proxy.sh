#!/usr/bin/env bash

{snip_deb_sysctl}

# General commands
apt update -y
apt install -y apache2 php5-cli php5 libapache2-mod-php5 php5-mysql php5-curl awscli zip unzip heirloom-mailx
#apt upgrade -y

{snip_deb_timezone}

{snip_deb_swap}

# Prepare directories
rm -r /var/www/*

# Download code from GitHub
curl -s -H "Authorization: token {github}" -o /tmp/{name}.tar -L https://api.github.com/repos/Firmstep/FS-Infrastructure/tarball/{gitbranch}
tar xf /tmp/{name}.tar -C /tmp
outfolder=$( ls -1c /tmp | grep Firmstep-FS-Infrastructure- | head -1)
mkdir -p /home/ubuntu/bin/{name}
cp -R /tmp/$outfolder/Proxy/* /home/ubuntu/bin/{name}
rm -R /tmp/$outfolder
rm /tmp/{name}.tar

# Copy custom files from S3
export AWS_ACCESS_KEY_ID={s3_key}
export AWS_SECRET_ACCESS_KEY={s3_secret}
export AWS_DEFAULT_REGION={s3_region}
aws s3 cp s3://{s3_bucket}/Configuration/{name}/{environment}/common.php /home/ubuntu/bin/{name}/config/common.php
aws s3 cp s3://{s3_bucket}/Configuration/Certificates/{environment}/certificate.txt /etc/apache2/certificate.txt
aws s3 cp s3://{s3_bucket}/Configuration/Certificates/{environment}/key.txt /etc/apache2/key.txt
aws s3 cp s3://{s3_bucket}/Configuration/Certificates/{environment}/chain.txt /etc/apache2/chain.txt

# Make required stub files/directories
touch /etc/apache2/sites-available/{name}.conf
mkdir /var/www/{name}

# Apache2 configuration
echo 'Mutex file:${APACHE_LOCK_DIR} default
PidFile ${APACHE_PID_FILE}
Timeout 300
KeepAlive On
MaxKeepAliveRequests 100
KeepAliveTimeout 5
User ${APACHE_RUN_USER}
Group ${APACHE_RUN_GROUP}
HostnameLookups Off
ErrorLog ${APACHE_LOG_DIR}/error.log
LogLevel warn
IncludeOptional mods-enabled/*.load
IncludeOptional mods-enabled/*.conf
Listen 80
<IfModule ssl_module>
    Listen 443
</IfModule>
<IfModule mod_gnutls.c>
    Listen 443
</IfModule>
AccessFileName .htaccess
<FilesMatch "^\.ht">
    Require all denied
</FilesMatch>
LogFormat "%v:%p %h %l %u %t \"%r\" %>s %O \"%{Referer}i\" \"%{User-Agent}i\" %D" vhost_combined
LogFormat "%h %l %u %t \"%r\" %>s %O \"%{Referer}i\" \"%{User-Agent}i\"" combined
LogFormat "%h %l %u %t \"%r\" %>s %O" common
LogFormat "%{Referer}i -> %U" referer
LogFormat "%{User-agent}i" agent
IncludeOptional conf-enabled/*.conf
IncludeOptional sites-enabled/*.conf' > /etc/apache2/apache2.conf

# Generate VirtualHost File
echo "<VirtualHost *:80>
    Include sites-available/Proxy.conf
</VirtualHost>"  > /etc/apache2/sites-available/000-default.conf

echo "<VirtualHost *:443>
    SSLEngine on
    SSLCertificateFile /etc/apache2/certificate.txt
    SSLCertificateKeyFile /etc/apache2/key.txt
    SSLCertificateChainFile /etc/apache2/chain.txt
    Include sites-available/Proxy.conf
</VirtualHost>" > /etc/apache2/sites-available/default-ssl.conf

# Create build file
printf "Build: {name} {build}" > /var/www/{name}/build

{snip_deb_monitoring}

{snip_deb_permissions}

# Finalise apache deployment environment
a2dismod autoindex
a2enmod rewrite
a2enmod proxy
a2enmod proxy_http
a2enmod ssl
a2ensite default-ssl
php5enmod mysql
service apache2 restart

# Setup crontab
echo "*/15 * * * * sudo sh -c 'echo 1 > /proc/sys/vm/drop_caches'
* * * * * sudo php /home/ubuntu/bin/Proxy/proxy_rebuild.php 1
{cron_mail_job} php /home/ubuntu/bin/Proxy/proxy_test_url.php
{cron_mail_job} sudo grep {cron_mail_kw} /var/log/apache2/other_vhosts_access.log.1 /var/log/apache2/other_vhosts_access.log > /tmp/access.log; sudo grep {cron_mail_kw} /var/log/apache2/error.log.1 /var/log/apache2/error.log > /tmp/error.log; sudo zip /tmp/logs.zip /tmp/access.log /tmp/error.log;echo \"Log Files\" | mail -v -a /tmp/logs.zip -s \"Log Files\" -S smtp=\"{cron_mail_server}:25\" -r \"{cron_mail_from}\" -c {cron_mail_cc} {cron_mail_to}" >> mycron
crontab -u ubuntu mycron
rm mycron

# Setup rc.local (ubuntu on safe reboot)
sed -i -e '$i \nohup php /home/ubuntu/bin/{name}/proxy_rebuild.php 0 &\n' /etc/rc.local

# Setup crontab
sudo -u ubuntu php /home/ubuntu/bin/{name}/proxy_update_crontab.php

# Format & mount logs drive
mkfs -t ext4 /dev/xvdf
cp -r /var/log/apache2/* /dev/xvdf
mount /dev/xvdf /var/log/apache2/ 
echo "/dev/xvdf /var/log/apache2/ ext4 defaults 0 0" >> /etc/fstab

# Finally shut down the instance for AMI creation
sudo shutdown -h now
