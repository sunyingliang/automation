<powershell>

Set-ExecutionPolicy Unrestricted -Force

write-host "Configuring timezone"
tzutil.exe /s "{timezone}"

if (-Not (Test-Path "C:\TempDownloads")) { New-Item -ItemType directory -Path C:\TempDownloads | Out-Null }
if (-Not (Test-Path "C:\Firmstep")) { New-Item -ItemType directory -Path C:\Firmstep | Out-Null }

[Net.ServicePointManager]::SecurityProtocol = "tls12, tls11, tls"
$errors = ""

write-host "Downloading and installing 7-Zip"
(New-Object System.Net.WebClient).DownloadFile("http://www.7-zip.org/a/7z1514-x64.msi", "C:\TempDownloads\7zip.msi")
if (Test-Path C:\TempDownloads\7zip.msi) {
    msiexec /package "C:\TempDownloads\7zip.msi" /quiet /passive /qn /norestart | Out-Null
} else {
    $errors += "7-Zip not found. "
}

write-host "Downloading and installing Notepad++"
(New-Object System.Net.WebClient).DownloadFile("https://notepad-plus-plus.org/repository/6.x/6.9.2/npp.6.9.2.Installer.exe", "C:\TempDownloads\npp.exe")
if (Test-Path C:\TempDownloads\npp.exe) {
    C:\TempDownloads\npp.exe /Q /S | Out-Null
} else {
    $errors += "Notepad++ not found. "
}

write-host "Downloading and installing AWS CLI Tools"
(New-Object System.Net.WebClient).DownloadFile("https://s3.amazonaws.com/aws-cli/AWSCLI64.msi", "C:\TempDownloads\AWSCLI64.msi")
if (Test-Path C:\TempDownloads\AWSCLI64.msi) {
    msiexec /package "C:\TempDownloads\AWSCLI64.msi" /quiet /passive /qn /norestart | Out-Null
} else {
    $errors += "AWS CLI Tools not found. "
}

{snip_win_monitoring}

{snip_win_localadmin}

if ("{environment}" -eq "test") {
    {snip_win_domainjoin}
}

if ($errors) {
    Send-MailMessage -From "{name}@firmstep.com" -To "auckland-team@firmstep.com" -Subject "{name} Automation Error [{environment}]" -SmtpServer "smtp.firmstep.com" -Body "$errors"
}

Import-Module ServerManager
Add-WindowsFeature -Name Web-Static-Content,Web-Default-Doc,Web-Http-Errors,Web-Http-Redirect,Web-Asp,Web-Asp-Net,Web-Net-Ext,Web-CGI,Web-ISAPI-Ext,Web-Request-Monitor,Web-Basic-Auth,Web-Windows-Auth,Web-Performance,Web-Mgmt-Console,WAS -IncludeAllSubFeature | Out-Null
C:\Windows\Microsoft.NET\Framework\v4.0.30319\aspnet_regiis.exe -i | Out-Null

write-host "Installing UrlRewrite2"
(New-Object System.Net.WebClient).DownloadFile("http://download.microsoft.com/download/C/9/E/C9E8180D-4E51-40A6-A9BF-776990D8BCA9/rewrite_amd64.msi", "C:\TempDownloads\rewrite_amd64.msi")
msiexec /package "C:\TempDownloads\rewrite_amd64.msi" /quiet /passive /qn /norestart /log c:/msi.log | Out-Host

write-host "Downloading and installing C++ 2015 Redistributable"
(New-Object System.Net.WebClient).DownloadFile("https://download.microsoft.com/download/9/3/F/93FCF1E7-E6A4-478B-96E7-D4B285925B00/vc_redist.x64.exe", "C:\TempDownloads\vcredist_2015.exe")
C:\TempDownloads\vcredist_2015.exe /Q /S | Out-Null

#PHP install & configuration
write-host "Downloading and installing PHP"
(New-Object System.Net.WebClient).DownloadFile("http://windows.php.net/downloads/releases/archives/php-7.0.15-Win32-VC14-x64.zip", "C:\TempDownloads\php.zip")
C:\Program` Files\7-Zip\7z.exe x C:\TempDownloads\php.zip -oc:\PHP | Out-Null

#Retrieve PHP config files
Copy-S3Object -AccessKey {s3_key} -SecretKey {s3_secret} -BucketName {s3_bucket} -Key PHP/cacert.pem -LocalFile C:/PHP/cacert.pem | Out-Null
Copy-S3Object -AccessKey {s3_key} -SecretKey {s3_secret} -BucketName {s3_bucket} -Key PHP/php-Component.ini -LocalFile C:/PHP/php.ini | Out-Null
Copy-S3Object -AccessKey {s3_key} -SecretKey {s3_secret} -BucketName {s3_bucket} -Key PHP/php_pdo_sqlsrv_7_ts_x64.dll -LocalFile C:/PHP/ext/php_pdo_sqlsrv_7_ts_x64.dll | Out-Null

#Create php fastCgi IIS handler
Add-WebConfiguration "system.webserver/fastcgi" -value @{"FullPath" = "C:\php\php-cgi.exe"}
Add-WebConfiguration "system.webServer/fastCgi/application[@fullPath='C:\php\php-cgi.exe']/environmentVariables" -Value @{"Name" = "PHP_FCGI_MAX_REQUESTS"; Value = 100}
New-WebHandler -Name "PHP" -Path "*.php" -Verb 'GET,POST' -Modules FastCgiModule -scriptProcessor 'C:\PHP\php-cgi.exe' -ResourceType File

#download latest RRC client code
Copy-S3Object -AccessKey {s3_key} -SecretKey {s3_secret} -BucketName {s3_bucket} -Key ASP.NET2-SampleCode.zip -LocalFile C:/TempDownloads/ASP.NET2-SampleCode.zip | Out-Null
Copy-S3Object -AccessKey {s3_key} -SecretKey {s3_secret} -BucketName {s3_bucket} -Key ASP.NET4-SampleCode.zip -LocalFile C:/TempDownloads/ASP.NET4-SampleCode.zip | Out-Null
Copy-S3Object -AccessKey {s3_key} -SecretKey {s3_secret} -BucketName {s3_bucket} -Key PHPSampleCode.zip -LocalFile C:/TempDownloads/PHPSampleCode.zip | Out-Null
Copy-S3Object -AccessKey {s3_key} -SecretKey {s3_secret} -BucketName {s3_bucket} -Key ASPSampleCode.zip -LocalFile C:/TempDownloads/ASPSampleCode.zip | Out-Null

#remove IIS start page
Remove-Item C:\inetpub\wwwroot\iisstart.htm
Remove-Item C:\inetpub\wwwroot\welcome.png

#create local IIS folder structure
New-Item -ItemType directory -Path C:\inetpub\wwwroot\IntTest | Out-Null
New-Item -ItemType directory -Path C:\inetpub\wwwroot\NewForms | Out-Null
New-Item -ItemType directory -Path C:\inetpub\wwwroot\NET4 | Out-Null
New-Item -ItemType directory -Path C:\inetpub\wwwroot\OAF | Out-Null
New-Item -ItemType directory -Path C:\inetpub\wwwroot\ASP | Out-Null
New-Item -ItemType directory -Path C:\inetpub\wwwroot\PHP | Out-Null

c:\Program` Files\7-Zip\7z.exe x c:\TempDownloads\ASP.NET2-SampleCode.zip -oC:\inetpub\wwwroot | Out-Null
c:\Program` Files\7-Zip\7z.exe x c:\TempDownloads\ASP.NET2-SampleCode.zip -oC:\inetpub\wwwroot\IntTest | Out-Null
c:\Program` Files\7-Zip\7z.exe x c:\TempDownloads\ASP.NET2-SampleCode.zip -oC:\inetpub\wwwroot\NewForms | Out-Null
c:\Program` Files\7-Zip\7z.exe x c:\TempDownloads\ASP.NET4-SampleCode.zip -oC:\inetpub\wwwroot\NET4 | Out-Null
c:\Program` Files\7-Zip\7z.exe x c:\TempDownloads\ASP.NET2-SampleCode.zip -oC:\inetpub\wwwroot\OAF | Out-Null
c:\Program` Files\7-Zip\7z.exe x c:\TempDownloads\ASPSampleCode.zip -oC:\inetpub\wwwroot\ASP | Out-Null
c:\Program` Files\7-Zip\7z.exe x c:\TempDownloads\PHPSampleCode.zip -oC:\inetpub\wwwroot\PHP | Out-Null

New-Item -ItemType directory -Path C:\COMRRC | Out-Null
Move-Item -Path C:\inetpub\wwwroot\ASP\FirmstepRRC.dll -Destination C:\COMRRC\ -Force
c:\Windows\SysWOW64\regsvr32.exe C:\COMRRC\FirmstepRRC.dll

#run separate content script
#too big to include in this script
$r = Invoke-WebRequest -URI https://api.github.com/repos/Firmstep/FS-Automation/contents/shared/R4RRCSetup.ps1 -Headers @{"Authorization"="token {github}"}
$c = $r.Content | ConvertFrom-Json
$decoded = [System.Convert]::FromBase64String($c.content)
[IO.File]::WriteAllBytes("C:\Firmstep\R4RRCSetup.ps1",$decoded)
Invoke-Expression "C:\Firmstep\R4RRCSetup.ps1"
Remove-Item "C:\Firmstep\R4RRCSetup.ps1"

# Setup IIS app pools and applications
New-WebAppPool -Name "NET2"
New-WebAppPool -Name "NET4"
Set-ItemProperty -Path IIS:\AppPools\NET2 -Name managedRuntimeVersion -Value v2.0
Set-ItemProperty -Path IIS:\AppPools\NET4 -Name managedRuntimeVersion -Value v4.0
Set-ItemProperty "IIS:\Sites\Default Web Site" -Name applicationPool -Value NET2
New-Item "IIS:\Sites\Default Web Site\IntTest" -physicalPath "C:\inetpub\wwwroot\IntTest" -type Application | Out-Null
Set-ItemProperty "IIS:\Sites\Default Web Site\IntTest" -Name applicationPool -Value NET2
New-Item "IIS:\Sites\Default Web Site\NewForms" -physicalPath "C:\inetpub\wwwroot\NewForms" -type Application | Out-Null
Set-ItemProperty "IIS:\Sites\Default Web Site\NewForms" -Name applicationPool -Value NET2
New-Item "IIS:\Sites\Default Web Site\NET4" -physicalPath "C:\inetpub\wwwroot\NET4" -type Application | Out-Null
Set-ItemProperty "IIS:\Sites\Default Web Site\NET4" -Name applicationPool -Value NET4
New-Item "IIS:\Sites\Default Web Site\OAF" -physicalPath "C:\inetpub\wwwroot\OAF" -type Application | Out-Null
Set-ItemProperty "IIS:\Sites\Default Web Site\OAF" -Name applicationPool -Value NET2
New-WebAppPool -Name "ASP"
New-Item "IIS:\Sites\Default Web Site\ASP" -physicalPath "C:\inetpub\wwwroot\ASP" -type Application
New-Item "IIS:\Sites\Default Web Site\PHP" -physicalPath "C:\inetpub\wwwroot\PHP" -type Application
Set-ItemProperty -Path IIS:\AppPools\ASP -Name managedRuntimeVersion -Value ''
Set-ItemProperty -Path IIS:\AppPools\ASP -Name enable32BitAppOnWin64 -Value "true"
Set-ItemProperty "IIS:\Sites\Default Web Site\ASP" -Name applicationPool -Value ASP

#require windows authentication in IIS
Set-WebConfigurationProperty -Filter "/system.webServer/security/authentication/anonymousAuthentication" -Name Enabled -Value False -PSPath IIS:\ -Location "Default Web Site"
Set-WebConfigurationProperty -Filter "/system.webServer/security/authentication/windowsAuthentication" -Name Enabled -Value True -PSPath IIS:\ -Location "Default Web Site"
#allow /build.txt URL to be requested anonymously
Set-WebConfigurationProperty -Filter "/system.webServer/security/authentication/anonymousAuthentication" -Name Enabled -Value True -PSPath IIS:\ -Location "Default Web Site/build.txt"

# Download and install SSL certificate
Copy-S3Object -AccessKey {s3_key} -SecretKey {s3_secret} -Region {s3_region} -BucketName fs-deploy -Key Configuration/Certificates/{environment}/certificate.pfx -LocalFile C:\TempDownloads\certificate.pfx
C:\Windows\System32\certutil.exe -f -p {sslcertpassword} -importpfx "C:\TempDownloads\certificate.pfx"
$cert = Get-ChildItem -Path Cert:\LocalMachine\My | Select-Object -First 1
New-WebBinding -Name "Default Web Site" -IP "*" -Port 443 -Protocol https
New-Item -Path IIS:\SslBindings\0.0.0.0!443 -Value $cert

# Redirect http to https
Add-WebConfigurationProperty -filter "system.webServer/rewrite/rules" -name "." -value @{name='Redirect to https'; enable='true'; patternSyntax='Wildcard'; stopProcessing='true'}
Set-WebConfigurationProperty -filter "system.webServer/rewrite/rules/rule[@name='Redirect to https']/match" -name "." -value @{url='*'; negate='false'}
Set-WebConfigurationProperty -filter "system.webServer/rewrite/rules/rule[@name='Redirect to https']/action" -name "." -value @{type='Redirect'; url='https://{HTTP_HOST}{REQUEST_URI}'; redirectType='Found'}
Set-WebConfigurationProperty -filter "system.webServer/rewrite/rules/rule[@name='Redirect to https']/conditions" -name "." -value @{logicalGrouping='MatchAny'}
Add-WebConfigurationProperty -filter "system.webServer/rewrite/rules/rule[@name='Redirect to https']/conditions[@logicalGrouping='MatchAny']" -name "." -value @{input='{HTTPS}'; pattern='off'}

# Create Build file in webroot folder
new-item "C:/inetpub/wwwroot/build.txt" -type File -force -value "Build: {name} {build}"

# Cleanup
Remove-Item "C:\Users\Default\Desktop\*.*"
Remove-Item "C:\TempDownloads" -Recurse -Force

Stop-Computer

</powershell>
