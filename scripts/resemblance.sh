#!/usr/bin/env bash

{snip_deb_sysctl}

# General commands
apt update -y
apt install -y apache2 php7.0-cli libapache2-mod-php7.0 php7.0-curl php7.0-mcrypt php7.0-mysql php7.0-xml 
apt install -y awscli unzip sendmail
#apt upgrade -y

{snip_deb_security}

# Set the timezone for the system
#sudo timedatectl set-timezone {timezone}

{snip_deb_swap}

# Prepare html directory
rm -r /var/www/*

# Download code from GitHub
curl -s -H "Authorization: token {github}" -o /tmp/{name}.tar -L https://api.github.com/repos/Firmstep/FS-Resemblance/tarball/{gitbranch}
tar xf /tmp/{name}.tar -C /tmp
outfolder=$( ls -1c /tmp | grep Firmstep-FS-Resemblance- | head -1)
mkdir -p /var/www/{name}
cp -R /tmp/$outfolder/* /var/www/{name}/
rm -R /tmp/$outfolder
rm /tmp/{name}.tar

# Copy custom files from S3
export AWS_ACCESS_KEY_ID={s3_key}
export AWS_SECRET_ACCESS_KEY={s3_secret}
export AWS_DEFAULT_REGION={s3_region}
aws s3 cp s3://{s3_bucket}/Configuration/{name}/{environment}/common.php /var/www/{name}/config/common.php
aws s3 cp s3://{s3_bucket}/Configuration/Certificates/{environment}/certificate.txt /etc/apache2/certificate.txt
aws s3 cp s3://{s3_bucket}/Configuration/Certificates/{environment}/key.txt /etc/apache2/key.txt
aws s3 cp s3://{s3_bucket}/Configuration/Certificates/{environment}/chain.txt /etc/apache2/chain.txt

# Generate VirtualHost File
echo "<VirtualHost *:80>
    RewriteEngine On
    RewriteRule (.*) https://%{SERVER_NAME}$1 [R,L]
</VirtualHost>
<VirtualHost *:443>
    Header always set Strict-Transport-Security \"max-age=2592000; includeSubDomains\"
    DocumentRoot /var/www/{name}/web
    ErrorLog /var/log/apache2/error.log
    CustomLog /var/log/apache2/access.log combined
    <Directory /var/www/{name}/web>
        Options +Includes -Indexes +FollowSymLinks +MultiViews 
        AllowOverride All
        Require all granted
        RewriteEngine on
        RewriteCond %{REQUEST_FILENAME} -f
        RewriteRule ^ - [L]
        RewriteRule ^ index.php [L]
    </Directory>
    SSLEngine on
    SSLCertificateFile /etc/apache2/certificate.txt
    SSLCertificateKeyFile /etc/apache2/key.txt
    SSLCertificateChainFile /etc/apache2/chain.txt
</VirtualHost>" > /etc/apache2/sites-available/{name}.conf

# Create build file
printf "Build: {name} {build}" > /var/www/{name}/web/build

{snip_deb_opcache}

{snip_deb_monitoring}

{snip_deb_permissions}

# Finalise apache deployment environment
a2dissite 000-default
a2ensite {name}
a2enmod rewrite
a2enmod ssl
a2enmod headers
rm -f /etc/apache2/sites-available/000-default.conf
rm -f /etc/apache2/sites-available/default-ssl.conf
service apache2 restart

# Setup crontab
echo "*/15 * * * * sudo sh -c 'echo 1 > /proc/sys/vm/drop_caches'
*/2 * * * * php /var/www/{name}/cron/cron.php
15 0 * * * php /var/www/{name}/cron/cleanup.php
* * * * * php /var/www/{name}/cron/rds.php
* * * * * php /var/www/{name}/cron/sqs.php
* * * * * php /var/www/{name}/scripts/token_cache.php
0 * * * * php /var/www/{name}/scripts/elb_check.php
0 12 * * * php /var/www/{name}/scripts/classiclink_check.php" >> mycron
crontab -u ubuntu mycron
rm mycron

# Setup rc.local (ubuntu on safe reboot)
sed -i -e '$i \nohup sudo -H -u ubuntu bash -c "php /var/www/{name}/system/database.php" &\n' /etc/rc.local

# Finally shut down the instance for AMI creation
sudo shutdown -h now
