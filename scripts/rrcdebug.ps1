<powershell>

Set-ExecutionPolicy Unrestricted -Force

write-host "Configuring timezone"
tzutil.exe /s "{timezone}"

write-host "Creating default directories"
if (-Not (Test-Path "C:\TempDownloads")) { New-Item -ItemType directory -Path C:\TempDownloads | Out-Null }
if (-Not (Test-Path "C:\Firmstep")) { New-Item -ItemType directory -Path C:\Firmstep | Out-Null }

[Net.ServicePointManager]::SecurityProtocol = "tls12, tls11, tls"
$errors = ""

write-host "Downloading and installing 7-Zip"
(New-Object System.Net.WebClient).DownloadFile("http://www.7-zip.org/a/7z1514-x64.msi", "C:\TempDownloads\7zip.msi")
if (Test-Path C:\TempDownloads\7zip.msi) {
    msiexec /package "C:\TempDownloads\7zip.msi" /quiet /passive /qn /norestart | Out-Null
} else {
    $errors += "7-Zip not found. "
}

write-host "Downloading and installing Notepad++"
(New-Object System.Net.WebClient).DownloadFile("https://notepad-plus-plus.org/repository/6.x/6.9.2/npp.6.9.2.Installer.exe", "C:\TempDownloads\npp.exe")
if (Test-Path C:\TempDownloads\npp.exe) {
    C:\TempDownloads\npp.exe /Q /S | Out-Null
} else {
    $errors += "Notepad++ not found. "
}

write-host "Downloading and installing AWS CLI Tools"
(New-Object System.Net.WebClient).DownloadFile("https://s3.amazonaws.com/aws-cli/AWSCLI64.msi", "C:\TempDownloads\AWSCLI64.msi")
if (Test-Path C:\TempDownloads\AWSCLI64.msi) {
    msiexec /package "C:\TempDownloads\AWSCLI64.msi" /quiet /passive /qn /norestart | Out-Null
} else {
    $errors += "AWS CLI Tools not found. "
}


write-host "Downloading and installing Fiddler"
(New-Object System.Net.WebClient).DownloadFile("https://www.telerik.com/docs/default-source/fiddler/fiddlersetup.exe?sfvrsn=2", "C:\TempDownloads\fiddlersetup.exe")
if (Test-Path C:\TempDownloads\fiddlersetup.exe) {
    C:\TempDownloads\fiddlersetup.exe /S | Out-Null
} else {
    $errors += "Fiddler not found. "
}

{snip_win_monitoring}

{snip_win_localadmin}

if ("{environment}" -eq "test") {
    {snip_win_domainjoin}
}

if ($errors) {
    Send-MailMessage -From "{name}@firmstep.com" -To "auckland-team@firmstep.com" -Subject "{name} Automation Error [{environment}]" -SmtpServer "smtp.firmstep.com" -Body "$errors"
}

# Import-Module ServerManager
#Add-WindowsFeature -Name Web-Common-Http,Web-Performance,Web-Mgmt-Console,WAS -IncludeAllSubFeature | Out-Null
#New-Item "C:/inetpub/wwwroot/build.txt" -type File -force -value "Build: {name} {build}"

# Allow remote fiddler connections through firewall
netsh advfirewall firewall add rule name=Fiddler dir=in action=allow protocol=TCP localport=8888

if (-Not (Test-Path "C:\Firmstep")) { New-Item -ItemType directory -Path C:\Firmstep | Out-Null }
$FiddlerReg = @"
Windows Registry Editor Version 5.00

[HKEY_CURRENT_USER\Software\Microsoft\Fiddler2]
"StartupCount"="0"
"FontSize"="8.25"
"AttachOnBoot"="True"
"AllowRemote"="True"
"ReuseServerSockets"="True"
"ReuseClientSockets"="True"
"FiddlerBypass"="<-loopback>;"
"ReportHTTPErrors"="True"
"GatewayType"=dword:00000002
"CaptureCONNECT"="True"
"CaptureFTP"="False"
"EnableIPv6"="True"
"EnableHighResTimers"="False"
"BreakOnImages"="False"
"MapSocketToProcess"="True"
"UseLegacyEncryptionForSaz"="False"
"AutoStreamAudioVideo"="True"
"CaptureHTTPS"="True"
"IgnoreServerCertErrors"="False"
"CheckForUpdates"="True"
"SendTelemetry"="False"
"HookAllConnections"="True"
"HookWithPAC"="False"
"ShowProcessFilter"=dword:00000000
"HTTPSProcessFilter"=dword:00000000
"AutoReloadScript"="True"
"HideOnMinimize"="False"
"AlwaysShowTrayIcon"="False"
"ListenPort"=dword:000022b8
"HotkeyMod"=dword:00000003
"Hotkey"=dword:00000046
"CheckForISA"="True"
"ScriptReferences"=""
"JSEditor"="notepad.exe"

[HKEY_CURRENT_USER\Software\Microsoft\Fiddler2\Dynamic]
"Attached"=dword:00000000

[HKEY_CURRENT_USER\Software\Microsoft\Fiddler2\Prefs]

[HKEY_CURRENT_USER\Software\Microsoft\Fiddler2\Prefs\.default]
"fiddler.inspectors.images.viewmode"="0"
"fiddler.ui.lastview"="Statistics"
"fiddler.lint.http"="False"
"fiddler.updater.offerbetabuilds"="False"
"fiddlerscript.rules.hide304s"="False"
"fiddler.threads.bumpbasepriority"="False"
"fiddler.memory.streamandforgetifover"="2147480000"
"fiddler.ui.showmemorypanel"="False"
"fiddlerscript.rules.autoauth"="False"
"fiddler.ui.font.size"="8.25"
"fiddler.network.https.checkcertificaterevocation"="False"
"fiddler.network.https.nodecryptionhosts"=""
"fiddler.welcomemsg"="Join the Telerik Fiddler Insider program  and get involved in creating the product roadmap and testing new features - http://bit.ly/FiddlerSurvey"

[HKEY_CURRENT_USER\Software\Microsoft\Fiddler2\UI]
"AutoScroll"="True"
"SmartScroll"="True"
"SearchUnmarkOldHits"="True"
"ResetCounterOnClear"="True"
"frmViewer_WState"=dword:00000002
"frmViewer_Top"=dword:000000dc
"frmViewer_Left"=dword:000000dc
"frmViewer_Height"=dword:000001cf
"frmViewer_Width"=dword:000003e0
"pnlSessions_Width"=dword:00000190
"tabsRequest_Height"=dword:000000f5
"lvSessions_Columns"="#~45§Result~50§Protocol~55§Host~120§URL~150§Body~52§Caching~60§Content-Type~80§Process~60§Comments~80§Custom~60"
"@

Add-Content C:\Firmstep\fiddler.reg $FiddlerReg

$StartupScript = @'
#Check for Fiddler keys in registry
$fiddlerPath = Test-Path 'HKCU:\Software\Microsoft\Fiddler2'

if ($fiddlerPath -eq $False) {
	reg import C:\Firmstep\fiddler.reg
}
'@
Add-Content C:\Firmstep\fiddler.ps1 $StartupScript

# Place ps1 script file in user's startup folder
$WScriptShell = New-Object -ComObject WScript.Shell
$Shortcut = $WScriptShell.CreateShortcut("C:\ProgramData\Microsoft\Windows\Start` Menu\Programs\Startup\fiddler.lnk")
$Shortcut.TargetPath = "powershell.exe"
$Shortcut.Arguments  = "C:\Firmstep\fiddler.ps1"
$Shortcut.Save()

# Place shortcut to fiddler on all users desktop
$WScriptShell = New-Object -ComObject WScript.Shell
$Shortcut = $WScriptShell.CreateShortcut("C:\Users\Public\Desktop\Fiddler.lnk")
$Shortcut.TargetPath = "C:\Program Files (x86)\Fiddler2\Fiddler.exe"
$Shortcut.WorkingDirectory = "C:\Program Files (x86)\Fiddler2"
$Shortcut.Save()

# Cleanup
Remove-Item "C:\Users\Default\Desktop\*.*"
Remove-Item "C:\TempDownloads" -Recurse -Force

Stop-Computer

</powershell>
