<powershell>

Set-ExecutionPolicy Unrestricted -Force

write-host "Configuring timezone"
tzutil.exe /s "{timezone}"

write-host "Creating default directories"
if (-Not (Test-Path "C:\TempDownloads")) { New-Item -ItemType directory -Path C:\TempDownloads | Out-Null }
if (-Not (Test-Path "C:\Firmstep")) { New-Item -ItemType directory -Path C:\Firmstep | Out-Null }

[Net.ServicePointManager]::SecurityProtocol = "tls12, tls11, tls"
$errors = ""

write-host "Downloading and installing 7-Zip"
(New-Object System.Net.WebClient).DownloadFile("http://www.7-zip.org/a/7z1514-x64.msi", "C:\TempDownloads\7zip.msi")
if (Test-Path C:\TempDownloads\7zip.msi) {
    msiexec /package "C:\TempDownloads\7zip.msi" /quiet /passive /qn /norestart | Out-Null
} else {
    $errors += "7-Zip not found. "
}

write-host "Downloading and installing Notepad++"
(New-Object System.Net.WebClient).DownloadFile("https://notepad-plus-plus.org/repository/6.x/6.9.2/npp.6.9.2.Installer.exe", "C:\TempDownloads\npp.exe")
if (Test-Path C:\TempDownloads\npp.exe) {
    C:\TempDownloads\npp.exe /Q /S | Out-Null
} else {
    $errors += "Notepad++ not found. "
}

write-host "Downloading and installing AWS CLI Tools"
(New-Object System.Net.WebClient).DownloadFile("https://s3.amazonaws.com/aws-cli/AWSCLI64.msi", "C:\TempDownloads\AWSCLI64.msi")
if (Test-Path C:\TempDownloads\AWSCLI64.msi) {
    msiexec /package "C:\TempDownloads\AWSCLI64.msi" /quiet /passive /qn /norestart | Out-Null
} else {
    $errors += "AWS CLI Tools not found. "
}

{snip_win_monitoring}

{snip_win_localadmin}

if ("{environment}" -eq "test") {
    {snip_win_domainjoin}
}

if ($errors) {
    Send-MailMessage -From "{name}@firmstep.com" -To "auckland-team@firmstep.com" -Subject "{name} Automation Error [{environment}]" -SmtpServer "smtp.firmstep.com" -Body "$errors"
}

Add-WindowsFeature SMTP-Server, Web-Mgmt-Compat, Telnet-Client

# Create SMTP relay domains
$smtpDomains = [wmiclass]'root\MicrosoftIISv2:IISSmtpDomain'

$newDomain = $smtpDomains.CreateInstance()
$newDomain.Name = "SmtpSvc/1/Domain/firmstep.com"
$newDomain.Put() | Out-Null

$newDomainTest = $smtpDomains.CreateInstance()
$newDomainTest.Name = "SmtpSvc/1/Domain/{local_domain}"
$newDomainTest.Put() | Out-Null

$settings = [wmiclass]'root\MicrosoftIISv2:IISSmtpDomainSetting'

$newConfig = $settings.CreateInstance()
$newConfig.RouteAction = 4098 
$newConfig.RouteActionString = "smtp.firmstep.com"
$newConfig.Name = "SmtpSvc/1/Domain/firmstep.com"
$newConfig.Put() | Out-Null

$newConfigTest = $settings.CreateInstance()
$newConfigTest.RouteAction = 4098
$newConfigTest.RouteActionString = "imap.{local_domain}"
$newConfigTest.Name = "SmtpSvc/1/Domain/{local_domain}"
$newConfigTest.Put() | Out-Null

# Set SMTP service to start automatically
Set-Service SMTPSvc -startuptype "Automatic"
Start-Service SMTPSvc

# Configure relay IP address list, using vbscript
$relayIPs = "{relayips}".Split(",") | ForEach { $_.Replace("/",",") }
$relayIPList = $relayIPs -Join '","'

$relayScript = @"
Set smtp = GetObject("IIS://localhost/smtpsvc/1")
Set relay = smtp.Get("RelayIpList")
relay.GrantByDefault = False
relay.IPGrant = Array("$relayIPList")
smtp.Put "RelayIpList",relay
smtp.SetInfo
"@
[IO.File]::WriteAllText("C:\inetpub\AdminScripts\smtprelay.vbs",$relayScript)
cscript C:\inetpub\AdminScripts\smtprelay.vbs | Out-Null

# Create and register SMTP hook script
$hookScript = @"
<SCRIPT LANGUAGE="VBScript">

Const RECIP_LIST = "http://schemas.microsoft.com/cdo/smtpenvelope/recipientlist"
Const MESSAGE_STATUS = "http://schemas.microsoft.com/cdo/smtpenvelope/messagestatus"

Sub ISMTPOnArrival_OnArrival(ByVal Msg, EventStatus)
	On Error Resume Next

	Dim FSO, LogFile
	Set FSO = CreateObject("Scripting.FileSystemObject")
	Set LogFile = FSO.OpenTextFile("C:\Mail.log", 8, True)
	
	Dim Fields, RecipientList, UpdatedList, Recipients, Recipient, RecipientDomain, i
	Set Fields = Msg.EnvelopeFields
	RecipientList = Fields(RECIP_LIST).Value
	LogFile.WriteLine "Recipients: " & RecipientList
	Recipients = Split(RecipientList, ";")
	UpdatedList = ""
		
	For i = 0 To UBound(Recipients)
		Recipient = Recipients(i)
		If Len(Recipient) > 0 Then
			If InStr(1, Recipient, "@") > 0 Then
				RecipientDomain = Split(Recipient, "@")(1)
				If RecipientDomain = "firmstep.com" Or RecipientDomain = "{local_domain}" Then
					UpdatedList = UpdatedList & Recipient & ";"
				Else
					LogFile.WriteLine "Removing " & Recipient
				End If
			End If
		End If
	Next

	If Len(UpdatedList) = 0 Then
		'no valid recipients
		Fields(MESSAGE_STATUS).Value = 2 'abort
		Fields.Update
		Msg.DataSource.Save ' Commit changes
		LogFile.WriteLine "No valid recipients!"
	Else
		If UpdatedList <> RecipientList Then
			'some recipients have been removed
			Fields(RECIP_LIST).Value = UpdatedList
			Fields.Update
			Msg.DataSource.Save ' Commit changes
			LogFile.WriteLine "Some invalid recipients!"
			LogFile.WriteLine UpdatedList & " <> " & RecipientList
		Else
			LogFile.WriteLine "Passing through unaltered"
		End If
	End If
	
	If Err.Number <> 0 Then
		LogFile.WriteLine "Error: " & Err.Description
	End If
	LogFile.Close
	
	EventStatus = 0 ' CDO_RUN_NEXT_SINK
End Sub
</SCRIPT>
"@
[IO.File]::WriteAllText("C:\inetpub\AdminScripts\smtphook.vbs",($hookScript -replace "`n", "`r`n"))

$r = Invoke-WebRequest -URI https://api.github.com/repos/Firmstep/FS-Automation/contents/shared/smtpreg.vbs -Headers @{"Authorization"="token {github}"}
$c = $r.Content | ConvertFrom-Json
$decoded = [System.Convert]::FromBase64String($c.content)
[IO.File]::WriteAllBytes("C:\inetpub\AdminScripts\smtpreg.vbs",$decoded)

cscript C:\inetpub\AdminScripts\smtpreg.vbs /add 1 onarrival SMTPHook CDO.SS_SMTPOnArrivalSink "mail from=*" | Out-Null
cscript C:\inetpub\AdminScripts\smtpreg.vbs /setprop 1 onarrival SMTPHook Sink ScriptName C:\inetpub\AdminScripts\smtphook.vbs | Out-Null
cscript C:\inetpub\AdminScripts\smtpreg.vbs /delprop 1 onarrival SMTPHook Source Rule | Out-Null

# Create Build file in webroot folder
new-item "C:/inetpub/wwwroot/build.txt" -type File -force -value "Build: {name} {build}"

# Cleanup
Remove-Item "C:\Users\Default\Desktop\*.*"
Remove-Item "C:\TempDownloads" -Recurse -Force

Stop-Computer

</powershell>
