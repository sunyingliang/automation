#!/usr/bin/env bash
	
{snip_deb_sysctl}

# General commands
apt update -y
apt install -y apache2 awscli default-jdk
#apt upgrade -y

{snip_deb_security}

{snip_deb_timezone}

{snip_deb_swap}

# Download Solr
curl -s -o /tmp/solr-5.4.1.tgz -L http://archive.apache.org/dist/lucene/solr/5.4.1/solr-5.4.1.tgz
tar -xf /tmp/solr-5.4.1.tgz solr-5.4.1/bin/install_solr_service.sh --strip-components=2 && mv install_solr_service.sh /tmp/install_solr_service.sh

# Run Solr service installation script
/tmp/install_solr_service.sh /tmp/solr-5.4.1.tgz -d /data/var/solr

# Clear directory for mountpoint
rm -rf /data/*

# Set AWS S3 credentials
mkdir /root/.aws/
printf "[default]\naws_access_key_id = {s3_key}\naws_secret_access_key = {s3_secret}\nregion = {s3_region}" > /root/.aws/config

# Copy custom files from S3
aws s3 cp s3://{s3_bucket}/Configuration/{name}/{environment}/htpasswd.txt /etc/apache2/.htpasswd
aws s3 cp s3://{s3_bucket}/Configuration/Certificates/{environment}/certificate.txt /etc/apache2/certificate.txt
aws s3 cp s3://{s3_bucket}/Configuration/Certificates/{environment}/key.txt /etc/apache2/key.txt
aws s3 cp s3://{s3_bucket}/Configuration/Certificates/{environment}/chain.txt /etc/apache2/chain.txt
rm /root/.aws/config

# Generate VirtualHost File
echo '<VirtualHost *:80>
    RewriteEngine On
    RewriteRule (.*) https://%{SERVER_NAME}$1 [R,L]
</VirtualHost>
<VirtualHost *:443>
    ErrorLog /var/log/apache2/error.log
    CustomLog /var/log/apache2/access.log combined
    ProxyPreserveHost on
    ProxyRequests off
    RewriteEngine On
    RewriteRule ^\/solr(.*)$ $1 [L,R]
    ProxyPass / http://localhost:8983/solr/
    ProxyPassReverse / http://localhost:8983/solr/
	<Proxy *>
        Require all granted
        Authtype Basic
        Authname "Password Required"
        AuthUserFile /etc/apache2/.htpasswd
        Require valid-user
    </Proxy>
    SSLEngine on
    SSLCertificateFile /etc/apache2/certificate.txt
    SSLCertificateKeyFile /etc/apache2/key.txt
    SSLCertificateChainFile /etc/apache2/chain.txt
</VirtualHost>' > /etc/apache2/sites-available/{name}.conf

# Create build file
printf "Build: {name} {build}" > /opt/solr/server/solr-webapp/webapp/build
rm /var/www/html/index.html
a2dismod autoindex -f

{snip_deb_opcache}

{snip_deb_monitoring}

{snip_deb_permissions}

# Finalise apache deployment environment
a2dissite 000-default
a2ensite {name}
a2enmod rewrite
a2enmod proxy
a2enmod proxy_http
a2enmod ssl
rm -f /etc/apache2/sites-available/000-default.conf
rm -f /etc/apache2/sites-available/default-ssl.conf
service apache2 restart

{snip_deb_dropcache}

# Setup rc.local (ubuntu on safe reboot)
sed -i -e '$i \nohup mount /dev/xvdf /data &\n' /etc/rc.local

# Finally shut down the instance
sudo shutdown -h now
