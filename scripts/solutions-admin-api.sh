#!/usr/bin/env bash

{snip_deb_sysctl}

# General commands
apt update -y
apt install -y apache2 php7.0-cli libapache2-mod-php7.0 php7.0-mysql php7.0-curl php7.0-mbstring php7.0-soap php7.0-xml php-ssh2
apt install -y awscli unzip mysql-client
#apt upgrade -y

{snip_deb_security}

{snip_deb_timezone}

{snip_deb_swap}

# Prepare directories
rm -r /var/www/*

# Download code from GitHub
curl -s -H "Authorization: token {github}" -o /tmp/{name}.tar -L https://api.github.com/repos/Firmstep/FS-Solutions/tarball/{gitbranch}
tar xf /tmp/{name}.tar -C /tmp
outfolder=$( ls -1c /tmp | grep Firmstep-FS-Solutions- | head -1)
mkdir /var/www/{name}/
cp -R /tmp/$outfolder/AdminAPI/* /var/www/{name}/
rm -R /tmp/$outfolder
rm /tmp/{name}.tar

# Download Common from Github
curl -s -H "Authorization: token {github}" -o /tmp/common.tar -L https://api.github.com/repos/Firmstep/FS-Common/tarball/{common_gitbranch}
tar xf /tmp/common.tar -C /tmp
outfolder=$( ls -1c /tmp | grep Firmstep-FS-Common- | head -1)
cp -R /tmp/$outfolder/lim/current/client/* /var/www/{name}/classes/Common/
cp -R /tmp/$outfolder/nzlog/current/php/* /var/www/{name}/classes/Common/
rm -R /tmp/$outfolder
rm /tmp/common.tar

# Copy custom files from S3
export AWS_ACCESS_KEY_ID={s3_key}
export AWS_SECRET_ACCESS_KEY={s3_secret}
export AWS_DEFAULT_REGION={s3_region}
aws s3 cp s3://{s3_bucket}/Configuration/{name}/{environment}/common.php /var/www/{name}/config/common.php
aws s3 cp s3://{s3_bucket}/Configuration/{name}/{environment}/lumberjack.xml /var/www/{name}/config/lumberjack.xml
aws s3 cp s3://{s3_bucket}/Configuration/Certificates/{environment}/certificate.txt /etc/apache2/certificate.txt
aws s3 cp s3://{s3_bucket}/Configuration/Certificates/{environment}/key.txt /etc/apache2/key.txt
aws s3 cp s3://{s3_bucket}/Configuration/Certificates/{environment}/chain.txt /etc/apache2/chain.txt

# VirtualHost
echo '<VirtualHost *:80>
    RewriteEngine On
    RewriteRule (.*) https://%{SERVER_NAME}$1 [R,L]
</VirtualHost>
<VirtualHost *:443>
    DocumentRoot /var/www/{name}/public
    ErrorLog /var/log/apache2/error.log
    CustomLog /var/log/apache2/access.log combined
    <Directory /var/www/{name}/public>
        Options +Includes -Indexes +FollowSymLinks +MultiViews 
        AllowOverride All
        Require all granted
    </Directory>
    SSLEngine on
    SSLCertificateFile /etc/apache2/certificate.txt
    SSLCertificateKeyFile /etc/apache2/key.txt
    SSLCertificateChainFile /etc/apache2/chain.txt
</VirtualHost>' > /etc/apache2/sites-available/{name}.conf

{snip_deb_mpmconfig}

# Create build file
printf "Build: {name} {build}" > /var/www/{name}/public/build

{snip_deb_opcache}

{snip_deb_monitoring}

{snip_deb_permissions}

# Finalise apache deployment environment
a2dissite 000-default
a2enmod rewrite
a2enmod ssl
a2ensite {name}
rm -f /etc/apache2/sites-available/000-default.conf
rm -f /etc/apache2/sites-available/default-ssl.conf
service apache2 restart

# Setup crontab
echo "*/15 * * * * sudo sh -c 'echo 1 > /proc/sys/vm/drop_caches'
* * * * * php /var/www/{name}/scripts/token_cache.php
{cron_check_customer_database} php /var/www/{name}/scripts/check_customer_database.php
{cron_publish_holiday} php /var/www/{name}/scripts/publish_holiday_list.php
{cron_check_customers} php /var/www/{name}/scripts/check_master_customer_connections.php
{cron_check_customers} php /var/www/{name}/scripts/check_master_customer_schema.php
{cron_check_holiday_function} php /var/www/{name}/scripts/check_customer_holiday_function.php
{cron_reset_feature_started} php /var/www/{name}/scripts/reset_feature_started.php
{orphan_report} sudo php /var/www/{name}/scripts/orphan_report.php
{cron_watchdog_symology} php /var/www/{name}/scripts/watchdog.php Symology
{cron_watchdog_confirm} php /var/www/{name}/scripts/watchdog.php Confirm
{cron_watchdog_mayrise} php /var/www/{name}/scripts/watchdog.php Mayrise
{cron_watchdog_uniform} php /var/www/{name}/scripts/watchdog.php Uniform" >> mycron
crontab -u ubuntu mycron
rm mycron

# Setup rc.local (ubuntu on safe reboot)
sed -i -e '$i \nohup sudo -H -u ubuntu bash -c "sleep 5m && [ $(date +%M) -gt 5 ] && [ $(date +%M) -lt 55 ] && php /var/www/{name}/scripts/checkCustomerDatabase.php" &\n' /etc/rc.local

# Finally shut down the instance for AMI creation
sudo shutdown -h now
