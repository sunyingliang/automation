#!/usr/bin/env bash

{snip_deb_sysctl}

# General commands
apt update -y
apt install -y apache2 php7.0-cli libapache2-mod-php7.0 php7.0-mysql php7.0-xml php7.0-mbstring php7.0-curl php7.0-soap
apt install -y awscli unzip
#apt upgrade -y

{snip_deb_security}

{snip_deb_timezone}

{snip_deb_swap}

# Prepare directories
rm -r /var/www/*

# Download code from GitHub
curl -s -H "Authorization: token {github}" -o /tmp/{name}.tar -L https://api.github.com/repos/Firmstep/FS-Solutions/tarball/{gitbranch}
tar xf /tmp/{name}.tar -C /tmp
outfolder=$( ls -1c /tmp | grep Firmstep-FS-Solutions- | head -1)
mkdir /var/www/{name}/
cp -R /tmp/$outfolder/CustomerAPI/* /var/www/{name}/
rm -R /tmp/$outfolder
rm /tmp/{name}.tar

# Download Common from GitHub
curl -s -H "Authorization: token {github}" -o /tmp/common.tar -L https://api.github.com/repos/Firmstep/FS-Common/tarball/{common_gitbranch}
tar xf /tmp/common.tar -C /tmp
outfolder=$( ls -1c /tmp | grep Firmstep-FS-Common- | head -1)
cp -R /tmp/$outfolder/nzlog/current/php/* /var/www/{name}/classes/Common/
rm -R /tmp/$outfolder
rm /tmp/common.tar

# Copy custom files from S3
export AWS_ACCESS_KEY_ID={s3_key}
export AWS_SECRET_ACCESS_KEY={s3_secret}
export AWS_DEFAULT_REGION={s3_region}
aws s3 cp s3://{s3_bucket}/Configuration/{name}/{environment}/common.php /var/www/{name}/config/common.php
aws s3 cp s3://{s3_bucket}/Configuration/{name}/{environment}/lumberjack.xml /var/www/{name}/config/lumberjack.xml

# VirtualHost
echo '<VirtualHost *:80>
    DocumentRoot /var/www/{name}/public
    ErrorLog /var/log/apache2/error.log
    CustomLog /var/log/apache2/access.log combined
    <Directory /var/www/{name}/public>
        Options -Indexes +FollowSymLinks +MultiViews
        AllowOverride All
        Require all granted
    </Directory>
</VirtualHost>' > /etc/apache2/sites-available/{name}.conf

{snip_deb_mpmconfig}

# Create build file
printf "Build: {name} {build}" > /var/www/{name}/public/build

{snip_deb_opcache}

{snip_deb_monitoring}
 
{snip_deb_permissions}

# Finalise apache deployment environment
a2dissite 000-default
a2enmod rewrite
a2ensite {name}
rm -f /etc/apache2/sites-available/000-default.conf
rm -f /etc/apache2/sites-available/default-ssl.conf
service apache2 restart

{snip_deb_dropcache}

# Finally shut down the instance for AMI creation
sudo shutdown -h now
