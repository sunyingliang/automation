<powershell>

Set-ExecutionPolicy Unrestricted -Force

write-host "Configuring timezone"
tzutil.exe /s "{timezone}"

write-host "Creating default directories"
if (-Not (Test-Path "C:\TempDownloads")) { New-Item -ItemType directory -Path C:\TempDownloads | Out-Null }
if (-Not (Test-Path "C:\Firmstep")) { New-Item -ItemType directory -Path C:\Firmstep | Out-Null }

[Net.ServicePointManager]::SecurityProtocol = "tls12, tls11, tls"
$errors = ""

write-host "Downloading and installing 7-Zip"
(New-Object System.Net.WebClient).DownloadFile("http://www.7-zip.org/a/7z1514-x64.msi", "C:\TempDownloads\7zip.msi")
if (Test-Path C:\TempDownloads\7zip.msi) {
    msiexec /package "C:\TempDownloads\7zip.msi" /quiet /passive /qn /norestart | Out-Null
} else {
    $errors += "7-Zip not found. "
}

write-host "Downloading and installing Notepad++"
(New-Object System.Net.WebClient).DownloadFile("https://notepad-plus-plus.org/repository/6.x/6.9.2/npp.6.9.2.Installer.exe", "C:\TempDownloads\npp.exe")
if (Test-Path C:\TempDownloads\npp.exe) {
    C:\TempDownloads\npp.exe /Q /S | Out-Null
} else {
    $errors += "Notepad++ not found. "
}

write-host "Downloading and installing AWS CLI Tools"
(New-Object System.Net.WebClient).DownloadFile("https://s3.amazonaws.com/aws-cli/AWSCLI64.msi", "C:\TempDownloads\AWSCLI64.msi")
if (Test-Path C:\TempDownloads\AWSCLI64.msi) {
    msiexec /package "C:\TempDownloads\AWSCLI64.msi" /quiet /passive /qn /norestart | Out-Null
} else {
    $errors += "AWS CLI Tools not found. "
}

{snip_win_monitoring}

{snip_win_localadmin}

if ("{environment}" -eq "test") {
    {snip_win_domainjoin}
}

write-host "Downloading and installing SQLSysClrTypes"
(New-Object System.Net.WebClient).DownloadFile("https://download.microsoft.com/download/1/3/0/13089488-91FC-4E22-AD68-5BE58BD5C014/ENU/x64/SQLSysClrTypes.msi", "C:\TempDownloads\SQLSysClrTypes.msi")
if (Test-Path C:\TempDownloads\SQLSysClrTypes.msi) {
    msiexec /package "C:\TempDownloads\SQLSysClrTypes.msi" /quiet /passive /qn /norestart | Out-Null
} else {
    $errors += "SQLSysClrTypes not found. "
}
 
write-host "Downloading and installing SharedManagementObjects"
(New-Object System.Net.WebClient).DownloadFile("https://download.microsoft.com/download/1/3/0/13089488-91FC-4E22-AD68-5BE58BD5C014/ENU/x64/SharedManagementObjects.msi", "C:\TempDownloads\SharedManagementObjects.msi")
if (Test-Path C:\TempDownloads\SharedManagementObjects.msi) {
    msiexec /package "C:\TempDownloads\SharedManagementObjects.msi" /quiet /passive /qn /norestart | Out-Null
} else {
    $errors += "SharedManagementObjects not found. "
}

if ($errors) {
    Send-MailMessage -From "{name}@firmstep.com" -To "auckland-team@firmstep.com" -Subject "{name} Automation Error [{environment}]" -SmtpServer "smtp.firmstep.com" -Body "$errors"
}

$wr = [System.Net.HttpWebRequest]::Create("https://api.github.com/repos/Firmstep/FS-SQL-Backups/zipball/master")
$wr.Headers.Add("Authorization", "token {github}")
$wr.UserAgent = "Firmstep"
$resp = $wr.GetResponse()
$str = $resp.GetResponseStream()
$file = [System.IO.File]::Create("C:\TempDownloads\FS-SQL-Backups.zip")
$str.CopyTo($file)
$file.Close()

c:\Program` Files\7-Zip\7z.exe x C:\TempDownloads\FS-SQL-Backups.zip -oC:\TempDownloads\Extracted\ | Out-Null
$folder = Get-ChildItem C:\TempDownloads\Extracted\* | Select -ExpandProperty FullName
move $folder C:\SQLJobs

Copy-S3Object -AccessKey {s3_key} -SecretKey {s3_secret} -Region {s3_region} -BucketName {s3_bucket} -Key Configuration/{name}/{environment}/config.xml -LocalFile C:/SQLJobs/config.xml | Out-Null
Copy-S3Object -AccessKey {s3_key} -SecretKey {s3_secret} -Region {s3_region} -BucketName {s3_bucket} -Key Configuration/{name}/{environment}/createtasks.ps1 -LocalFile C:/SQLJobs/createtasks.ps1 | Out-Null
Copy-S3Object -AccessKey {s3_key} -SecretKey {s3_secret} -Region {s3_region} -BucketName {s3_bucket} -Key Configuration/{name}/{environment}/Scripts/config.xml -LocalFile C:/SQLJobs/Scripts/config.xml
C:/SQLJobs/createtasks.ps1
C:/SQLJobs/Scripts/Setup.ps1
C:/SQLJobs/FullSetups.ps1

Copy-S3Object -AccessKey {s3_key} -SecretKey {s3_secret} -Region {s3_region} -BucketName {s3_bucket} -Key SQLMigrationTool.zip -LocalFile C:/TempDownloads/SQLMigrationTool.zip | Out-Null
c:\Program` Files\7-Zip\7z.exe x c:\TempDownloads\SQLMigrationTool.zip -oC:\SQLMigrationTool\
Copy-S3Object -AccessKey {s3_key} -SecretKey {s3_secret} -Region {s3_region} -BucketName {s3_bucket} -Key Configuration/{name}/{environment}/SQLMigrationTool.xml -LocalFile C:\SQLMigrationTool\config.xml | Out-Null

# Create desktop shortcut for SQL Migration Tool
$WScriptShell = New-Object -ComObject WScript.Shell
$Shortcut = $WScriptShell.CreateShortcut("C:\Users\Public\Desktop\SQLMigrationTool.lnk")
$Shortcut.TargetPath = "C:\SQLMigrationTool\SQLMigrationTool.exe"
$Shortcut.WorkingDirectory = "C:\SQLMigrationTool\"
$Shortcut.Save()

# Create Build file in webroot folder
New-Item "C:/inetpub/wwwroot/build.txt" -type File -force -value "Build: {name} {build}"

# Cleanup
Remove-Item "C:\Users\Default\Desktop\*.*"
Remove-Item "C:\TempDownloads" -Recurse -Force

Stop-Computer

</powershell>
