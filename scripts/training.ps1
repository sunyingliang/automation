<powershell>

Set-ExecutionPolicy Unrestricted -Force

write-host "Configuring timezone"
tzutil.exe /s "{timezone}"

write-host "Creating default directories"
if (-Not (Test-Path "C:\TempDownloads")) { New-Item -ItemType directory -Path C:\TempDownloads | Out-Null }
if (-Not (Test-Path "C:\Firmstep")) { New-Item -ItemType directory -Path C:\Firmstep | Out-Null }

[Net.ServicePointManager]::SecurityProtocol = "tls12, tls11, tls"
$errors = ""

write-host "Downloading and installing 7-Zip"
(New-Object System.Net.WebClient).DownloadFile("http://www.7-zip.org/a/7z1514-x64.msi", "C:\TempDownloads\7zip.msi")
if (Test-Path C:\TempDownloads\7zip.msi) {
    msiexec /package "C:\TempDownloads\7zip.msi" /quiet /passive /qn /norestart | Out-Null
} else {
    $errors += "7-Zip not found. "
}

write-host "Downloading and installing Notepad++"
(New-Object System.Net.WebClient).DownloadFile("https://notepad-plus-plus.org/repository/6.x/6.9.2/npp.6.9.2.Installer.exe", "C:\TempDownloads\npp.exe")
if (Test-Path C:\TempDownloads\npp.exe) {
    C:\TempDownloads\npp.exe /Q /S | Out-Null
} else {
    $errors += "Notepad++ not found. "
}

write-host "Downloading and installing AWS CLI Tools"
(New-Object System.Net.WebClient).DownloadFile("https://s3.amazonaws.com/aws-cli/AWSCLI64.msi", "C:\TempDownloads\AWSCLI64.msi")
if (Test-Path C:\TempDownloads\AWSCLI64.msi) {
    msiexec /package "C:\TempDownloads\AWSCLI64.msi" /quiet /passive /qn /norestart | Out-Null
} else {
    $errors += "AWS CLI Tools not found. "
}

{snip_win_monitoring}

{snip_win_localadmin}

write-host "Downloading and installing SSMS"
(New-Object System.Net.WebClient).DownloadFile("https://download.microsoft.com/download/4/2/A/42A5A62F-9290-45CB-84CF-6A4E17888FDE/SQLManagementStudio_x64_ENU.exe", "C:\TempDownloads\ssms.exe")
if (Test-Path C:\TempDownloads\ssms.exe){
    C:\TempDownloads\ssms.exe /QUIET /IACCEPTSQLSERVERLICENSETERMS /FEATURES=SSMS /ACTION=Install | Out-Null
} else {
    $errors += "SSMS not found. "
}

write-host "Downloading and installing SQLEXPR_x64_ENU.exe" 
(New-Object System.Net.WebClient).DownloadFile("https://download.microsoft.com/download/0/4/B/04BE03CD-EAF3-4797-9D8D-2E08E316C998/SQLEXPR_x64_ENU.exe", "C:\TempDownloads\SQLEXPR_x64_ENU.exe") 
if (Test-Path C:\TempDownloads\SQLEXPR_x64_ENU.exe) { 
    C:\TempDownloads\SQLEXPR_x64_ENU.exe /Q /Action=Install /IACCEPTSQLSERVERLICENSETERMS /FEATURES=SQLEngine,BC,Conn /InstanceName=MSSQLSERVER /SECURITYMODE=SQL "/SAPWD={adminpassword}" /TCPENABLED=1 /SQLSVCACCOUNT=SYSTEM | Out-Null 
} else { 
    $errors += "SQLEXPR_x64_ENU not found. " 
} 

if ("{environment}" -eq "test") {
    {snip_win_domainjoin}
}

if ($errors) {
    Send-MailMessage -From "{name}@firmstep.com" -To "auckland-team@firmstep.com" -Subject "{name} Automation Error [{environment}]" -SmtpServer "smtp.firmstep.com" -Body "$errors"
}

Write-Host "Installing IIS"
Import-Module ServerManager
Add-WindowsFeature -Name Web-Common-Http,Web-Net-Ext,Web-Asp-Net,Web-ISAPI-Ext,Web-ISAPI-Filter,Web-Http-Logging,Web-Filtering,Web-Performance,Web-Mgmt-Console -IncludeAllSubFeature | Out-Null

# Download and install SSL certificate
Copy-S3Object -AccessKey {s3_key} -SecretKey {s3_secret} -BucketName {s3_bucket} -Key Configuration/Certificates/{environment}/certificate.pfx -LocalFile C:\TempDownloads\certificate.pfx
C:\Windows\System32\certutil.exe -f -p {sslcertpassword} -importpfx "C:\TempDownloads\certificate.pfx"
$cert = Get-ChildItem -Path Cert:\LocalMachine\My | Select-Object -First 1
New-WebBinding -Name "Default Web Site" -IP "*" -Port 443 -Protocol https
New-Item -Path IIS:\SslBindings\0.0.0.0!443 -Value $cert

#Download LIM from S3
Copy-S3Object -AccessKey {s3_key} -SecretKey {s3_secret} -BucketName {s3_bucket} -Key LIM-deploy.zip -LocalFile C:/TempDownloads/LIM.zip | Out-Null
c:\Program` Files\7-Zip\7z.exe x c:\TempDownloads\LIM.zip -oc:\LIM | Out-Null
New-Item "IIS:\Sites\Default Web Site\LIM" -physicalPath "C:\LIM" -type Application | Out-Null

$config = @" 
<appSettings>
	<add key="aes_key" value="{lim_key}"/>
	<add key="aes_iv" value="{lim_iv}"/>

	<add key="max_request_length" value="419430400"/> 
	
	<add key="smtpHost" value="{lim_smtp}"/>
    <add key="smtpPort" value="25"/>
	<add key="smtpUserName" value=""/>
    <add key="smtpPassword" value=""/>
	
	<add key="storage" value=""/>
</appSettings>
"@ 
New-Item "C:\LIM\lim.config" -type File -force -value $config 

# Download latest code from GitHub
Invoke-RestMethod -Uri https://api.github.com/repos/Firmstep/FS-Training-Materials/zipball/{gitbranch} -Headers @{"Authorization" = "token {github}"} -OutFile C:/TempDownloads/Training.zip
c:\Program` Files\7-Zip\7z.exe x c:\TempDownloads\Training.zip -oc:\TempDownloads\ | Out-Null
Get-ChildItem C:\TempDownloads -Directory -Filter "Firmstep-FS-Training-Materials-*" | Select-Object -First 1 -OutVariable out
Move-Item $out.FullName C:\Training

#point IIS site at new folder
Set-ItemProperty 'IIS:\Sites\Default Web Site\' -name physicalPath -value C:\Training\web

#Verify that SQL Server installation has completed, then create blank database
$connectionString = "Server=localhost;uid=sa;pwd={adminpassword};"
$connectCount = 0
$databaseName = "Training"

While (1) {
	Try {
		$connection = New-Object System.Data.SQLClient.SQLConnection($connectionString)
		$connection.Open()
		Break
	}
	Catch {
		# 10m timeout, check once per 10s
		$connectCount++
		Start-Sleep -s (10)
		if ($connectCount -eq 60) {
			PostSlackMessage "Training SQL Server connection failed - automation aborted!"
			Exit
		}
		else {
			PostSlackMessage "Training SQL Server connection error: $($_.Exception.Message), retrying in 10 sec"
		}			
	}
}
$command = New-Object System.Data.SQLClient.SQLCommand("CREATE DATABASE $databaseName", $connection)
$command.ExecuteNonQuery()

$config = @" 
<appSettings>
	<add key="ConnectionString" value="$($connectionString)Database=$($databaseName);"/>
</appSettings>
"@ 
New-Item "C:\Training\settings.config" -type File -force -value $config 

Invoke-Expression "C:\Training\SQLSchema.ps1"
Invoke-Expression "C:\Training\SQLData.ps1"

# Create Build file in webroot folder
new-item "C:\Training\web\build.txt" -type File -force -value "Build: {name} {build}"

# Cleanup
Remove-Item "C:\Users\Default\Desktop\*.*"
Remove-Item "C:\TempDownloads" -Recurse -Force

Stop-Computer

</powershell>
