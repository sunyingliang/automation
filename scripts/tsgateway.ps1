<powershell>

Set-ExecutionPolicy Unrestricted -Force

write-host "Configuring timezone"
tzutil.exe /s "{timezone}"

write-host "Creating default directories"
if (-Not (Test-Path "C:\TempDownloads")) { New-Item -ItemType directory -Path C:\TempDownloads | Out-Null }
if (-Not (Test-Path "C:\Firmstep")) { New-Item -ItemType directory -Path C:\Firmstep | Out-Null }

[Net.ServicePointManager]::SecurityProtocol = "tls12, tls11, tls"
$errors = ""

write-host "Downloading and installing 7-Zip"
(New-Object System.Net.WebClient).DownloadFile("http://www.7-zip.org/a/7z1514-x64.msi", "C:\TempDownloads\7zip.msi")
if (Test-Path C:\TempDownloads\7zip.msi) {
    msiexec /package "C:\TempDownloads\7zip.msi" /quiet /passive /qn /norestart | Out-Null
} else {
    $errors += "7-Zip not found. "
}

write-host "Downloading and installing Notepad++"
(New-Object System.Net.WebClient).DownloadFile("https://notepad-plus-plus.org/repository/6.x/6.9.2/npp.6.9.2.Installer.exe", "C:\TempDownloads\npp.exe")
if (Test-Path C:\TempDownloads\npp.exe) {
    C:\TempDownloads\npp.exe /Q /S | Out-Null
} else {
    $errors += "Notepad++ not found. "
}

write-host "Downloading and installing AWS CLI Tools"
(New-Object System.Net.WebClient).DownloadFile("https://s3.amazonaws.com/aws-cli/AWSCLI64.msi", "C:\TempDownloads\AWSCLI64.msi")
if (Test-Path C:\TempDownloads\AWSCLI64.msi) {
    msiexec /package "C:\TempDownloads\AWSCLI64.msi" /quiet /passive /qn /norestart | Out-Null
} else {
    $errors += "AWS CLI Tools not found. "
}

{snip_win_monitoring}

{snip_win_localadmin}

if ("{environment}" -eq "test") {
    {snip_win_domainjoin}
}

if ($errors) {
    Send-MailMessage -From "{name}@firmstep.com" -To "auckland-team@firmstep.com" -Subject "{name} Automation Error [{environment}]" -SmtpServer "smtp.firmstep.com" -Body "$errors"
}

Add-WindowsFeature -Name RDS-Gateway -IncludeAllSubFeature
Add-WindowsFeature -Name RSAT-AD-Tools -IncludeAllSubFeature

Copy-S3Object -AccessKey {s3_key} -SecretKey {s3_secret} -Region {s3_region} -BucketName {s3_bucket} -Key Configuration/Certificates/{environment}/certificate.pfx -LocalFile C:\Firmstep\certificate.pfx | Out-Null
C:\Windows\System32\certutil.exe -f -p {sslcertpassword} -importpfx "C:\Firmstep\certificate.pfx"

Import-Module RemoteDesktopServices

$thumb = (Get-ChildItem -Path Cert:\LocalMachine\My  | Select-Object -first 1).Thumbprint
Set-Item -Path "RDS:\GatewayServer\SSLCertificate\Thumbprint" $thumb

# Cannot create CAP/RAP until server has been joined to domain
#so create another script for this and run it on reboot
$script = @'
Import-Module RemoteDesktopServices
$configured = (Get-ChildItem -Path RDS:\GatewayServer\CAP | Measure-Object).Count
if ($configured -eq 0)
{
	$localdomain = (Get-WmiObject Win32_ComputerSystem).Domain
	$group = "Domain Users@" + $localdomain
	New-Item -Name Default -Path RDS:\GatewayServer\CAP -UserGroups $group -AuthMethod 1
	New-Item -Name Default -Path RDS:\GatewayServer\RAP -UserGroups $group -ComputerGroupType 2
	Restart-Service TSGateway
}
'@
[IO.File]::WriteAllText("C:\Firmstep\tsgateway.ps1",$script)
C:\Windows\system32\schtasks.exe /create /sc ONSTART /tn TSGateway /tr "powershell.exe C:\Firmstep\tsgateway.ps1" /RU System /RL HIGHEST

# Create Build file in webroot folder 
New-Item "C:/inetpub/wwwroot/build.txt" -type File -force -value "Build: {name} {build}"

# Cleanup
Remove-Item "C:\Users\Default\Desktop\*.*"
Remove-Item "C:\TempDownloads" -Recurse -Force

Stop-Computer

</powershell>
