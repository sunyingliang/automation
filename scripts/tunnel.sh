#!/usr/bin/env bash

{snip_deb_sysctl}

# General commands
sudo add-apt-repository ppa:ondrej/php
apt update -y
apt install -y php7.2-cli php7.2-curl
apt install -y awscli socat
#apt upgrade -y

{snip_deb_security}

{snip_deb_timezone}

{snip_deb_swap}

# Create build file
printf "Build: {name} {build}" > /home/ubuntu/bin/build

# Install and configure monitoring agent
{snip_deb_monitoring}

curl -H 'Authorization: token {github}' -H 'Accept: application/vnd.github.v3.raw' -O -L https://api.github.com/repos/Firmstep/FS-Automation/contents/shared/tunnel-rebuild.php >> tunnel-rebuild.php
cat tunnel-rebuild.php | tr -d '\r' > tunnel_tmp && mv tunnel_tmp tunnel-rebuild.php

# Setup permissions
chown -R ubuntu:ubuntu /home/ubuntu/bin
chmod -R 770 /home/ubuntu/bin

{snip_deb_dropcache}

# Add rebuild script to rc.local to ensure it's the last thing completed at the of boot
sed -i -e '$i \nohup php /home/ubuntu/bin/tunnel-rebuild.php \"{endpoint}\" \"{token}\" &\n' /etc/rc.local

# Finally shut down the instance for AMI creation
sudo shutdown -h now
