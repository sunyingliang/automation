#!/usr/bin/env bash

{snip_deb_sysctl}

# General commands
sudo add-apt-repository ppa:ondrej/php
apt update -y
apt install -y apache2 php7.2-cli libapache2-mod-php7.2 php7.2-mysql php7.2-curl php7.2-soap php7.2-mbstring php7.2-xml
apt install -y awscli unzip
#apt upgrade -y

{snip_deb_security}

{snip_deb_timezone}

{snip_deb_swap}

# Prepare directories
rm -r /var/www/*

# Install Wordpress
cd /var/www
wget http://wordpress.org/latest.tar.gz
tar -xzvf latest.tar.gz
rm latest.tar.gz

# Copy Wordpress to webroot
cp -a /var/www/wordpress/. /var/www/{name}/
rm -r /var/www/wordpress

# Remove any auto-installed plugins
rm -r /var/www/{name}/wp-content/plugins/*

# Install SMTP plugin
wget https://downloads.wordpress.org/plugin/wp-mail-smtp.0.9.6.zip -O /tmp/wp-mail-smtp.zip
unzip -o -q /tmp/wp-mail-smtp.zip -d /var/www/{name}/wp-content/plugins/
rm /tmp/wp-mail-smtp.zip

# Install RRC Plugin,FAM plugin and Firmstep Theme from github
curl -s -H "Authorization: token {github}" -o /tmp/rrc.tar -L https://api.github.com/repos/Firmstep/FS-RRC-Clients/tarball/{rrcgitbranch}
tar xf /tmp/rrc.tar -C /tmp
rrcfolder=$( ls -1c /tmp | grep Firmstep-FS-RRC-Clients- | head -1)
mkdir /var/www/{name}/wp-content/plugins/RRCPlugin/
cp -R /tmp/$rrcfolder/PHP/WordPressPlugin/* /var/www/{name}/wp-content/plugins/RRCPlugin/
mkdir /var/www/{name}/wp-content/plugins/RRCPlugin/FirmstepRRC/
cp -R /tmp/$rrcfolder/PHP/FirmstepRRC/* /var/www/{name}/wp-content/plugins/RRCPlugin/FirmstepRRC/

curl -s -H "Authorization: token {github}" -o /tmp/fam.tar -L https://api.github.com/repos/Firmstep/FS-CMS-Modules/tarball/{famgitbranch}
tar xf /tmp/fam.tar -C /tmp
famfolder=$( ls -1c /tmp | grep Firmstep-FS-CMS-Modules- | head -1)
mkdir /var/www/{name}/wp-content/plugins/FirmstepFAM/
cp -R /tmp/$famfolder/FAMModuleWordpress/* /var/www/{name}/wp-content/plugins/FirmstepFAM/
mkdir -p /var/www/{name}/wp-content/themes/firmstep/
cp -R /tmp/$famfolder/FirmstepThemeWordpress/* /var/www/{name}/wp-content/themes/firmstep/

# Copy any required custom files
export AWS_ACCESS_KEY_ID={s3_key}
export AWS_SECRET_ACCESS_KEY={s3_secret}
export AWS_DEFAULT_REGION={s3_region}
aws s3 cp s3://{s3_bucket}/Configuration/{name}/{environment}/wp-config.php /var/www/{name}/wp-config.php
aws s3 cp s3://{s3_bucket}/Configuration/Certificates/{environment}/certificate.txt /etc/apache2/certificate.txt
aws s3 cp s3://{s3_bucket}/Configuration/Certificates/{environment}/key.txt /etc/apache2/key.txt
aws s3 cp s3://{s3_bucket}/Configuration/Certificates/{environment}/chain.txt /etc/apache2/chain.txt

# VirtualHost wordpress.conf
echo "<VirtualHost *:80>
    RewriteEngine On
    RewriteRule (.*) https://%{SERVER_NAME}$1 [R,L]
</VirtualHost>
<VirtualHost *:443>
    DocumentRoot /var/www/{name}
    ErrorLog /var/log/apache2/error.log
    CustomLog /var/log/apache2/access.log combined
    <Directory /var/www/{name}>
        Options +Includes -Indexes +FollowSymLinks +MultiViews
        AllowOverride All
        Require all granted
		RewriteEngine On
		RewriteBase /
		RewriteRule ^index\.php$ - [L]
		RewriteCond %{REQUEST_FILENAME} !-f
		RewriteCond %{REQUEST_FILENAME} !-d
		RewriteRule . /index.php [L]
    </Directory>
    SSLEngine on
    SSLCertificateFile /etc/apache2/certificate.txt
    SSLCertificateKeyFile /etc/apache2/key.txt
    SSLCertificateChainFile /etc/apache2/chain.txt
</VirtualHost>" > /etc/apache2/sites-available/{name}.conf

# Create build file
echo "Build: {name} {build}" > /var/www/{name}/build

{snip_deb_opcache}

{snip_deb_monitoring}

{snip_deb_permissions}

# Finalise apache deployment environment
a2dissite 000-default
a2ensite {name}
a2enmod rewrite
a2enmod ssl
rm -f /etc/apache2/sites-available/000-default.conf
rm -f /etc/apache2/sites-available/default-ssl.conf
service apache2 restart

{snip_deb_dropcache}

# Finally shut down the instance for AMI creation
sudo shutdown -h now
