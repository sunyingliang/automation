param ($environment)
function CheckStatus ($url, $site)
{
	$reply = ""
    Try{
        $reply = (New-Object System.Net.WebClient).DownloadString($url);
    }
    Catch {}
    if ($reply -Match "Service is alive")
    {
        if ($reply -NotMatch "AppStorage connection: <strong>Pass<\/strong>")
        {
            return "$site Appstorage connection failed`r`n"
        }
		return ""
    }
    return "$site is down`r`n"
}
function CheckURL ($url, $site)
{
	Try {
		$reply = (New-Object System.Net.WebClient).DownloadString($url);
	}
	Catch {
		return "$site is down`r`n"
	}
	return ""
}

$rootUrl = "http://localhost/"
$body = ""
$sites = Get-ChildItem C:\PaymentConnectors -Directory
foreach ($site in $sites) {
    if (("paymentsimulator","reportwebservice","parismotomock","Scripts") -Contains "$site")
    {
        continue
    }
    $url = "$rootUrl$site/ServerStatus.aspx"
	$body += CheckStatus $url $site
	sleep -s 20
}
$body += CheckStatus ($rootUrl+"connector/serverStatus.php") "PHP Payment Connector"
$body += CheckURL ($rootUrl+"RRCService/RRCService.asmx") "RRC Service"
$body += CheckURL ($rootUrl+"FillTask/FillTask.asmx") "FillTask"
$body += CheckURL ($rootUrl+"AchieveService/Service.asmx") "AchieveService"
$body += CheckURL ($rootUrl+"FirmstepTaskHandler/TestLookupFillTaskPage.html") "FirmstepTaskHandler"
$body += CheckURL ($rootUrl+"FS.Notify/Notify.asmx") "FS.Notify"
$body += CheckURL ($rootUrl+"ical/ical.aspx") "ical"
$body += CheckURL ($rootUrl+"PD3ThreadSplitter/DummyAPI.aspx") "PD3ThreadSplitter"
$body += CheckURL ($rootUrl+"SimpleReturn/post.aspx") "SimpleReturn"
$body += CheckURL ($rootUrl+"StringEncrypter/StringEncrypter.svc") "StringEncrypter"
$body += CheckURL ($rootUrl+"WSSEWebservice/WSSEWebservice.svc") "WSSEWebservice"
if ($body)
{
Send-MailMessage -From "components@firmstep.com" -To "auckland-team@firmstep.com" -Subject "Components Error [$environment]" -SmtpServer "smtp.firmstep.com" -Body $body
}