Import-Module WebAdministration

#Checks for D:\FAM, if false then moves C:\FAM .asp files to a new D:\FAM folder
if (-Not (Test-Path "D:\FAM")) {
	New-Item -ItemType directory -Path D:\FAM
	Copy-Item -Path C:\FAM\* -Filter *.asp -Destination D:\FAM -Recurse
}

if (-Not (Test-Path "IIS:\Sites\Default Web Site\FAM")) {
	#Sets FAM root virtual directory to the D:\FAM folder
	New-Item "IIS:\Sites\Default Web Site\FAM" -type Application -physicalPath D:\FAM
	
	#Creates FAM applicationPool from D:\FAM .asp files
	Set-ItemProperty "IIS:\Sites\Default Web Site\FAM" -Name applicationPool -Value FAM
}
