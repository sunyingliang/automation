$scriptpath = $MyInvocation.MyCommand.Path
$dir = Split-Path $scriptpath

$connectionNode = Select-Xml -Path ($dir + '/agent.config') -XPath '/configuration/fillTaskAgents/add[1]/@itemConnectionString'
$connectionstring = $connectionNode.Node.Value
$connection = New-Object System.Data.SQLClient.SQLConnection($connectionstring)
$connection.Open()

$command = $connection.CreateCommand()
$command.CommandText = 'SELECT CaseID FROM FillTask WHERE Completed = 1'
$dataReader = $command.ExecuteReader()
while ($dataReader.Read()) {
    $reference = $dataReader['CaseID']
    $url = 'https://inttest-forms.devops.firmstep.com/api/cdb/process/delete/case/json?process_id=AF-Process-855e5c6c-ec64-4e02-9b5a-a988f4fcec41&key=9ab1ef54-a244-fb4b-e3a3-4e68b6db0b79&pass=c39cd825-66f0-1de5-6041-192c9c1ce109&case_id=' + $reference
	Write-Host $url
    Invoke-WebRequest -Uri $url
}
$dataReader.Close()

$command = $connection.CreateCommand()
$command.CommandText = 'DELETE FROM FillTask WHERE Completed = 1'
$command.ExecuteNonQuery()

$command = $connection.CreateCommand()
$command.CommandText = 'DELETE FROM StartThread WHERE Completed = 1'
$command.ExecuteNonQuery()

$connection.Close()