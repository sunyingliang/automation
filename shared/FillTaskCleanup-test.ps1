$scriptpath = $MyInvocation.MyCommand.Path
$dir = Split-Path $scriptpath

$connectionNode = Select-Xml -Path ($dir + '/agent.config') -XPath '/configuration/fillTaskAgents/add[1]/@itemConnectionString'
$connectionstring = $connectionNode.Node.Value
$connection = New-Object System.Data.SQLClient.SQLConnection($connectionstring)
$connection.Open()

$command = $connection.CreateCommand()
$command.CommandText = 'SELECT TaskID FROM Items WHERE Completed = 1 AND Version = 2 AND TaskID IS NOT NULL'
$dataReader = $command.ExecuteReader()
while ($dataReader.Read()) {
    $reference = $dataReader['TaskID']
    $url = 'https://aucklandteam-forms.achieveservice.com/api/cdb/process/delete/case/json?process_id=AF-Process-3cb83bc2-9912-4d2f-93b9-2732ad42b324&key=aa125ab9-75cd-253e-b205-a9bc8c081f1a&pass=6c691805-2665-b226-eb4d-5356d1a46a6d&case_id=' + $reference
	Write-Host $url
    Invoke-WebRequest -Uri $url
}
$dataReader.Close()

$command = $connection.CreateCommand()
$command.CommandText = 'SELECT Field FROM Items WHERE Completed = 1 AND Version = 1 AND TaskID IS NOT NULL'
$dataReader = $command.ExecuteReader()
while ($dataReader.Read()) {
    $reference = $dataReader['Field']
    $reference = $reference -replace 'Stage 2 ',''
    $url = 'https://forms.firmsteptest.com/default.aspx/API/DeleteSubmission/?Key=3591626F-D76F-44E4-87A6-3AF7B38EA8DB&Reference=' + $reference
	Write-Host $url
	Invoke-WebRequest -Uri $url
}
$dataReader.Close()

$command = $connection.CreateCommand()
$command.CommandText = 'DELETE FROM Items WHERE Completed = 1'
$command.ExecuteNonQuery()

$connection.Close()