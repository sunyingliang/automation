$scriptpath = $MyInvocation.MyCommand.Path
$dir = Split-Path $scriptpath

$connectionNode = Select-Xml -Path ($dir + '/agent.config') -XPath '/configuration/fillTaskAgents/add[1]/@itemConnectionString'
$connectionstring = $connectionNode.Node.Value
$connection = New-Object System.Data.SQLClient.SQLConnection($connectionstring)
$connection.Open()

$command = $connection.CreateCommand()
$command.CommandText = "INSERT INTO StartThread (ProcessID, Completed, UserEmail, field1, field2) VALUES ('AF-Process-855e5c6c-ec64-4e02-9b5a-a988f4fcec41', 0, '720734743', CONVERT(varchar(30), GetDate()), CONVERT(varchar(45), NewID()))"
$command.ExecuteNonQuery()

$connection.Close()