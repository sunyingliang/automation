$scriptpath = $MyInvocation.MyCommand.Path
$dir = Split-Path $scriptpath

$connectionNode = Select-Xml -Path ($dir + '/agent.config') -XPath '/configuration/fillTaskAgents/add[1]/@itemConnectionString'
$connectionstring = $connectionNode.Node.Value
$connection = New-Object System.Data.SQLClient.SQLConnection($connectionstring)
$connection.Open()

$command = $connection.CreateCommand()
$command.CommandText = "INSERT INTO Items (TaskID, Completed, Version, Field, EmailAddress) VALUES (NULL, 0, 1, CONVERT(varchar(30), GetDate()), 'david.simon@firmstep.com')"
$command.ExecuteNonQuery()

$command = $connection.CreateCommand()
$command.CommandText = "INSERT INTO Items (TaskID, Completed, Version, Field, EmailAddress) VALUES (NULL, 0, 2, CONVERT(varchar(30), GetDate()), '776241902')"
$command.ExecuteNonQuery()

$connection.Close()