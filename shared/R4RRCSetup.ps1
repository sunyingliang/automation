#custom test pages for root applications
$page = Get-Content -Raw C:\inetpub\wwwroot\render.aspx
$page = $page.Replace("//PlatformRenderControl1.InitialFormValues", "PlatformRenderControl1.InitialFormValues")
$page = $page.Replace("MyField", "field1")
$page = $page.Replace("MyField2", "field2")
$page = $page.Replace("//PlatformRenderControl1.FormSaved", "PlatformRenderControl1.FormSaved")
$page = $page.Replace("CurrentUrl = (string)Session[`"SelectedPageUrl`"]", "CurrentUrl = `"/R4/RenderForm/?form=AF-Form-196a376c-20ca-46c8-a7b6-4d47605c394d`"")
[IO.File]::WriteAllText("C:\inetpub\wwwroot\Form.aspx",$page)

$page = Get-Content -Raw C:\inetpub\wwwroot\render.aspx
$page = $page.Replace("//PlatformRenderControl1.FormSaved", "PlatformRenderControl1.FormSaved")
$page = $page.Replace("CurrentUrl = (string)Session[`"SelectedPageUrl`"]", "CurrentUrl = `"/R4/RenderForm/?form=AF-Form-35bf81c5-6519-4382-a968-c7ed3ce51166`"")
[IO.File]::WriteAllText("C:\inetpub\wwwroot\Payment.aspx",$page)

$page = Get-Content -Raw C:\inetpub\wwwroot\render.aspx
$page = $page.Replace("//PlatformRenderControl1.FormSaved", "PlatformRenderControl1.FormSaved")
$page = $page.Replace("CurrentUrl = (string)Session[`"SelectedPageUrl`"]", "CurrentUrl = `"/R4/RenderForm/?form=AF-Form-606a915c-4fe1-4062-ac7e-a7fa3fb7e6e2`"")
[IO.File]::WriteAllText("C:\inetpub\wwwroot\NewPayment.aspx",$page)

$page = Get-Content -Raw C:\inetpub\wwwroot\render.aspx
$page = $page.Replace("//PlatformRenderControl1.InitialFormValues", "PlatformRenderControl1.InitialFormValues")
$page = $page.Replace("MyField", "field1")
$page = $page.Replace("MyField2", "field2")
$page = $page.Replace("//PlatformRenderControl1.FormSaved", "PlatformRenderControl1.FormSaved")
$page = $page.Replace("CurrentUrl = (string)Session[`"SelectedPageUrl`"]", "CurrentUrl = `"/R4/Render/?process=AF-Process-1d18ae7e-fee4-4830-bf3b-808761d095de`"")
[IO.File]::WriteAllText("C:\inetpub\wwwroot\Process.aspx",$page)

#OAF test form via passthrough
$page = Get-Content -Raw C:\inetpub\wwwroot\render.aspx
$page = $page.Replace("//PlatformRenderControl1.FormSubmitted", "PlatformRenderControl1.FormSubmitted")
$page = $page.Replace("CurrentUrl = (string)Session[`"SelectedPageUrl`"]", "CurrentUrl = `"/RenderForm/?F.Name=eByjG48Dcn2`"")
[IO.File]::WriteAllText("C:\inetpub\wwwroot\OAFPayment.aspx",$page)

#OAF test form direct
$page = Get-Content -Raw C:\inetpub\wwwroot\render.aspx
$page = $page.Replace("//PlatformRenderControl1.FormSubmitted", "PlatformRenderControl1.FormSubmitted")
$page = $page.Replace("CurrentUrl = (string)Session[`"SelectedPageUrl`"]", "CurrentUrl = `"/RenderForm/?F.Name=eByjG48Dcn2`"")
[IO.File]::WriteAllText("C:\inetpub\wwwroot\OAF\Payment.aspx",$page)

#create new home page
$homePage = @"
<html>
<head><title>R4RRC home page</title></head>
<body>
Main test site:<br/>
<a href="/default.aspx">Auckland-Team Forms</a><br/>
<a href="/Form.aspx">Test Form</a><br/>
<a href="/Process.aspx">Test Process</a><br/>
<a href="/Payment.aspx">Payment Test Form</a><br/>
<a href="/NewPayment.aspx">New Payment Test Form</a><br/>
<a href="/OAFPayment.aspx">OAF Payment Test Form (passthrough)</a><br/>
<br/>
Other test sites:<br/>
<a href="/IntTest/default.aspx">IntTest</a><br/>
<a href="/NewForms/default.aspx">NewForms (firmsteptest.com)</a><br/>
<a href="/NET4/default.aspx">Auckland-Team Forms .NET4</a><br/>
<a href="/OAF/default.aspx">OAF (firmsteptest.com)</a><br/>
<a href="/OAF/Payment.aspx">OAF Payment Test Form (direct)</a><br/>
<br/>
Other clients:<br/>
<a href="/ASP/default.asp">Classic ASP</a><br/>
<a href="/PHP/index.php">PHP</a><br/>
</body>
</html>
"@
[IO.File]::WriteAllText("C:\inetpub\wwwroot\index.html",$homePage)

#config for root application
$config = (Get-Content "C:\inetpub\wwwroot\web.config") -as [Xml]
($config.configuration.appSettings.add | Where-Object { $_.Key -eq "FirmstepSiteUrl"}).Value = "aucklandteam-forms.achieveservice.com"
($config.configuration.appSettings.add | Where-Object { $_.Key -eq "FirmstepWebServiceUrl"}).Value = "https://components.firmsteptest.com/RRCService/RRCService.asmx"
($config.configuration.appSettings.add | Where-Object { $_.Key -eq "FirmstepCustomerKey"}).Value = "6ec1fcf0-5c25-4d91-3272-bc0b55391677"
($config.configuration.appSettings.add | Where-Object { $_.Key -eq "FirmstepExternalReferenceUrl"}).Value = "/popup.aspx"
$config.Save("C:\inetpub\wwwroot\web.config")

#config for IntTest application
$config = (Get-Content "C:\inetpub\wwwroot\IntTest\web.config") -as [Xml]
($config.configuration.appSettings.add | Where-Object { $_.Key -eq "FirmstepSiteUrl"}).Value = "inttest-forms.devops.firmstep.com"
($config.configuration.appSettings.add | Where-Object { $_.Key -eq "FirmstepWebServiceUrl"}).Value = "https://components.firmsteptest.com/RRCService/RRCService.asmx"
($config.configuration.appSettings.add | Where-Object { $_.Key -eq "FirmstepCustomerKey"}).Value = "XYZ"
($config.configuration.appSettings.add | Where-Object { $_.Key -eq "FirmstepExternalReferenceUrl"}).Value = "/IntTest/popup.aspx"
$config.Save("C:\inetpub\wwwroot\IntTest\web.config")

#config for NewForms application
$config = (Get-Content "C:\inetpub\wwwroot\NewForms\web.config") -as [Xml]
($config.configuration.appSettings.add | Where-Object { $_.Key -eq "FirmstepSiteUrl"}).Value = "newforms.firmsteptest.com"
($config.configuration.appSettings.add | Where-Object { $_.Key -eq "FirmstepWebServiceUrl"}).Value = "https://components.firmsteptest.com/RRCService/RRCService.asmx"
($config.configuration.appSettings.add | Where-Object { $_.Key -eq "FirmstepCustomerKey"}).Value = "FIRMSTEPTEST"
($config.configuration.appSettings.add | Where-Object { $_.Key -eq "FirmstepExternalReferenceUrl"}).Value = "/NewForms/popup.aspx"
$config.Save("C:\inetpub\wwwroot\NewForms\web.config")

#config for NET4 application
$config = (Get-Content "C:\inetpub\wwwroot\NET4\web.config") -as [Xml]
($config.configuration.appSettings.add | Where-Object { $_.Key -eq "FirmstepSiteUrl"}).Value = "aucklandteam-forms.achieveservice.com"
($config.configuration.appSettings.add | Where-Object { $_.Key -eq "FirmstepWebServiceUrl"}).Value = "https://components.firmsteptest.com/RRCService/RRCService.asmx"
($config.configuration.appSettings.add | Where-Object { $_.Key -eq "FirmstepCustomerKey"}).Value = "6ec1fcf0-5c25-4d91-3272-bc0b55391677"
($config.configuration.appSettings.add | Where-Object { $_.Key -eq "FirmstepExternalReferenceUrl"}).Value = "/NET4/popup.aspx"
$config.Save("C:\inetpub\wwwroot\NET4\web.config")

#config for OAF application
$config = (Get-Content "C:\inetpub\wwwroot\OAF\web.config") -as [Xml]
($config.configuration.appSettings.add | Where-Object { $_.Key -eq "FirmstepSiteUrl"}).Value = "forms.firmsteptest.com"
($config.configuration.appSettings.add | Where-Object { $_.Key -eq "FirmstepWebServiceUrl"}).Value = "https://forms.firmsteptest.com/API/RRCService.asmx"
($config.configuration.appSettings.add | Where-Object { $_.Key -eq "FirmstepCustomerKey"}).Value = "XXX"
($config.configuration.appSettings.add | Where-Object { $_.Key -eq "FirmstepExternalReferenceUrl"}).Value = "/OAF/popup.aspx"
$config.Save("C:\inetpub\wwwroot\OAF\web.config")

#create settings files
$aspSettings = @'
<%
RRC.SiteUrl = "aucklandteam-forms.achieveservice.com"
RRC.WebServiceUrl = "https://components.firmsteptest.com/RRCService/RRCService.asmx"
RRC.CustomerKey = "6ec1fcf0-5c25-4d91-3272-bc0b55391677"
RRC.ExternalReferenceUrl = "/ASP/popup.asp?FS="
RRC.AuthenticationType = "none"
'RRC.AuthenticatedUser = ""
'RRC.StylesheetMode = "full"
'RRC.ExtraStylesheet = ""
'RRC.ProxyURL = "http://rr4debug.firmsteptest.com:8888"
%>
'@
[IO.File]::WriteAllText("C:\inetpub\wwwroot\ASP\settings.asp",$aspSettings)

$phpSettings = @'
<?php
$rrc->SiteUrl = "aucklandteam-forms.achieveservice.com";
$rrc->WebServiceUrl = "https://components.firmsteptest.com/RRCService/RRCService.asmx";
$rrc->CustomerKey = "6ec1fcf0-5c25-4d91-3272-bc0b55391677";
$rrc->ExternalReferenceUrl = "/PHP/popup.php";
$rrc->AuthenticationType = "none";
//$rrc->AuthenticatedUser = "";
//$rrc->ProxyUrl = "http://rr4debug.firmsteptest.com:8888";
'@
[IO.File]::WriteAllText("C:\inetpub\wwwroot\PHP\settings.php",$phpSettings)