Param(
[string]$name,
[string]$environment,
[string]$config,
[string]$scriptPath
)

[xml]$xml = Get-Content -Path $config
$resemblanceUrl = (Select-Xml -Xml $xml -XPath "/config/resemblance/text()").Node.Value
$token = (Select-Xml -Xml $xml -XPath "/config/resemblanceToken/text()").Node.Value
$queueUrl = (Select-Xml -Xml $xml -XPath "/config/queueUrl/text()").Node.Value

$body =@{
queueUrl = $queueUrl
token = $token
}
$message = Invoke-RestMethod -Method Post -Uri "$resemblanceUrl/api/sqs/list" -Body (ConvertTo-Json $body) -ContentType 'application/json'
If ($message.response.data.Messages) {
	$body = $message.response.data.Messages[0].Body
	$json = ConvertFrom-Json $body
	$instance = $json.EC2InstanceId
	If (-Not $instance) {
		$body = @{
		queueUrl = $queueUrl
		token = $token
		receiptHandle = $message.response.data.Messages[0].ReceiptHandle
		}
		Invoke-RestMethod -Method Post -Uri "$resemblanceUrl/api/sqs/delete" -Body (ConvertTo-Json $body) -ContentType 'application/json'
	}
	$instanceID = Invoke-RestMethod http://169.254.169.254/latest/meta-data/instance-id -TimeoutSec 10
	#keep retrying until network comes up
	While ($instanceID.Length -eq 0)
	{
		Start-Sleep -s 30
		$instanceID = Invoke-RestMethod http://169.254.169.254/latest/meta-data/instance-id -TimeoutSec 10
	}
	If ($instanceID -Eq $instance) {
		powershell.exe "$scriptPath -name $name -environment $environment -config $config -stop"
		$body = @{
		queueUrl = $queueUrl
		token = $token
		receiptHandle = $message.response.data.Messages[0].ReceiptHandle
		}
		Invoke-RestMethod -Method Post -Uri "$resemblanceUrl/api/sqs/delete" -Body (ConvertTo-Json $body) -ContentType 'application/json'
		Stop-Computer -Force
	}
}