Param(
[string] $scriptPath,
[string] $scriptParameter
)

Move-Item -Path $scriptPath -Destination "C:\WINDOWS\System32\GroupPolicy\Machine\Scripts\Shutdown\shutdown.ps1"

$shutdownReg = @"
Windows Registry Editor Version 5.00
[HKEY_LOCAL_MACHINE\SOFTWARE\Microsoft\Windows\CurrentVersion\Group Policy\Scripts\Shutdown\0]
"GPO-ID"="LocalGPO"
"SOM-ID"="Local"
"FileSysPath"="C:\\Windows\\System32\\GroupPolicy\\Machine"
"DisplayName"="Local Group Policy"
"GPOName"="Local Group Policy"
"PSScriptOrder"=dword:00000001
[HKEY_LOCAL_MACHINE\SOFTWARE\Microsoft\Windows\CurrentVersion\Group Policy\Scripts\Shutdown\0\0]
"Script"="shutdown.ps1"
"Parameters"="$scriptParameter"
"IsPowershell"=dword:00000001
"ExecTime"=hex(b):00,00,00,00,00,00,00,00,00,00,00,00,00,00,00,00
[HKEY_LOCAL_MACHINE\SOFTWARE\Microsoft\Windows\CurrentVersion\Group Policy\State\Machine\Scripts\Shutdown\0]
"GPO-ID"="LocalGPO"
"SOM-ID"="Local"
"FileSysPath"="C:\\Windows\\System32\\GroupPolicy\\Machine"
"DisplayName"="Local Group Policy"
"GPOName"="Local Group Policy"
"PSScriptOrder"=dword:00000001
[HKEY_LOCAL_MACHINE\SOFTWARE\Microsoft\Windows\CurrentVersion\Group Policy\State\Machine\Scripts\Shutdown\0\0]
"Script"="shutdown.ps1"
"Parameters"="$scriptParameter"
"ExecTime"=hex(b):e1,07,05,00,01,00,08,00,01,00,03,00,24,00,86,03
"ErrorCode"=dword:00000000
"@
[IO.File]::WriteAllText("C:\shutdown.reg",$shutdownReg)
REG IMPORT C:\shutdown.reg | Out-Null
#create shutdown ini file
$shutdownIni = @"
[Shutdown]
0CmdLine=shutdown.ps1
0Parameters=$scriptParameter
"@.Replace("`r","`r`n")
[IO.File]::WriteAllText("C:\TempDownloads\psscripts.ini",$shutdownIni)
New-Item -ItemType Directory -Force -Path C:\Windows\System32\GroupPolicy\Machine\Scripts\
New-Item -ItemType Directory -Force -Path C:\Windows\System32\GroupPolicy\Machine\Scripts\Startup
New-Item -ItemType Directory -Force -Path C:\Windows\System32\GroupPolicy\Machine\Scripts\Shutdown
New-Item -ItemType Directory -Force -Path C:\Windows\System32\GroupPolicy\User
Copy-Item -force -path C:\TempDownloads\psscripts.ini -destination C:\Windows\System32\GroupPolicy\Machine\Scripts\psscripts.ini
gpupdate /force