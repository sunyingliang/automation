Param(
[string]$name,
[string]$environment,
[string]$config,
[switch]$stop
)

Function UploadIISLog {
	Param(
	[string]$name,
	[string]$environment,
	[string]$logDirectory,
	[string]$logEndpoint,
	[string]$token,
	[string]$dateFormat = "yyMMddHH",
	[bool]$stop
	)

	$currentLog = "*" + (Get-Date).ToUniversalTime().ToString($dateFormat) + ".log"
	$filePaths = Get-ChildItem $logDirectory -Filter *.log
	If ($stop) {
		Invoke-Command -scriptblock {iisreset /STOP}
	}
	If (!$filePaths) {Break}

	Foreach ($filePath in $filePaths) {
		If (-Not $stop -And "$filePath" -Like $currentLog) {Continue}
		$content = Get-Content -Path "$logDirectory/$filePath"
		$headers = $NULL
		$json = '{"requests":['
		Foreach ($log in $content) {
			If ($log.StartsWith("#Fields:")) {
				$log = $log.Substring(9)
				$headers = $log.Split(' ')
			} ElseIf ($log.StartsWith("#")) {
			} Else {
				$logItems = $log.Split(' ')
				$logJson = '{'
				$date = ''
				$continue = $False
				For ($i=0; $i -le $logItems.Length; $i++) {
					$headerName = $headers[$i]
					$value = $logItems[$i]
					$skip = $False
					switch ($headerName)
					{
						"date" {
							$date = $value
							$skip = $True
							break
						}
						"time" {
							$headerName = "logTime"
							$value = "$date $value"
							break
						}
						"c-ip" {$headerName = "clientHost"; break}
						"cs-uri-stem" {
							If ($value -Eq "/" -Or $value -Eq "/iisstart.htm" -Or $value -Eq "/build.txt") {$continue = $True}
							$headerName = "target"
							break
						}
						"s-sitename" {$headerName = "service"; break}
						"s-computername" {$headerName = "machine"; break}
						"s-ip" {$headerName = "serverIp"; break}
						"cs-username" {$headerName = "userName"; break}
						"time-taken" {$headerName = "processingTime"; break}
						"cs-bytes" {$headerName = "bytesRecvd"; break}
						"sc-bytes" {$headerName = "bytesSent"; break}
						"sc-status" {$headerName = "serviceStatus"; break}
						"sc-win32-status" {$headerName = "win32Status"; break}
						"cs-method" {$headerName = "operation"; break}
						"cs-uri-query" {$headerName = "parameters"; break}
						default {$skip = $True}
					}
					If ($continue -Or $skip) {continue}
					$logJson += """$headerName"":""$value"","
				}
				If ($continue) {continue}
				$logJson = $logJson.Trim(',')
				$logJson += '},'
				$json += $logJson
			}
		}
		If ($json -Ne '{"requests":[') {
			$json = $json.Trim(',')
			$json += '],"token":"' + $token + '"}'
			Write-Host $json
			$retry = 0
			While ($true) {
				$client = New-Object System.Net.WebClient
				Try {
					$client.Headers.Add("Content-Type","application/json")
					$client.UploadString($logEndpoint, $json)
					Rename-Item -Path "$logDirectory/$filePath" -NewName "$filePath.processed"
					return
				} Catch [System.Net.WebException] {
					$retry++
					If ($retry -gt 3) {
						$errorMessage = $_.Exception.Status + "`r`n" + $_.Exception.ToString()
						If($_.Exception.Response) {
							$responseStream = $_.Exception.Response.GetResponseStream()
							$errorMessage = $errorMessage + "`r`n" + (New-Object System.IO.StreamReader($responseStream)).ReadToEnd()
						}
						Send-MailMessage -From "$name@firmstep.com" -To "auckland-team@firmstep.com" -Subject "IIS Log upload Error [$environment]" -SmtpServer "smtp.firmstep.com" -Body "$errorMessage"
						return
					}
				} Finally {
					$client.Dispose()
				}
			}
		}
	}
}
Function UploadLog {
	Param(
	[string]$name,
	[string]$environment,
	[string]$logFile,
	[string]$logEndpoint,
	[string]$token
	)

	$content = Get-Content -Path "$logFile" | Out-String
	If ($content) {
		$content = $content.Trim().Trim(',')
		$json = '{"requests":[' + $content + '],"token":"' + $token + '"}'
		$retry = 0
		While ($true) {
			$client = New-Object System.Net.WebClient
			Try {
				$client.Headers.Add("Content-Type","application/json")
				$client.UploadString($logEndpoint, $json)
				$date = (Get-Date).ToUniversalTime().ToString("yyMMddHH")
				Rename-Item -Path "$logFile" -NewName "$logFile.$date.processed"
				return
			} Catch [System.Net.WebException] {
				$retry++
				If ($retry -gt 3) {
					$errorMessage = "log file:$logFile, log endpoint:$logEndpoint`r`n" $_.Exception.Status + "`r`n" + $_.Exception.ToString()
					If($_.Exception.Response) {
						$responseStream = $_.Exception.Response.GetResponseStream()
						$errorMessage = $errorMessage + "`r`n" + (New-Object System.IO.StreamReader($responseStream)).ReadToEnd()
					}
					Send-MailMessage -From "$name@firmstep.com" -To "auckland-team@firmstep.com" -Subject "Log upload Error [$environment]" -SmtpServer "smtp.firmstep.com" -Body "$errorMessage"
					return
				}
			} Finally {
				$client.Dispose()
			}
		}
	}
}

[xml]$xml = Get-Content -Path $config
$token = (Select-Xml -Xml $xml -XPath "/config/infrastructureToken/text()").Node.Value
$iisDirectory = (Select-Xml -Xml $xml -XPath "/config/iis/@directory").Node.Value
$iisEndpoint = (Select-Xml -Xml $xml -XPath "/config/iis/@endpoint").Node.Value
$iisDateFormat = (Select-Xml -Xml $xml -XPath "/config/iis/@dateFormat").Node.Value

UploadIISLog -name $name -environment $environment -logDirectory $iisDirectory -logEndpoint $iisEndpoint -token $token -stop $stop

$logs = Select-Xml -Xml $xml -XPath "/config/logs/log" | Select-Object -ExpandProperty Node
Foreach($log in $logs) {
	UploadLog -name $name -environment $environment -logFile $log.file -logEndpoint $log.endpoint -token $token
}