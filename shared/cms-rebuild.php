<?php

// Info: This script is designed to connect to the CMS-Admin server and collect the VirtualHost & Rebuild script that this particular server needs.

/*
PARAMETERS
  1) CMS URL
  2) CMS Token (Security token)
  3) Environment (Dev / Staging / Live)
*/

// Require CLI
if (php_sapi_name() != 'cli') die('Error: This script can only be run from command line only, exiting...' . PHP_EOL);

// Check whether $argv is registered
if ((int)ini_get('register_argc_argv') != 1) die('Error: register_argc_argv is not on, please check configuration in php.ini' . PHP_EOL);

// Check required parameters
if (!isset($argv) || empty($argv[1]) || empty($argv[2]) || empty($argv[3])) die('Error: Missing configuration parameter, exiting...' . PHP_EOL);

// Initialise CMSRebuild class & ready self for good times!
$rebuild = new CMSRebuild($argv);

// Process CMS Admin Virtual Hosts
$rebuild->processVirtualHosts();

// Process CMS Admin Git Repositories
$rebuild->processWebDirectory();

exit(PHP_EOL . 'Script complete.' . PHP_EOL);

// Class definitions
class CMSRebuild
{
    private $cooldown         = 0;
    private $timeout          = 0;
    private $CMSURL           = '';
    private $CMSToken         = '';
    private $environment      = '';
    private $configLocation   = '';
    private $webRootLocation  = '';
    private $errorLogLocation = '';
    private $scriptLocation   = '';
    private $CURLResponse     = '';
    private $CURLStatusCode   = '';

    public function __construct($argv)
    {
        $this->cooldown         = 30;
        $this->timeout          = ceil(300 / $this->cooldown);
        $this->CMSURL           = $argv[1];
        $this->CMSToken         = $argv[2];
        $this->environment      = $argv[3];
        $this->configLocation   = '/etc/apache2/sites-available/cms.conf';
        $this->webRootLocation  = '/var/www/';
        $this->errorLogLocation = '/home/ubuntu/bin/error.log';
        $this->scriptLocation   = '/home/ubuntu/bin/script.sh';
    }

    public function processVirtualHosts()
    {
        while (true) {
            // Set CURL Data
            $data = [
                'token'       => $this->CMSToken,
                'environment' => $this->environment
            ];

            $this->doMessage('Connecting and fetching VirtualHosts from CMS-Admin');

            // Do CURL Post to get VirtualHosts
            $this->doCURL('/cms/config', $data);

            // If offline do not continue
            if ($this->CURLStatusCode == 200 && $this->validate()) {
                if ($this->CURLResponse['response']['status'] == 'error') {
                    $this->doMessage($this->CURLResponse['response']['message']);
                } else if (!empty($this->CURLResponse['response']['data'])) {
                    $this->doMessage('Successfully received VirtualHosts from CMS-Admin');

                    // Do we need to replace the current VirtualHost config?
                    if (file_exists($this->configLocation)) {
                        if ($this->CURLResponse['response']['data'] == file_get_contents($this->configLocation)) {
                            $this->doMessage('VirtualHosts from CMS-Admin do not require an update');
                            return true;
                        }
                    }

                    $this->doMessage('VirtualHosts need an update');

                    // Initialise VirtualHost file
                    $handler = fopen($this->configLocation, 'w') or $this->doMessage(PHP_EOL . 'Unable to open ' . $this->configLocation . ' for writing.' . PHP_EOL, 0, true);

                    // Write VirtualHost file
                    fwrite($handler, $this->CURLResponse['response']['data']);
                    fclose($handler);

                    // Enable the VirtualHost
                    $this->doMessage('Adding VirtualHost to sites-available [1/2]');
                    exec('sudo a2ensite cms');

                    // Reload ApacheConfiguration
                    $this->doMessage('Reloading Apache2 configuration [2/2]');
                    exec('sudo service apache2 reload');

                    return true;
                }
            } 

            $this->timeout--;

            // Check timeout hasn't expired
            if ($this->timeout <= 0) $this->doMessage('Timed out fetching VirtualHosts from CMS-Admin', true);

            // Wait for the cooldown period before continuing
            sleep($this->cooldown);
        }
    }

    public function processWebDirectory()
    {
        while (true) {
            // Set CURL Data
            $data = [
                'token'       => $this->CMSToken,
                'environment' => $this->environment
            ];

            $this->doMessage('Connecting and fetching rebuild script from CMS-Admin');

            // Do CURL Post to get rebuild script
            $this->doCURL('/cms/rebuild', $data);

            // If offline do not continue
            if ($this->CURLStatusCode == 200 && $this->validate()) {
                if ($this->CURLResponse['response']['status'] == 'error') {
                    $this->doMessage($this->CURLResponse['response']['message']);
                } else if (!empty($this->CURLResponse['response']['data'])) {
                    $this->doMessage('Saving rebuild script');

                    // Save script to disk
                    $handler = fopen($this->scriptLocation, 'w') or die('Unable to open ' . $this->scriptLocation . ' for writing.' . PHP_EOL);

                    fwrite($handler, $this->CURLResponse['response']['data']);
                    fclose($handler);

                    // Grant execute permissions to script
                    shell_exec('chmod +x ' . $this->scriptLocation);

                    $this->doMessage('Running rebuild script');

                    // Run the remote command
                    $this->doMessage(shell_exec($this->scriptLocation));

                    $this->doMessage('Deleting rebuild script');

                    unlink($this->scriptLocation);

                    return true;
                }
            }

            $this->timeout--;

            // Check timeout hasn't expired
            if ($this->timeout <= 0) $this->doMessage('Timed out fetching Rebuild script from CMS-Admin.', true);

            $this->doMessage('Timed out connecting to CMS-Admin, retrying');

            // Wait for the cooldown period before continuing
            sleep($this->cooldown);
        }
    }

    private function doCURL($endpoint, $data)
    {
        // Init CURL Request
        $ch = curl_init($this->CMSURL . $endpoint);

        // Set CURL Options
        curl_setopt($ch, CURLOPT_POST, 1);
        curl_setopt($ch, CURLOPT_POSTFIELDS, http_build_query($data));
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_FOLLOWLOCATION, 1);
        curl_setopt($ch, CURLOPT_TIMEOUT, 10);
        curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, 10);

        // Get CURL result
        $curlResponse = curl_exec($ch);

        $this->CURLStatusCode = curl_getinfo($ch, CURLINFO_HTTP_CODE);

        // Close CURL request
        curl_close($ch);

        if ($curlResponse === false) {
            $this->doMessage('Failed to call curl function');
        }

        $this->CURLResponse = json_decode($curlResponse, true);

        if (json_last_error() !== JSON_ERROR_NONE) {
            $this->doMessage('Failed to parse response from ' . $this->CMSURL);
        }
    }

    private function doMessage($message, $die = false)
    {
        if ($die) {
            $handler = fopen($this->errorLogLocation, 'w') or die('Unable to open ' . $this->errorLogLocation . ' for writing.' . PHP_EOL);

            fwrite($handler, $message);
            fclose($handler);

            die($message . PHP_EOL);
        }

        echo $message . '...' . PHP_EOL;
        sleep(1);
    }

    private function validate() {

        if (!isset($this->CURLResponse['response'])) {
            $this->doMessage('Failed to get response object');
            return false;

        } else if (!isset($this->CURLResponse['response']['status'])) {
            $this->doMessage('Failed to get response status object');
            return false;

        } else if (!isset($this->CURLResponse['response']['data'])) {
            $this->doMessage('Failed to get response data object');
            return false;
        }

        return true;
    }
}
?>
