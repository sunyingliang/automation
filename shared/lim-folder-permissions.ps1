Import-Module NTFSSecurity

if (-Not (Test-Path "D:\Uploads")) { New-Item -ItemType directory -Path "D:\Uploads" | Out-Null }
if (-Not (Test-Path "D:\LIMLogs")) { New-Item -ItemType directory -Path "D:\LIMLogs" | Out-Null }

Add-NTFSAccess -Path "D:\Uploads" -Account Everyone -AccessRights FullControl
Add-NTFSAccess -Path "D:\LIMLogs" -Account Everyone -AccessRights FullControl
