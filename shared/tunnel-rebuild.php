<?php

// Info: This script is designed to connect to the CMS Admin server and collect the hostname that this particular tunnel needs.

// Require CLI
if ( php_sapi_name() != 'cli' ) error('This script can only be run from command line only');

// Check whether $argv is registered
if ((int)ini_get('register_argc_argv') != 1) error('register_argc_argv is not on, please check configuration in php.ini');

// Check required parameters
if ( !isset( $argv ) || empty( $argv[1] ) || empty( $argv[2] ) ) error('missing configuration parameter');

// Wait for internet connection (5 minutes, check every 30 seconds)
$cooldown   = 30;
$timeout    = ceil(300 / $cooldown);
$url        = $argv[1];
$token      = $argv[2];
$ip         = '';
$endpoint   = '';

while (true) {

  // Get IP from AWS API
  $ip = shell_exec('curl -m 30 -s http://169.254.169.254/latest/meta-data/public-ipv4');

  // Check if we have the IP from the API, if not we don't have a connection yet, loop and try again
  if (!empty($ip)) {

    // Check IP is in the valid IPv4 range we're looking for
    if (!filter_var($ip, FILTER_VALIDATE_IP, FILTER_FLAG_IPV4) === false) {

      // Init CURL Request
      $ch  = curl_init($url);

      // Set CURL Data
      $data = [
        'token' => $token,
        'ip'    => $ip
      ];

      // Do CURL Post
      curl_setopt($ch, CURLOPT_URL, $url);
      curl_setopt($ch, CURLOPT_POST, true);
      curl_setopt($ch, CURLOPT_POSTFIELDS, http_build_query($data));
      curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
      curl_setopt($ch, CURLOPT_FOLLOWLOCATION, true);
      curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
      curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, false);

      // Get CURL Response
      $response       = curl_exec($ch);
      $orig_response  = $response;

      // Get CURL HTTP Code
      $status = curl_getinfo($ch, CURLINFO_HTTP_CODE);

      // Close connection
      curl_close($ch);

      // If down do not continue
      if ($status == 200 && !empty($response)) {

        $response = json_decode($response);

        if ( json_last_error() !== JSON_ERROR_NONE) {
          error('could not decode JSON from Infrastructure. Data: "' . $orig_response . '"');
        }

        $response = $response->response;

        if ($response->status == 'success') {
          if (isset($response->data->endpoint) && !empty(trim($response->data->endpoint))) {
            exec('sudo socat TCP-LISTEN:80,fork TCP4:' . trim($response->data->endpoint) . ':80 > /dev/null 2>/dev/null &');
            exec('sudo socat TCP-LISTEN:443,fork TCP4:' . trim($response->data->endpoint) . ':443 > /dev/null 2>/dev/null &');
            exit('Script completed sucessfully.' . PHP_EOL);
          }
        } else if ($response->status == 'error') { 
          error('error code "' . $response->code . '"" and message "' . $response->message . '"');
        }
        
        error('data: "' . var_dump( $response->code ) . '"');
      }

      error('HTTP Code: "' . $status . '"');
    }

    error('Invalid IPv4 address was returned from AWS API "' . $ip . '"');
  }

  $timeout--;

  // Check timeout hasn't expired
  if ($timeout <= 0) error('Timed out fetching IP address from AWS API.');

  // Wait for the cooldown period before continuing
  wait($cooldown);
}

// Error function, writes to both console and bin/error.log
function error($error) {
    $error      = 'Script failed, ' . $error . ', exiting...' . PHP_EOL;
    $location   = '/home/ubuntu/bin/error.log';
    $handler    = fopen($location, 'w');

    fwrite($handler, $error);
    fclose($handler);

    exit($error);
}
?>
