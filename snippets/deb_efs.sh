# Check whether the NFS has been mounted successfully
echo '#!/bin/bash -e

#function cron_flush
cron_flush () {
        local cron_file="/root/cron_temp"
        crontab -l > $cron_file
        local script_name=$(basename $0)
        sed -i -n "/$script_name/d" $cron_file
        crontab $cron_file
        rm $cron_file
}


F_CRON=0
EFS="/efs/*"

for f in $EFS; do
        if [ -d "$f" ]; then
                F_CRON=1
                break
        fi
done

if [ $F_CRON -eq 1 ]; then
        echo "Removing efs crontab"
        cron_flush
else
        echo "Mount efs"
        mount /efs
fi

exit 0' > /root/efs
chmod 0755 /root/efs

# Setup efs crontab
echo "* * * * * /root/efs" >> mycron
crontab mycron
rm mycron
