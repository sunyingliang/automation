# Install and configure monitoring agent
mkdir -p /home/ubuntu/bin

# Download project from GitHub
curl -s -H "Authorization: token {github}" -o /tmp/Monitoring.tar -L https://api.github.com/repos/Firmstep/FS-Monitoring/tarball/master
tar xf /tmp/Monitoring.tar -C /tmp
outfolder=$( ls -1c /tmp | grep Firmstep-FS-Monitoring- | head -1)
cp -R /tmp/$outfolder/Bash/monitoring /home/ubuntu/bin/monitoring
rm -R /tmp/$outfolder
rm /tmp/Monitoring.tar

# Replace the token using a global monitoring token in environments/commands
cd /home/ubuntu/bin/
cat monitoring | tr -d '\r' > monitoring_tmp && mv monitoring_tmp monitoring
sed -i -e "s@###serverid###@{name}@g" -e "s@###monurl###@{monitoring_url}@g" -e "s@###appendinstanceid###@{monitoring_appendinstanceid}@g" -e "s@###montoken###@{monitoring_token}@g" monitoring
sed -i -e '$i /sbin/start-stop-daemon -S -b --name monitoring --exec /home/ubuntu/bin/monitoring &\n' /etc/rc.local
