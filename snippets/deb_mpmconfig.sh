echo "<IfModule mpm_prefork_module>
    StartServers              5
    MinSpareServers           5
    MaxSpareServers           10
    MaxRequestWorkers         {MaxRequestWorkers}
    ServerLimit               {MaxRequestWorkers}
    MaxConnectionsPerChild    1024
</IfModule>" > /etc/apache2/mods-available/mpm_prefork.conf
