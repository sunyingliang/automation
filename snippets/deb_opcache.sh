# Enable & configure php opcache module (test only)
if [ "{environment}" != "test" ]; then
    cd /etc/php/7.0/apache2/
    sed -e "s@;opcache.enable=0@opcache.enable=1@g" php.ini > /tmp/php_tmp && mv /tmp/php_tmp php.ini
    sed -e "s@;opcache.memory_consumption=64@opcache.memory_consumption=128@g" php.ini > /tmp/php_tmp && mv /tmp/php_tmp php.ini
    sed -e "s@;opcache.interned_strings_buffer=4@opcache.interned_strings_buffer=8@g" php.ini > /tmp/php_tmp && mv /tmp/php_tmp php.ini
    sed -e "s@;opcache.max_accelerated_files=2000@opcache.max_accelerated_files=4000@g" php.ini > /tmp/php_tmp && mv /tmp/php_tmp php.ini
    sed -e "s@;opcache.revalidate_freq=2@opcache.revalidate_freq=60@g" php.ini > /tmp/php_tmp && mv /tmp/php_tmp php.ini
    sed -e "s@;opcache.fast_shutdown=0@opcache.fast_shutdown=1@g" php.ini > /tmp/php_tmp && mv /tmp/php_tmp php.ini
    sed -e "s@;opcache.enable_cli=0@opcache.enable_cli=1@g" php.ini > /tmp/php_tmp && mv /tmp/php_tmp php.ini
fi
