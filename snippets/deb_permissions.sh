# Setup permissions
usermod -a -G www-data ubuntu
chown -R www-data:www-data /var/www
chmod -R 775 /var/www
chown -R ubuntu:ubuntu /home/ubuntu/bin
chmod -R 770 /home/ubuntu/bin
