# Automatic security upgrades
apt install -y unattended-upgrades
sudo dpkg-reconfigure --priority=low unattended-upgrades -f noninteractive
sed -i -e '$i \nohup sudo unattended-upgrade -d &\n' /etc/rc.local
