# Generate swap file (currently 512mb)
dd if=/dev/zero of=/swapfile1 bs=1024 count=524288
chown root:root /swapfile1
chmod 0600 /swapfile1
mkswap /swapfile1
swapon /swapfile1
sed -i -e '$i \nohup swapon /swapfile1 &\n' /etc/rc.local
