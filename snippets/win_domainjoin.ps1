write-host "Configuring domain"
if (-Not (Test-Path "C:\Firmstep")) { New-Item -ItemType directory -Path C:\Firmstep | Out-Null } 
#download domain join script and set to run on every boot 
$r = Invoke-WebRequest -URI https://api.github.com/repos/Firmstep/FS-Automation/contents/shared/DomainJoin.ps1 -Headers @{"Authorization"="token {github}"} 
$c = $r.Content | ConvertFrom-Json 
$decoded = [System.Convert]::FromBase64String($c.content) 
[IO.File]::WriteAllBytes("C:\Firmstep\DomainJoin.ps1",$decoded) 
C:\Windows\system32\schtasks.exe /create /sc ONSTART /tn JoinDomain /tr "powershell.exe C:\Firmstep\DomainJoin.ps1 -username {domainjoin_user} -password {domainjoin_password} -ip1 {domainjoin_ip1} -ip2 {domainjoin_ip2} -domainou {domainjoin_ou} -domainname {domainjoin_name}" /RU System /RL HIGHEST 
