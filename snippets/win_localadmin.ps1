write-host "Creating local administrator account"
$localcomputer = [ADSI]"WinNT://$env:computername"
$localadmin = $localcomputer.Create("User", "LocalAdmin")
$localadmin.SetPassword("{adminpassword}")
$localadmin.UserFlags = 64 + 65536
#$localadmin.Put("HomeDirectory", "D:\LocalAdmin")
$localadmin.SetInfo()
$usergroup = [ADSI]"WinNT://$env:computername/Users,group"
$usergroup.psbase.Invoke("Add",$localadmin.path)
$admingroup = [ADSI]"WinNT://$env:computername/Administrators,group"
$admingroup.psbase.Invoke("Add",$localadmin.path)
