write-host "Setting up monitoring"
if (-Not (Test-Path "C:\Program Files\Monitoring")) { New-Item -ItemType directory -Path C:\Program` Files\Monitoring | Out-Null }
Copy-S3Object -AccessKey {s3_key} -SecretKey {s3_secret} -BucketName {s3_bucket} -Key monitoring.zip -LocalFile C:/TempDownloads/monitoring.zip | Out-Null
c:\Program` Files\7-Zip\7z.exe x c:\TempDownloads\monitoring.zip -o"C:\Program Files\Monitoring"
C:\Program` Files\Monitoring\MonitoringAgent.exe install -url {monitoring_url} -token {monitoring_token} -appendid {monitoring_appendinstanceid} -name {name} | Out-File -FilePath C:\monitorlogs.txt
Start-Service -name FSAgent
